package com.tiendosqui.dapp.register;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DatePicker;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by Divait on 10/04/2017.
 */

public class RegisterActivity extends BaseActivity implements RegisterContract.View, DatePicker.Callback {

    private EditText editName;
    private EditText editEmail;
    private EditText editPass;
    private EditText editRePass;
    private EditText editPhone;
    private EditText editDate;

    private TextInputLayout errorName;
    private TextInputLayout errorEmail;
    private TextInputLayout errorPass;
    private TextInputLayout errorRePass;
    private TextInputLayout errorPhone;
    private TextInputLayout errorDate;

    private Button btnRegister;
    private CheckBox check;

    private View progressView;
    private View registerFormView;

    private HttpCalls ra_httpCalls;
    private RegisterContract.Presenter ra_presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Create the http handler
        ra_httpCalls = new HttpCalls(this);

        RegisterController registerController = new RegisterController(getApplicationContext(), ra_httpCalls);
        ra_presenter = new RegisterPresenter(this, registerController, this);

        editName = findViewById(R.id.edit_register_name);
        editEmail= findViewById(R.id.edit_register_email);
        editPass = findViewById(R.id.edit_register_password);
        editRePass = findViewById(R.id.edit_register_repassword);
        editPhone = findViewById(R.id.edit_register_phone);
        editDate = findViewById(R.id.edit_register_date);

        errorName = findViewById(R.id.register_error_name);
        errorEmail = findViewById(R.id.register_error_email);
        errorPass = findViewById(R.id.register_error_password);
        errorRePass = findViewById(R.id.register_error_repassword);
        errorPhone = findViewById(R.id.register_error_phone);
        errorDate = findViewById(R.id.register_error_date);

        btnRegister = findViewById(R.id.register_button);
        check = findViewById(R.id.check_terms);

        registerFormView = findViewById(R.id.user_register_form);
        progressView = findViewById(R.id.register_progress);

        // Events
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorName.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorEmail.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorPass.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editRePass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorRePass.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorPhone.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorDate.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editDate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.register || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker datePicker = new DatePicker();
                datePicker.setOnDateSelected(RegisterActivity.this);
                datePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        setToolbar(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ra_presenter.start();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void setPresenter(RegisterContract.Presenter presenter) {
        if (presenter != null) {
            ra_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showProgress(boolean show) {
        registerFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNameError(String error) {
        errorName.setError(error);
        editName.requestFocus();
    }

    @Override
    public void setEmailError(String error) {
        errorEmail.setError(error);
        editEmail.requestFocus();
    }

    @Override
    public void setPasswordError(String error) {
        errorPass.setError(error);
        editPass.requestFocus();
    }

    @Override
    public void setConfPasswordError(String error) {
        errorRePass.setError(error);
        editRePass.requestFocus();
    }

    @Override
    public void setPhoneError(String error) {
        errorPhone.setError(error);
        editPhone.requestFocus();
    }

    @Override
    public void setBirthdateError(String error) {
        errorDate.setError(error);
        editDate.requestFocus();
    }

    @Override
    public void showRegisterError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPrevActivity() {
        finish();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    private void attemptRegister() {
        ra_presenter.attemptRegister(
                editName.getText().toString(),
                editEmail.getText().toString(),
                editPass.getText().toString(),
                editRePass.getText().toString(),
                editPhone.getText().toString(),
                editDate.getText().toString(),
                check.isChecked()
        );
    }

    @Override
    public void onDateSelect(String date) {
        editDate.setText(date);
    }
}
