package com.tiendosqui.dapp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.checkout.CheckoutActivity;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.recover_pass.RecoverActivity;
import com.tiendosqui.dapp.register.RegisterActivity;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 30/03/2017.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View {
    public static final int PERMISSION_PHONE_STATE = 0x001;

    private HttpCalls la_httpCalls;
    private LoginContract.Presenter la_presenter;

    // UI references.
    private EditText lf_userView;
    private EditText lf_passwordView;
    private TextInputLayout lf_userError;
    private TextInputLayout lf_passwordError;
    private TextView lf_textForgot;

    private Button lf_signInButton;
    private Button lf_signUpButton;

    private View lf_progressView;
    private View lf_loginFormView;

    private boolean goBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setToolbar(true);

        // Create the http handler
        la_httpCalls = new HttpCalls(this);

        LoginController loginInteractor = new LoginController(getApplicationContext(), la_httpCalls);
        la_presenter = new LoginPresenter(this, loginInteractor, this);

        lf_userView = (EditText) findViewById(R.id.username);
        lf_passwordView = (EditText) findViewById(R.id.password);
        lf_userError = (TextInputLayout) findViewById(R.id.register_error_email);
        lf_passwordError = (TextInputLayout) findViewById(R.id.error_password);

        lf_signInButton = (Button) findViewById(R.id.email_sign_in_button);
        lf_signUpButton = (Button) findViewById(R.id.register_button);

        lf_loginFormView = findViewById(R.id.user_login_form);
        lf_progressView = findViewById(R.id.login_progress);

        lf_textForgot = (TextView) findViewById(R.id.recover_pass_text);

        // Eventos
        lf_userView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lf_userError.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lf_passwordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lf_passwordError.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lf_passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                attemptLogin();
                return true;
            }
        });

        lf_textForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPassActivity();
            }
        });

        lf_signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        lf_signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegisterActivity();
            }
        });

        // TODO: Remove dummy DATA
//        lf_userView.setText(R.string.email_dummy);
//        lf_passwordView.setText(R.string.pass_dummy);

        // Get Data
        Intent intent = getIntent();
        goBack = intent.getBooleanExtra("back", true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        la_presenter.start();
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 4);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }

    @Override
    public void showProgress(boolean show) {
        lf_loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        lf_progressView.setVisibility(show ? View.VISIBLE : View.GONE);

    }

    @Override
    public void setEmailError(String error) {
        lf_userError.setError(error);
        lf_userView.requestFocus();
    }

    @Override
    public void setPasswordError(String error) {
        lf_passwordError.setError(error);

        if(lf_userError.getError() == null)
            lf_passwordView.requestFocus();
    }

    @Override
    public void showLoginError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMainActivity() {
        if(!goBack)
            startActivity(new Intent(this, CheckoutActivity.class));
        else {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("tab", 4);

            startActivity(intent);
        }
        finish();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        if (presenter != null) {
            la_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }
    // </editor-fold>

    // <editor-fold desc="Out Methods">
    private void attemptLogin() {
        la_presenter.attemptLogin(
                lf_userView.getText().toString(),
                lf_passwordView.getText().toString());
    }
    private void showRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        //.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void showForgotPassActivity() {
        Intent intent = new Intent(this, RecoverActivity.class);
        startActivity(intent);
        // getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    // </editor-fold>
}
