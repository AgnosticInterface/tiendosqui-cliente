package com.tiendosqui.dapp.shop;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.json.CategoryJson;
import com.tiendosqui.dapp.entities.json.NewCategoryJson;
import com.tiendosqui.dapp.entities.json.ProductJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by divait on 24/03/2017.
 */

public class ShopController {

    private Context sc_context;
    private HttpCalls sc_httpCalls;
    private List<Category> categories;
    private List<Object> products;

    private int categoryNum;
    private int pages;
    private int page;

    private BaseTabActivity activity;

    public interface Callback {

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onLoadProductsFail(String msg);

        void onLoadProducts(List<Object> products);

        void onLoadCategories(List<Category> categories);
    }

    public ShopController(Context context, HttpCalls httpCalls, BaseTabActivity activity) {
        sc_context = context;
        this.activity = activity;

        if (httpCalls != null) {
            sc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getFirstProducts(long id, Callback callback) {
        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadShopProducts(id, callback);
    }

    private void attemptLoadShopProducts(final long id, final Callback callback) {

        Map<String, String> params = new HashMap<>();

        sc_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.SHOP_PRODUCTS_URL_START) + id + NetworkInfo.SHOP_PRODUCTS_URL_END, NetworkInfo.PRODUCTS_TAG, params, params, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data shop products: " +response);
                onSuccessResponse(response, id, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, long id, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);

            Log.d("Data dev", "Data shop products: " + json);
        } catch (JSONException e) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            ProductJson[] products = gson.fromJson(json.toString(), ProductJson[].class);

            List<Product> productsList = ProductJson.toProducts(products);

            MainOrder mainOrder = DataStorage.getOrders(sc_context);

            if(mainOrder != null && mainOrder.hasOrder(id))
                productsList = addProductsInOrder(productsList, mainOrder.getOrder(id));

            List<Object> list = new ArrayList<>();

            if(categories == null) {
                for (Product product : productsList) {
                    product.setShopId(id);
                    list.add(product);
                }

                callback.onLoadProducts(list);
                return;
            }

            for (Category category : categories) {
                list.add(category);
                for (Product product : productsList) {
                    if (product.getCategory().getId() == category.getId()) {
                        product.setShopId(id);
                        list.add(product);
                    }
                }
            }

            callback.onLoadProducts(list);

        } catch (Exception ex) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            ex.printStackTrace();
        }
    }
/*
    public void getFirstProducts(long id, Callback callback) {

        Log.d("Data dev", " Get products");
        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        if(categories.size() <= 0) {
            callback.onLoadProductsFail("No products");
            return;
        }

        page = 1;
        pages = 1;
        categoryNum = 0;

        Log.d("Data dev", "Attemp");
        attemptLoadShopProducts(id, callback);

    }

    public void getProducts(long id, Callback callback) {

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        if(page + 1 < pages) {
            page ++;
            attemptLoadShopProducts(id, callback);
        } else {
            page = 1;
            if(categoryNum + 1 < categories.size()) {
                categoryNum++;
                attemptLoadShopProducts(id, callback);
            }
        }
    }

    private void attemptLoadShopProducts(final long id, final Callback callback) {
        Log.d("Data dev", "page " + page + ", cat " + categories.get(categoryNum).getId());

        Map<String, String> params = new HashMap<>();

        params.put("page", String.valueOf(page));
        params.put("category", String.valueOf(categories.get(categoryNum).getId()));

        sc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SHOP_PRODUCTS_URL_START) + id + NetworkInfo.SHOP_PRODUCTS_URL_END, NetworkInfo.PRODUCTS_TAG, params, params, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data shop products: " +response);
                onSuccessResponse(response, id, callback);
                getProducts(id, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, long id, Callback callback) {
        JSONArray json;
        JSONObject pagination;
        JSONArray inventory;
        try {
            json = new JSONArray(response);
            pagination = json.getJSONObject(0).getJSONArray("pagination").getJSONObject(0);
            inventory = json.getJSONObject(0).getJSONArray("inventory");

            pages = pagination.getInt("num_pages");

            Log.d("Data dev", "Data shop products: " +inventory);
        } catch (JSONException e) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            ProductJson[] products = gson.fromJson(inventory.toString(), ProductJson[].class);

            List<Product> productsList = ProductJson.toProducts(products);

            MainOrder mainOrder = DataStorage.getOrders(sc_context);

            if(mainOrder != null && mainOrder.hasOrder(id))
                productsList = addProductsInOrder(productsList, mainOrder.getOrder(id));

            if(categories == null) {
                for (Product product : productsList) {
                    product.setShopId(id);
                    this.products.add(product);
                }

                callback.onLoadProducts(this.products);
                return;
            }

            if (this.products == null) {
                this.products = new ArrayList<>();
                
                for (Category category : categories) {
                    this.products.add(category);
                    for (Product product : productsList) {
                        if (product.getCategory().getId() == category.getId()) {
                            product.setShopId(id);
                            this.products.add(product);
                        }
                    }
                }

                callback.onLoadProducts(this.products);
                return;
            }

            long lastId = -1;
            for (int i=0; i < this.products.size(); i++) {
                if(this.products.get(i) instanceof Category || i == this.products.size() -1) {
                    if(lastId < 0) {
                        lastId = ((Category) this.products.get(i)).getId();
                        continue;
                    }

                    for (Product product : productsList) {
                        if(product.getCategory().getId() == lastId) {
                            this.products.add(i, product);
                        }
                    }
                }
            }

            callback.onLoadProducts(this.products);


        } catch (Exception ex) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            ex.printStackTrace();
        }
    }
*/
    public void getCategories(long id, Callback callback) {

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadShopCategories(id, callback);
    }

    private void attemptLoadShopCategories(final long id, final Callback callback) {

        Map<String, String> params = new HashMap<>();

        sc_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.SHOP_PRODUCTS_URL_START) + id + NetworkInfo.SHOP_CATEGORIES_URL_END, NetworkInfo.PRODUCTS_TAG, params, params, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data shop categories: " +response);
                onSuccessCategoryResponse(response, id, callback);
                // TODO getFirstProducts(id, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessCategoryResponse(String response, long id, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Data shop category: " +json);
        } catch (JSONException e) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            NewCategoryJson[] categories = gson.fromJson(json.toString(), NewCategoryJson[].class);

            this.categories = NewCategoryJson.toCategories(categories);

            callback.onLoadCategories(this.categories);
        } catch (Exception ex) {
            callback.onServerError(sc_context.getString(R.string.error_loading_shop));
            ex.printStackTrace();
        }
    }

    public void addItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        Log.d("Data dev", "order add: " + order);

        if(order == null)
            return;

        Log.d("Data dev", "added: " + order.addProduct(product, activity));
    }

    public void removeItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null)
            return;

        order.removeProduct(product.getId(), activity);
    }

    public List<Product> addProductsInOrder(List<Product> products, Order order) {
        if(order == null)
            return products;

        List<Product> orderProducts = order.getProducts();
        for(Product product : products) {
            for(Product orderProduct : orderProducts) {
                if(product.getId() == orderProduct.getId()) {
                    product.setQuantity(orderProduct.getQuantity());
                    break;
                }
            }
        }

        return products;
    }

    public HttpCalls getHttpCalls() {
        return sc_httpCalls;
    }
}
