package com.tiendosqui.dapp.shop;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.entities.SellerSearch;
import com.tiendosqui.dapp.main.CategoryAdapter;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.shop_search.ShopSearchController;
import com.tiendosqui.dapp.shop_search.ShopSearchFragment;
import com.tiendosqui.dapp.shop_search.ShopSearchPresenter;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static com.tiendosqui.dapp.R.id.fab;
import static com.tiendosqui.dapp.R.id.progress_products;

/**
 * Created by divait on 24/03/2017.
 */

public class ShopFragment extends TabChildFragment implements ShopContract.View, ShopAdapter.onItemClicked, CategoryAdapter.OnItemClickListener {
    private Seller seller;
    private String category;
    private SellerSearch sellerSearch;
    private ShopContract.Presenter presenter;

    private RecyclerView productsList;
    private ProgressBar progressBar;
    private ShopAdapter productsAdapter;

    private RecyclerView categoryList;
    private CategoryAdapter categoryAdapter;

    private View emptySearch;
    private View searchBoxLayout;
    private EditText boxSearch;

    public TextView txtName;
    public TextView txtDescription;
    public TextView txtTimeDelivery;
    public ImageView imgPicture;
    public RatingBar rateBar;

    private FloatingActionButton btnSearch;
    public boolean back;

    public ShopFragment() {
        // Require empty constructor
    }

    public static ShopFragment getInstance() {
        return new ShopFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String seller = null;

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            seller = bundle.getString("seller", null);
            category = bundle.getString("category", null);
        } else
            parent.removeBackItem();

        if(seller != null) {
            this.sellerSearch = SellerSearch.JsonToSeller(seller);
            this.seller = SellerSearch.JsonToSeller(seller);
        } else {
            parent.removeBackItem();
        }

        categoryAdapter =CategoryAdapter.newInstance(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_seller, container, false);

        txtName = root.findViewById(R.id.seller_name);
        txtDescription = root.findViewById(R.id.seller_desc);
        txtTimeDelivery = root.findViewById(R.id.seller_time);
        imgPicture = root.findViewById(R.id.seller_image);
        rateBar = root.findViewById(R.id.seller_rating);

        btnSearch = root.findViewById(fab);
        boxSearch =  root.findViewById(R.id.search_box);
        emptySearch = root.findViewById(R.id.willy_view);
        searchBoxLayout = root.findViewById(R.id.search_box_layout);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)root.findViewById(R.id.willy_message)).setText(Html.fromHtml(getString(R.string.willy_search, seller.getName())));

                emptySearch.setVisibility(View.VISIBLE);
                searchBoxLayout.setVisibility(View.VISIBLE);
                categoryList.setVisibility(View.GONE);
                productsList.setVisibility(View.GONE);
                btnSearch.hide();

                ((AppBarLayout)root.findViewById(R.id.image_app_bar)).setExpanded(false, true);

                boxSearch.requestFocus();
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });

        root.findViewById(R.id.close_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emptySearch.setVisibility(View.GONE);
                searchBoxLayout.setVisibility(View.GONE);
                categoryList.setVisibility(View.VISIBLE);
                productsList.setVisibility(View.VISIBLE);
                btnSearch.hide();

                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });

        boxSearch.setText("");
        boxSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(back) {
                    back = false;
                    return;
                }
                try {

                    ShopSearchFragment shopFragment = ShopSearchFragment.getInstance(seller);
                    ShopSearchController shopController = new ShopSearchController(getActivity(), presenter.getHttpCaller(), (BaseTabActivity) getActivity());
                    ShopSearchPresenter shopPresenter = new ShopSearchPresenter(shopFragment, shopController);
                    shopFragment.setPresenter(shopPresenter);
                    shopFragment.setParent(parent);

                    Bundle bundle = new Bundle();
                    bundle.putString("seller", seller.toString());
                    bundle.putString("word", boxSearch.getText().toString());
                    shopFragment.setArguments(bundle);

                    parent.changeFragment(shopFragment);
                }catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        // Setup the RecyclerView for the List
        // LinearLayoutManager is used here, this will layout the elements in a similar fashion to the way ListView would layout elements.
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        productsAdapter = ShopAdapter.newInstance(getActivity(), this);

        productsList = root.findViewById(R.id.list_products);
        progressBar = root.findViewById(R.id.progress_products);

        productsList.setLayoutManager(layoutManager);
        productsList.setAdapter(productsAdapter);

        categoryList = root.findViewById(R.id.list_products_category);

        categoryList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryList.setAdapter(categoryAdapter);

        productsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    btnSearch.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 ||dy<0 && btnSearch.isShown())
                {
                    btnSearch.hide();
                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return  root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rateBar.setRating(seller.getRate());

        txtName.setText(seller.getName());
        txtDescription.setText(seller.getCat_shop());

        String dPrice = seller.getMin_shipping_price();
        String minPrice = Float.toString(seller.getMin_price());

        try {
            dPrice = NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(dPrice));
            minPrice = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(minPrice));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        txtTimeDelivery.setText(getString(R.string.shop_average_time,
                minPrice,
                dPrice,
                seller.getAverage_deliveries())
        );

        ImageLoader.loadImageFromURL(seller.getPicture(), imgPicture, R.drawable.ic_shopping_cart, getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            presenter.start();
            rechargeProducts();
        }catch (NullPointerException ex){
        }

    }

    @Override
    public void showProducts(List<Object> products) {
        progressBar.setVisibility(View.GONE);
        if(category != null) {
            ((BaseTabActivity) getActivity()).showAppBar();

            for(Product product : sellerSearch.getProducts()) {
                products.add(0, product);
            }
            products.add(0, new Category(category, null, sellerSearch.getProducts().size()));
        }

        productsAdapter.rechargeObjects(products);
    }

    @Override
    public void showLoadProductsError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCategories(List<Category> categories) {
        categoryAdapter.rechargeCategories(categories);
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError(boolean show) {
        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(ShopContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());

        if(mainOrder == null) {
            DataStorage.setOrder(getActivity(), new Order(seller, DataStorage.getAddress(getActivity())));
            presenter.addItem(product);
            holder.addQuantity();
            return;
        }

        if(mainOrder.hasOrder(seller.getId())) {
            presenter.addItem(product);
            holder.addQuantity();
        }else {
            UtilDialog.showDialog(getActivity(),
                    getString(R.string.dialog_delete_order_title),
                    getString(R.string.dialog_delete_order_desc),
                    R.string.dialog_delete_order,
                    deleteOrderClick(product, holder),
                    R.string.dialog_cancel,
                    null
            );
        }
    }

    public DialogInterface.OnClickListener deleteOrderClick(final Product product, final ProductViewHolder holder) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataStorage.setOrder(getActivity(), new Order(seller, DataStorage.getAddress(getActivity())));
                presenter.addItem(product);
                holder.addQuantity();
            }
        };
    }

    @Override
    public void onRemove(Product product) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());
        if(mainOrder != null && mainOrder.hasOrder(seller.getId()))
            presenter.removeItem(product);
    }

    @Override
    public void onComment(CheckBox checkBox, long sellerId) {
        // Not used here
    }

    @Override
    public void onItemClick(Category category) {

        int position = 0;
        for (Object obj : productsAdapter.getObjects()) {
            if (obj instanceof Category) {
                if(category.getId() == ((Category) obj).getId())
                    break;
            }

            position++;
        }
        ((LinearLayoutManager)productsList.getLayoutManager()).scrollToPositionWithOffset(position, 20);
    }

    public void rechargeProducts() {
        presenter.getCategories(seller.getId());
        presenter.getProducts(seller.getId());
    }

    public String getSellerName() {
        if(seller == null)
            return getString(R.string.app_name);

        return seller.getName();
    }
}