package com.tiendosqui.dapp.shop;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.ImageLoader;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by divai on 26/03/2017.
 */

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private Product product;
    private onItemClicked callback;
    private Context context;

    private final TextView quantityView;
    private final TextView detailsView;
    private final TextView nameView;
    private final TextView unitCostView;
    private final TextView costView;
    private final ImageView imageView;

    private final ImageView addView;
    private final ImageView removeView;

    private int quantity;

    public interface onItemClicked {
        public void onAdd(Product product, ProductViewHolder holder);
        public void onRemove(Product product);
    }

    public ProductViewHolder(View v, final Context context, final onItemClicked callback) {
        super(v);
        this.context = context;
        this.callback = callback;

        quantityView = (TextView) v.findViewById(R.id.product_quantity);
        detailsView = (TextView) v.findViewById(R.id.product_details);
        nameView = (TextView) v.findViewById(R.id.product_name);
        unitCostView = (TextView) v.findViewById(R.id.product_unit_price);
        costView = (TextView) v.findViewById(R.id.product_price);
        imageView = (ImageView) v.findViewById(R.id.product_image);

        addView = (ImageView) v.findViewById(R.id.add_item);
        removeView = (ImageView) v.findViewById(R.id.remove_item);

        addView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onAdd(product, ProductViewHolder.this);

            }
        });

        removeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(quantity > 0) {
                    callback.onRemove(product);
                    quantity--;
                    quantityView.setText(context.getString(R.string.multiplier, quantity));

                    String cost = product.getPrice();
                    try {
                        if(quantity != 0) {
                            float costNum = Float.valueOf(cost) * quantity;

                            cost = NumberFormat.getNumberInstance(Locale.US).format(costNum);
                        } else
                            cost = "";
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    getCostView().setText(cost.length() <=0?cost:context.getString(R.string.price, cost));
                }
            }
        });
    }

    public TextView getQuantityView() {
        return quantityView;
    }

    public TextView getDetailsView() {
        return detailsView;
    }

    public TextView getNameView() {
        return nameView;
    }

    public TextView getUnitCostView() {
        return unitCostView;
    }

    public TextView getCostView() {
        return costView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void configureViewHolder (Product product) {
        this.product = product;
        this.quantity = product.getQuantity();

        String quantity = context.getString(R.string.multiplier, this.quantity);
        String name = product.getName();
        String details = product.getDescription();
        String unitCost = product.getPrice();
        String disCost = product.getDiscountPrice();
        String cost = product.getPrice();

        try {
            unitCost = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(unitCost));

            if(this.quantity != 0) {
                float costNum = Float.valueOf(cost) * this.quantity;

                cost = NumberFormat.getNumberInstance(Locale.US).format(costNum);
            } else
                cost = "";
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Get element from your dataset at this position and replace the contents of the view with that element
        getQuantityView().setText(quantity);
        getNameView().setText(name);
        getDetailsView().setText(details);
        getUnitCostView().setText(context.getString(R.string.unit, unitCost));
        getCostView().setText(cost.length() <=0?cost:context.getString(R.string.price, cost));

        ImageLoader.loadCircleImageFromURL(product.getPicture(), getImageView(), R.drawable.ic_favorite, context);
    }

    public void addQuantity() {
        quantity++;
        quantityView.setText(context.getString(R.string.multiplier, quantity));

        String cost = product.getPrice();
        try {
            if(quantity != 0) {
                float costNum = Float.valueOf(cost) * quantity;

                cost = NumberFormat.getNumberInstance(Locale.US).format(costNum);
            } else
                cost = "";
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getCostView().setText(cost.length() <=0?cost:context.getString(R.string.price, cost));
    }

}
