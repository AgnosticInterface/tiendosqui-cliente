package com.tiendosqui.dapp.search;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.json.ProductSearchJson;
import com.tiendosqui.dapp.entities.json.SoldProductJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Divait on 18/04/2017.
 */

public class SearchController {

    private Context sc_context;
    private HttpCalls sc_httpCalls;

    private BaseTabActivity activity;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onNoAddress (String msg);

        void onLoadRecommendedFail (String msg);

        void onLoadRecommended(List<Product> products);

        void onSearchFail (String msg);

        void onSearchResults(List<Product> products);

    }

    public SearchController(Context context, HttpCalls httpCalls, BaseTabActivity activity) {
        sc_context = context;
        this.activity = activity;

        if (httpCalls != null) {
            sc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getRecommended(final Callback callback) {

        sc_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.RECOMMENDED_URL), NetworkInfo.SEARCH_TAG, null, new HashMap<String, String>(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data products: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    public void getProductsSearch(String word, Callback callback) {
        if(word == null || word.length() < 0) {
            return;
        }

        Address address = DataStorage.getAddress(sc_context);

        if(address == null) {
            callback.onNoAddress("No address");
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptSearch(address.getLatitude(), address.getLongitude(), word, callback);
    }

    private void onSuccessResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Data shops: " +json);
        } catch (JSONException e) {
            try {
                JSONObject jsonObj = new JSONObject(response);
                Log.d("Data dev", "Data shops: " +jsonObj);
                callback.onServerError(jsonObj.getString("detail"));
            } catch (JSONException ex) {
                callback.onServerError(sc_context.getString(R.string.error_getting_recommended));
                ex.printStackTrace();
                return;
            }

            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            SoldProductJson[] products = gson.fromJson(json.toString(), SoldProductJson[].class);

            List<Product>  productsList = new ArrayList<>();

            for(SoldProductJson productJson : products) {
                productsList.add(SoldProductJson.toProduct(productJson));
            }

            callback.onLoadRecommended(productsList);
        } catch (Exception ex) {
            callback.onLoadRecommendedFail(sc_context.getString(R.string.error_getting_recommended));
            ex.printStackTrace();
        }
    }

    private void attemptSearch(double latitude, double longitude, String word, final Callback callback) {
        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.SHOPS_POST_PARAMETER_POINT, longitude + " " + latitude);
            body.put(NetworkInfo.SEARCH__POST_PARAMETER_WORD, word);
            body.put(NetworkInfo.SEARCH__POST_PARAMETER_EXTRA, "False");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Data dev", "body: " + body.toString());

        sc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SEARCH_URL), NetworkInfo.SEARCH_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data search: " +response);
                onSuccessSearchResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessSearchResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Data products: " +json);
        } catch (JSONException e) {
            callback.onSearchFail(sc_context.getString(R.string.error_searching_products));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            ProductSearchJson[] products = gson.fromJson(json.toString(), ProductSearchJson[].class);

            List<Product> productsList = new ArrayList<>();
            MainOrder orders = DataStorage.getOrders(sc_context);
            for (ProductSearchJson product: products) {
                Product p = ProductSearchJson.toProductSearch(product);

                if( orders != null) {
                    for (Order order : orders.getOrders()) {
                        if (order.getSellerId() == p.getShopId())
                            p.setQuantity(order.containProduct(p));
                    }
                }

                productsList.add(p);
            }

            callback.onSearchResults(productsList);
        } catch (Exception ex) {
            callback.onSearchFail(sc_context.getString(R.string.error_searching_products));
            ex.printStackTrace();
        }
    }

    public void addItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null)
            return;

        order.addProduct(product, activity);
    }

    public void removeItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null)
            return;

        order.removeProduct(product.getId(), activity);
    }

    public HttpCalls getHttpCalls() {
        return sc_httpCalls;
    }
}
