package com.tiendosqui.dapp.network;

import android.content.Context;
import android.net.ConnectivityManager;

import com.tiendosqui.dapp.BuildConfig;

/**
 * Created by divait on 24/03/2017.
 *
 * Network Information.
 */

public class NetworkInfo {

    public static final int ID_TEST = 0x001;
    public static final int ID_DEV = 0x002;
    public static final int ID_RELEASE = 0x003;

    public static int selectedUrl = ID_RELEASE;

    public static final String SHOPS_GEO_URL = "shopkeepers/geo/";
    public static final String SHOPS_TAG = "SHOPS";
    public static final String SHOPS_POST_PARAMETER_POINT = "point";

    public static final String CATEGORIES_URL = "products/category/";

    public static final String SHOP_PRODUCTS_URL_START = "shopkeepers/";
    public static final String SHOP_PRODUCTS_URL_END ="/inventory/?list_price=true&user=";
    public static final String SHOP_CATEGORIES_URL_END ="/inventory/category/";
    public static final String PRODUCTS_TAG = "PRODUCTS";

    public static final String SHOP_REQUEST_LIST_URL = "shopkeepers/invitation/listprice/";
    public static final String SHOP_POST_PARAMETER_SHOP_ID = "shopid";
    public static final String SHOP_POST_PARAMETER_MOTIVES = "motive";

    public static final String LOGIN_URL = "xlogin/";
    public static final String LOGIN_TAG = "LOGIN";
    public static final String LOGIN_POST_PARAMETER_USERNAME = "username";
    public static final String LOGIN_POST_PARAMETER_PASSWORD = "password";
    public static final String LOGIN_HEADER_AUTH = "Authorization";

    public static final String GET_ADDRESSES_URL = "users/address/all/";
    public static final String ADDRESS_TAG = "ADDRESS";
    public static final String ADDRESS_PARAMETER_ADDRESS_USER_ID = "user_id";
    public static final String ADDRESS_PARAMETER_ADDRESS_USER_CANT = "user_cant";

    public static final String ADD_ADDRESS_URL = "users/xaddress/add/";
    public static final String ADDRESS_PARAMETER_USER_ID = "usersids";
    public static final String ADDRESS_PARAMETER_ADDRESS_NAME = "addressname";
    public static final String ADDRESS_PARAMETER_ADDRESS_FORMATED = "addressformated";
    public static final String ADDRESS_PARAMETER_ADDRESS_DETAILS = "addressdetail";
    public static final String ADDRESS_PARAMETER_ADDRESS_LAT = "addresslat";
    public static final String ADDRESS_PARAMETER_ADDRESS_LON = "addresslon";
    public static final String ADDRESS_PARAMETER_ADDRESS_UNIQUE = "uniqueid";

    public static final String REGISTER_URL = "users/add/";
    public static final String REGISTER_TAG = "REGISTER";
    public static final String REGISTER_POST_PARAMETER_NAME = "name";
    public static final String REGISTER_POST_PARAMETER_EMAIL = "email";
    public static final String REGISTER_POST_PARAMETER_PASSWORD = "password";
    public static final String REGISTER_POST_PARAMETER_BIRTHDATE = "birthdate";
    public static final String REGISTER_POST_PARAMETER_PHONE = "phone";

    public static final String USER_INFO_URL = "users/profile/";

    public static final String ORDER_URL = "orders/pedido/";
    public static final String ORDER_TAG = "ORDER";
    public static final String ORDER_POST_PARAMETER_USER = "usuario";
    public static final String ORDER_POST_PARAMETER_ORDER = "orden";

    public static final String ORDERS_URL_START = "orders/user/";
    public static final String ORDERS_URL_END ="/active/";

    public static final String ORDERS_URL = "orders/all/status/user/";

    public static final String ORDERS_H_URL_END ="/history/page/1/"; // history/page/1

    public static final String ORDERS_POST_PARAMETER_ID = "user_id";
    public static final String ORDERS_POST_PARAMETER_OFFSET = "offset";
    public static final String ORDERS_POST_PARAMETER_PAGE = "page";
    public static final String ORDERS_POST_PARAMETER_IDS = "order_status_ids";

    public static final String ORDERS_TAG = "ORDERS";

    public static final String ORDER_DETAILS_URL_START = "orders/";
    public static final String ORDER_DETAILS_URL_END = "/detail/";
    public static final String ORDER_DETAILS_TAG = "ORDER_DETAILS";

    public static final String RECOMMENDED_URL = "shopkeepers/products/sold/globally/";

    public static final String SEARCH_URL = "shopkeepers/search/products/";
    public static final String SEARCH_TAG = "SEARCH";
    public static final String SEARCH__POST_PARAMETER_WORD = "search";
    public static final String SEARCH__POST_PARAMETER_EXTRA = "extra";
    public static final String SEARCH__POST_PARAMETER_POINT = "point";

    public static final String SEARCH__POST_PARAMETER_USER_ID = "user_id";
    public static final String SEARCH__POST_PARAMETER_LIST_PRICE = "list_price";

    public static final String SHOP_SEARCH_URL = "shopkeepers/search/products/shop/";
    public static final String SHOP_SEARCH__POST_PARAMETER_WORD = "search";
    public static final String SHOP_SEARCH__POST_PARAMETER_ID = "shop_id";

    public static final String TERMS_URL = "system/terms/";
    public static final String TERMS_TAG = "TERMS";

    public static final String SUPPORT_NUM_URL = "system/contact/phones/";
    public static final String SUPPORT_CALL_ME_URL = "system/call/user/";
    public static final String SUPPORT_CALL_ME_POST_PARAMETER_ID = "user_id";
    public static final String SUPPORT_TICKET_URL = "orders/support/add/ticket/";
    public static final String SUPPORT_TICKET__POST_PARAMETER_ID = "order_id";
    public static final String SUPPORT_TICKET__POST_PARAMETER_MSG = "motive";
    public static final String SUPPORT_TAG = "SUPPORT";

    public static final String RECOVER_PASS_URL = "users/send/email/password/";
    public static final String RECOVER_PASS_TAG = "RECOVER";
    public static final String RECOVER_PASS_POST_PARAMETER_EMAIL = "user_email";

    public static final String DEVICE_ID_URL = "users/deviceids/create/";
    public static final String DEVICE_ID_POST_PARAMETER_USER = "userid";
    public static final String DEVICE_ID_POST_PARAMETER_TYPE = "type_device";
    public static final String DEVICE_ID_POST_PARAMETER_ID = "users_deviceid";

    public static final String UPDATE_USER_URL = "users/profile/update/";
    public static final String UPDATE_USER_TAG = "UPDATE_USER";
    public static final String UPDATE_USER_POST_PARAMETER_ID = "user_id";
    public static final String UPDATE_USER_POST_PARAMETER_EMAIL = "user_email";
    public static final String UPDATE_USER_POST_PARAMETER_PHONE = "user_phone";
    public static final String UPDATE_USER_POST_PARAMETER_USER_NAME = "user_name";
    public static final String UPDATE_USER_POST_PARAMETER_USER_IMG = "user_image";

    public static final String RATE_URL = "qualify/shop/";
    public static final String RATE_TAG = "RATE";
    public static final String RATE_POST_PARAMETER_ID = "order_id";
    public static final String RATE_POST_PARAMETER_RATE = "rate";
    public static final String RATE_POST_PARAMETER_COMMENT = "comment";

    public static final String CITIES_URL = "shopkeepers/cities/";

    /**
     * Is internet Connection.
     *
     * @return True if there is internet connection.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static void setURL(int id){
        switch (id){
            case ID_TEST:
                selectedUrl = ID_TEST;
                break;
            case ID_DEV:
                selectedUrl = ID_DEV;
                break;
        }
    }

    public static String getURL(){
        switch (selectedUrl){
            case ID_TEST:
                return BuildConfig.URL_TEST;
            case ID_DEV:
                return BuildConfig.URL_DEV;
            case ID_RELEASE:
            default:
                return BuildConfig.URL;
        }
    }

    public static String getImageURL(){
        switch (selectedUrl){
            case ID_TEST:
                return BuildConfig.URL_TEST_IMAGE;
            case ID_DEV:
                return BuildConfig.URL_DEV_IMAGE;
            case ID_RELEASE:
            default:
                return BuildConfig.URL_IMAGE;
        }
    }

    public static String getURL(String data){
        return getURL() + data;
    }
}
