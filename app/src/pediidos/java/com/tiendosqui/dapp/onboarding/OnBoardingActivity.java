package com.tiendosqui.dapp.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.login.LoginActivity;
import com.tiendosqui.dapp.map.MapActivity;
import com.tiendosqui.dapp.utils.DataStorage;

/**
 * Created by divait on 11/10/2016.
 *
 *
 */

public class OnBoardingActivity extends AppCompatActivity {
    private ViewPager ub_pager;
    private PagerAdapter ub_pagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        ub_pager = findViewById(R.id.benefits_container);
        ub_pagerAdapter = new OnBoardingPagerAdapter(getSupportFragmentManager());
        ub_pager.setAdapter(ub_pagerAdapter);

        DataStorage.setOnBoarding(this, true);
    }

    @Override
    public void onBackPressed() {
        if (ub_pager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            exitDemo();
        } else {
            // Otherwise, select the previous step.
            ub_pager.setCurrentItem(ub_pager.getCurrentItem() - 1);
        }
    }

    public void nextPage() {
        ub_pager.setCurrentItem(ub_pager.getCurrentItem() + 1);
    }

    public void exitDemo() {
        Intent intent = new Intent(this, LoginActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
