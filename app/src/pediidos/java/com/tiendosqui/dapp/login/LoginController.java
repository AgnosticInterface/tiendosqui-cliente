package com.tiendosqui.dapp.login;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.AddressController;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.entities.json.AddressJson;
import com.tiendosqui.dapp.entities.json.UserJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.JWTUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by divai on 30/03/2017.
 */

public class LoginController {
    private static final String PASSWORD_PATTERN_LOG = "^(?=\\S+$).{4,}$";
    public static final Pattern PASSWORD_LOG = Pattern.compile(PASSWORD_PATTERN_LOG);

    private final Context li_context;
    private HttpCalls li_httpCalls;

    public interface Callback {

        void onEmailError(String msg);

        void onPasswordError(String msg);

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onAuthFailed(String msg);

        void onAuthSuccess();

        void onNoAddress(String msg);
    }

    public LoginController(Context context, HttpCalls httpCalls) {
        li_context = context;
        if (httpCalls != null) {
            li_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    /**
     * Receive and validate the information to send a login request.
     * Show the error if exist or use http call to perform the login if data is correct.
     *
     * @param username to login.
     * @param password to login.
     * @param callback where login request will be return.
     */
    public void login(String username, String password, final Callback callback) {
        // Check logic
        boolean validEmail = isValidUsername(username, callback);
        boolean validPass = isValidPassword(password, callback);
        if (!(validEmail && validPass)) {
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(li_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        // Make login call
        signInUser(username, password, callback);

    }

    /**
     * Check if the password is useful.
     *
     * @param password the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidPassword(String password, Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(password)) {
            callback.onPasswordError(li_context.getString(R.string.error_empty_password));
            isValid = false;
        } /* TODO else if (PASSWORD_LOG.matcher(password).matches()) {
            callback.onPasswordError(li_context.getString(R.string.error_invalid_password));
            isValid = false;
        } */
        return isValid;
    }

    /**
     * Check if the username is useful.
     *
     * @param username the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidUsername(String username, Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(username)) {
            callback.onEmailError(li_context.getString(R.string.error_empty_user));
            isValid = false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            callback.onEmailError(li_context.getString(R.string.error_invalid_user));
            isValid = false;
        }
        return isValid;
    }

    /**
     * Make a call to login user in the system, report if any error or get token if login complete.
     *
     * @param username to login.
     * @param password to login.
     * @param callback to send the respond of the system.
     */
    private void signInUser(String username, String password, final Callback callback) {

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.LOGIN_POST_PARAMETER_USERNAME, username);
            body.put(NetworkInfo.LOGIN_POST_PARAMETER_PASSWORD, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        li_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.LOGIN_URL), NetworkInfo.LOGIN_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data1: " + response);
                JSONObject json;
                try {
                    json = new JSONObject(response);
                    JSONObject data = JWTUtils.decoded(json.getString("token"));
                    onSuccessLogin(data, json.getString("token"), callback);
                } catch (Exception e) {
                    e.printStackTrace();

                    callback.onServerError(li_context.getString(R.string.error_login));
                }
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessLogin(JSONObject data, String token, final Callback callback) throws JSONException {
       DataStorage.setToken(li_context, token);

        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ token);

        final long id = data.getLong("user_id");

        li_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.USER_INFO_URL)+ id +"/", NetworkInfo.LOGIN_TAG, null, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data2: " + response);
                onSuccessResponse(response, id, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(li_context.getString(R.string.server_error));
            }
        });

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.DEVICE_ID_POST_PARAMETER_USER, id);
            body.put(NetworkInfo.DEVICE_ID_POST_PARAMETER_TYPE, "android");
            body.put(NetworkInfo.DEVICE_ID_POST_PARAMETER_ID, DataStorage.getDeviceID(li_context));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());

        li_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.DEVICE_ID_URL), NetworkInfo.LOGIN_TAG, headers, body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data3: " + response);
            }

            @Override
            public void errorResponse(String error) {
                Log.d("Data dev", "error3: " + error);

                // callback.onServerError(li_context.getString(R.string.server_error));
            }
        });
    }

    private void onSuccessResponse(String response, long id, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray (response);

            Gson gson = new Gson();
            UserJson userJson = gson.fromJson(json.get(0).toString(), UserJson.class);
            User user = UserJson.fromJsonToUser(userJson, id);

            DataStorage.saveUserData(li_context, user);

            getAddress(user, callback);
        } catch (JSONException e) {
            Log.d("Data dev", "error parsing json: " + response);
            callback.onServerError(li_context.getString(R.string.error_creating_order));
            e.printStackTrace();
        }
    }

    public void getAddress(User user, Callback callback) {
        // Check Network
        if (!NetworkInfo.isNetworkAvailable(li_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadAddress(user.getId(), callback);
    }

    private void attemptLoadAddress(long id, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(li_context));

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_ID, Long.toString(id) );
        params.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_CANT, "10" );

        JSONObject body = new JSONObject();

        try {
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_ID, id);
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_CANT, "10");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());
        li_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.GET_ADDRESSES_URL), NetworkInfo.ADDRESS_TAG, headers, body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                onSuccessAddressResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessAddressResponse(String response, LoginController.Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Address Json: " +json);
        } catch (JSONException e) {
            try {
                JSONObject job = new JSONObject(response);
                Log.d("Data dev", "Address Json: " + job);

                if(job.getString("petition").trim().equals("OK")) {
                    callback.onAuthFailed("No Address");
                    return;
                }

            } catch (JSONException ex) {
                callback.onServerError(li_context.getString(R.string.server_error));
                ex.printStackTrace();
                return;
            }

            callback.onServerError(li_context.getString(R.string.server_error));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            AddressJson[] addressesJson = gson.fromJson(json.toString(), AddressJson[].class);

            if(addressesJson.length <= 0) {
                callback.onAuthFailed("No Address");
                return;
            }

            Address address = AddressJson.toAddress(addressesJson[0]);
            address.setId(addressesJson[0].getId());
            address.setImageId(getImage(addressesJson[0].getAddress_alias()));

            DataStorage.setAddress(li_context, address);
            callback.onAuthSuccess();
        } catch (Exception ex) {
            callback.onServerError(li_context.getString(R.string.error_address_loading));
            ex.printStackTrace();
        }
    }

    private int getImage(String name){
        switch (name){
            case "Casa":
                return R.drawable.home;
            case "Oficina":
                return R.drawable.work;
            case "Novi@":
                return R.drawable.friend;
            default:
            case "Otro":
                return R.drawable.other;

        }
    }
}
