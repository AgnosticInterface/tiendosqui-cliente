package com.tiendosqui.dapp.shop;

import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by divai on 24/03/2017.
 */

public interface ShopContract {
    interface View extends BaseView<ShopContract.Presenter> {

        void showProducts (List<Object> products);

        void showLoadProductsError (String msg);

        void showCategories (List<Category> categories);

        void showServerError (String msg);

        void showNetworkError(boolean show);

        void showRequestListAccessSuccess();

    }

    interface Presenter extends BasePresenter {
        void getCategories(long id);

        void getProducts (long id);

        void subscribeList (long id);

        void addItem (Product product);

        void removeItem (Product product);

        HttpCalls getHttpCaller();
    }
}
