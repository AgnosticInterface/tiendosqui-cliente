package com.tiendosqui.dapp.shop;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.HttpCalls;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The presenter for
 */

public class ShopPresenter implements ShopContract.Presenter, ShopController.Callback {

    private ShopContract.View sp_view;
    private ShopController sp_controller;

    public ShopPresenter (@NonNull ShopContract.View mainView,
                          @NonNull ShopController mainController) {

        sp_view = mainView;
        sp_controller = mainController;
    }

    @Override
    public void getCategories(long id) {
        sp_controller.getCategories(id, this);
    }

    @Override
    public void getProducts(long id) {
        sp_controller.getProducts(id, this);
    }

    @Override
    public void subscribeList(long id) {
        sp_controller.requestListAccess(id, this);
    }

    @Override
    public void addItem(Product product) {
        sp_controller.addItem(product);
    }

    @Override
    public void removeItem(Product product) {
        sp_controller.removeItem(product);
    }

    @Override
    public void start() {
    }

    @Override
    public void onNetworkConnectFailed() {
        sp_view.showNetworkError(true);
    }

    @Override
    public void onServerError(String msg) {
        sp_view.showServerError(msg);
    }

    @Override
    public void onLoadProductsFail(String msg) {
        sp_view.showLoadProductsError(msg);
    }

    @Override
    public void onLoadProducts(List<Object> products) {
        sp_view.showProducts(products);
    }

    @Override
    public void onLoadCategories(List<Category> categories) {
        sp_view.showCategories(categories);
    }

    @Override
    public void onSuccessRequestListAccess() {
        sp_view.showRequestListAccessSuccess();
    }

    @Override
    public HttpCalls getHttpCaller() {
        return sp_controller.getHttpCalls();
    }

}
