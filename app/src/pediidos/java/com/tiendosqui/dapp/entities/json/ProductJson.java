package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 25/03/2017.
 */

public class ProductJson {

    private long id;
    private long shop;
    private InnerProductJson product;
    private BasePriceJson base_price;
    private boolean enable;

    public long getId() {
        return id;
    }

    public InnerProductJson getProduct() {
        return product;
    }

    public BasePriceJson getBase_price() {
        return base_price;
    }

    public boolean isEnable() {
        return enable;
    }

    public long getShop() {
        return shop;
    }

    public static Product toProduct (ProductJson json) {
        Category category = new Category(
                json.getProduct().getSubcategory().getCategory().getId(),
                json.getProduct().getSubcategory().getId(),
                json.getProduct().getSubcategory().getCategory().getName(),
                json.getProduct().getSubcategory().getName(),
                json.getProduct().getSubcategory().getCategory().getPicture()
        );
        String price;
        String fullPrice;

        if(json.getBase_price().getListPrice() != null) {
            price = json.getBase_price().getListPrice();
            fullPrice = json.getBase_price().getPublicPrice();
        } else {
            price = json.getBase_price().getPublicPrice();
            fullPrice = null;
        }

        return new Product(
                json.getId(),
                json.getShop(),
                json.getProduct().getName(),
                json.getProduct().getDescription(),
                json.getProduct().getPicture(),
                category,
                price,
                fullPrice
        );
    }

    public static List<Product> toProducts (ProductJson[] array) {
        List<Product> products = new ArrayList<>();
        for (ProductJson json: array) {
            if(json.isEnable()) // TODO Remove if (Check when get from database)
                products.add(toProduct(json));
        }

        return products;
    }

    public ProductJson() {
    }

}
