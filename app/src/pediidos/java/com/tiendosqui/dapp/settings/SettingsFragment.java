package com.tiendosqui.dapp.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.login.LoginActivity;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.profile.ProfileActivity;
import com.tiendosqui.dapp.support.SupportActivity;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.terms.TermsActivity;
import com.tiendosqui.dapp.utils.DataStorage;

/**
 * Created by Divait on 04/04/2017.
 *
 * The view of the settings.
 */

public class SettingsFragment extends TabChildFragment implements SettingsContract.View {
    static final int LOGIN_REQUEST = 1;

    private SettingsContract.Presenter presenter;

    public ImageView imgUser;
    public TextView txtName;
    public TextView txtEmail;

    public Button btnProfile;
    public Button btnAssistance;
    public Button btnTerms;
    public Button btnLogout;

    private View profileView;

    public SettingsFragment() {
        // Require empty constructor
    }

    public static SettingsFragment getInstance() {
        return new SettingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        txtName = root.findViewById(R.id.user_name);
        txtEmail = root.findViewById(R.id.user_email);

        imgUser = root.findViewById(R.id.user_img);
        profileView = root.findViewById(R.id.user_info);

        btnProfile = root.findViewById(R.id.btn_profile);
        btnAssistance = root.findViewById(R.id.btn_asistence);
        btnTerms = root.findViewById(R.id.btn_terms_cond);
        btnLogout = root.findViewById(R.id.btn_logout);

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(DataStorage.isLogged(getActivity()))
                    startActivity(new Intent(getActivity(), ProfileActivity.class));
                else {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                }
            }
        });

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TermsActivity.class));
            }
        });

        btnAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SupportActivity.class));
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DataStorage.isLogged(getActivity())) {
                    presenter.closeSession();
                }
            }
        });

        return  root;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            presenter.start();
        }catch (NullPointerException ex){
        }
    }

    @Override
    public void setPresenter(SettingsContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showNoUser() {
        imgUser.setImageDrawable(getResources().getDrawable(R.drawable.will));
        profileView.setVisibility(View.GONE);
        btnLogout.setVisibility(View.GONE);

        btnProfile.setText(getString(R.string.set_login));
    }

    @Override
    public void showUser(User user) {
        profileView.setVisibility(View.VISIBLE);
        btnLogout.setVisibility(View.VISIBLE);
        txtName.setText(user.getName());
        txtEmail.setText(user.getEmail());
        btnProfile.setText(getString(R.string.set_profile));

        ImageLoader.loadCircleImageFromURL(user.getPicture(), imgUser, R.drawable.will, getContext());
    }

    @Override
    public void showSessionClose() {
        Toast.makeText(getActivity(), getString(R.string.on_logout), Toast.LENGTH_LONG).show();
        getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void showSessionCloseError() {

    }
}
