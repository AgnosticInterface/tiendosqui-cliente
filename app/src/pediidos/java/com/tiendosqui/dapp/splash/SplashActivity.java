package com.tiendosqui.dapp.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.tiendosqui.dapp.login.LoginActivity;
import com.tiendosqui.dapp.map.MapActivity;
import com.tiendosqui.dapp.onboarding.OnBoardingActivity;
import com.tiendosqui.dapp.utils.DataStorage;

/**
 * Created by divait on 9/11/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!DataStorage.getOnBoarding(this)) {
            Intent intent = new Intent(this, OnBoardingActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
}