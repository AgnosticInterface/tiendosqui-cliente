package com.tiendosqui.dapp.shop_search;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.json.ProductJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by divait on 24/03/2017.
 */

public class ShopSearchController {

    private Context sc_context;
    private HttpCalls sc_httpCalls;

    private BaseTabActivity activity;

    public interface Callback {

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onSearchFail(String msg);

        void onSearchResults(List<Product> products);
    }

    public ShopSearchController(Context context, HttpCalls httpCalls, BaseTabActivity activity) {
        sc_context = context;
        this.activity = activity;

        if (httpCalls != null) {
            sc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getProductsSearch(long id, String word, Callback callback) {
        if(word == null || word.length() < 0) {
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptSearch(id, word, callback);
    }

    private void attemptSearch(long id, String word, final Callback callback) {
        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.SHOP_SEARCH__POST_PARAMETER_ID, Long.toString(id));
            body.put(NetworkInfo.SHOP_SEARCH__POST_PARAMETER_WORD, word);
            body.put(NetworkInfo.SEARCH__POST_PARAMETER_LIST_PRICE, true);
            body.put(NetworkInfo.SEARCH__POST_PARAMETER_USER_ID, DataStorage.getUserId(sc_context));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Data dev", "body: " + body.toString());

        sc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SHOP_SEARCH_URL), NetworkInfo.SEARCH_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data search: " +response);
                onSuccessSearchResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessSearchResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Data products: " +json);
        } catch (JSONException e) {
            callback.onSearchFail(sc_context.getString(R.string.error_searching_products));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            ProductJson[] products = gson.fromJson(json.toString(), ProductJson[].class);

            List<Product> productsList = new ArrayList<>();
            MainOrder orders = DataStorage.getOrders(sc_context);
            for (ProductJson product: products) {
                Product p = ProductJson.toProduct(product);

                if(orders != null) {
                    for (Order order : orders.getOrders()) {
                        if (order.getSellerId() == p.getShopId())
                            p.setQuantity(order.containProduct(p));
                    }
                }

                productsList.add(p);
            }

            callback.onSearchResults(productsList);
        } catch (Exception ex) {
            callback.onSearchFail(sc_context.getString(R.string.error_searching_products));
            ex.printStackTrace();
        }
    }

    public void addItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null)
            return;

        order.addProduct(product, activity);
    }

    public void removeItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null)
            return;

        order.removeProduct(product.getId(), activity);
    }
}
