package com.tiendosqui.dapp.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;

/**
 * Created by divait on 12/10/2016.
 *
 *
 */

public class OnBoardingFragment extends Fragment {
    public static final int SPEED = 0x001;
    public static final int STAY = 0x002;
    public static final int SERVICE = 0x003;

    private static final String KEY_TYPE = "type";

    private int currentType;

    public static OnBoardingFragment newInstance(int type) {
        OnBoardingFragment fragment = new OnBoardingFragment();

        Bundle args = new Bundle();
        args.putInt(KEY_TYPE, type);
        fragment.setArguments(args);

        return fragment;
    }

    public OnBoardingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentType = getArguments().getInt(KEY_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_on_boarding, container, false);

        ImageView image = (ImageView) root.findViewById(R.id.benefits_image);
        TextView titleView = (TextView) root.findViewById(R.id.benefits_title);
        TextView descView = (TextView) root.findViewById(R.id.benefits_desc);
        TextView loginView = (TextView) root.findViewById(R.id.benefits_login);
        TextView nextView = (TextView) root.findViewById(R.id.benefits_next);

        String title;
        String subtitle;
        int imageID;

        switch (currentType){
            default:
            case SPEED:
                imageID = R.drawable.onboarding1;
                title = getString(R.string.speed_title);
                subtitle = getString(R.string.speed_desc);

                nextView.setTextColor(getResources().getColor(R.color.colorAccentSecondary));
                break;
            case STAY:
                imageID = R.drawable.onboarding2;
                title = getString(R.string.stay_title);
                subtitle = getString(R.string.stay_desc);

                nextView.setTextColor(getResources().getColor(R.color.colorAccentSecondary));
                break;
            case SERVICE:
                imageID = R.drawable.onboarding3;
                title = getString(R.string.service_title);
                subtitle = getString(R.string.service_desc);

                nextView.setVisibility(View.GONE);
                loginView.setGravity(Gravity.CENTER_HORIZONTAL);
                loginView.setTextColor(getResources().getColor(R.color.colorAccentSecondary));
                break;
        }

        nextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OnBoardingActivity)getActivity()).nextPage();
            }
        });

        loginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OnBoardingActivity)getActivity()).exitDemo();
            }
        });

        image.setImageResource(imageID);
        titleView.setText(title);
        descView.setText(subtitle);

        return root;
    }
}
