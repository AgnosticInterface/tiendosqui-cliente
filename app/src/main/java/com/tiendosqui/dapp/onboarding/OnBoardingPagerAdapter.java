package com.tiendosqui.dapp.onboarding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by divait on 12/10/2016.
 *
 *
 */

public class OnBoardingPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUMBER_SLIDES = 3;

    public OnBoardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            default:
            case 0:
                return OnBoardingFragment.newInstance(OnBoardingFragment.SPEED);
            case 1:
                return OnBoardingFragment.newInstance(OnBoardingFragment.STAY);
            case 2:
                return OnBoardingFragment.newInstance(OnBoardingFragment.SERVICE);

        }
    }

    @Override
    public int getCount() {
        return NUMBER_SLIDES;
    }
}
