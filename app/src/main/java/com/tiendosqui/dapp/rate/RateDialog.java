package com.tiendosqui.dapp.rate;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by divai on 24/05/2017.
 */

public class RateDialog {

    public static AlertDialog showDialogRate(final Activity activity, ViewGroup parent, final SmallOrder order) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(createRateDialog(activity, parent));
        builder.setTitle(activity.getString(R.string.rate_title, order.getSeller().getName()));
        builder.setMessage(activity.getString(R.string.rate_description));
        builder.setPositiveButton(activity.getString(R.string.action_rate), null);
        builder.setCancelable(false);

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button b = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        positiveButtonClick(dialog, order, activity);
                    }
                });
            }
        });

        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(activity.getResources().getColor(R.color.grey_6));

        return dialog;
    }

    private static View createRateDialog (final Activity activity, ViewGroup parent) {

        final LinearLayout item = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.dialog_rate, parent);
        RatingBar ratingBar = (RatingBar) item.findViewById(R.id.rating_bar);
        final EditText input =(EditText) item.findViewById(R.id.comment_input);
        final TextInputLayout error = (TextInputLayout) item.findViewById(R.id.comment_error);
        final TextView text = (TextView) item.findViewById(R.id.rate_why);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                int starts = (int) rating;
                switch (starts) {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        error.setVisibility(View.VISIBLE);
                        text.setVisibility(View.VISIBLE);
                        input.requestFocus();
                        break;
                    case 5:
                        error.setVisibility(View.GONE);
                        text.setVisibility(View.GONE);
                        break;
                }
            }
        });

        return item;
    }

    private static void positiveButtonClick(DialogInterface dialog, SmallOrder order, Activity activity) {
        RatingBar ratingBar = (RatingBar) ((AlertDialog)dialog).findViewById(R.id.rating_bar);

        int starts = (int) ratingBar.getRating();

        switch (starts) {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
                EditText input =(EditText)((AlertDialog)dialog).findViewById(R.id.comment_input);

                if(!input.getText().toString().isEmpty()) {
                    sendRate(ratingBar.getNumStars(), input.getText().toString(), order.getId(), activity);
                    dialog.dismiss();
                } else {
                    Toast.makeText(activity, "Por favor llena la caja de texto.", Toast.LENGTH_SHORT).show();
                }
                break;
            case 5:
                sendRate(ratingBar.getNumStars(), "", order.getId(), activity);
                dialog.dismiss();
                break;
        }
    }

    private static void sendRate(int starts, String comment, long order_id, final Context context) {
        HttpCalls httpCalls = new HttpCalls(context);

        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(context));

        JSONObject body = new JSONObject();

        try {
            body.put(NetworkInfo.RATE_POST_PARAMETER_ID, order_id);
            body.put(NetworkInfo.RATE_POST_PARAMETER_RATE, starts);
            body.put(NetworkInfo.RATE_POST_PARAMETER_COMMENT, comment);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());

        httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.RATE_URL), NetworkInfo.RATE_TAG, headers, body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                onSuccessResponse(response, context);
            }

            @Override
            public void errorResponse(String error) {
                Log.d("Data dev", "error: " + error);
                Toast.makeText(context,context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private static void onSuccessResponse(String response, Context context) {
        JSONObject json;
        try {
            json = new JSONObject(response);
            Log.d("Data dev", "Support response Json: " +json);

            if(json.has("petition")){
                if(json.getString("petition").trim().equals("OK")) {
                    if(json.has("detail"))
                        Toast.makeText(context, json.getString("detail"), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, context.getString(R.string.thanks_rate), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        } catch (JSONException e) {
            Log.d("Data dev", "error parsing json: " + response);
            Toast.makeText(context,context.getString(R.string.error_processing_rate), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return;
        }
    }
}
