package com.tiendosqui.dapp.search;

import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by Medycitas on 18/04/2017.
 */

public interface SearchContract {

    interface View extends BaseView<Presenter> {

        void showRecommended(List<Product> products);

        void showLoadRecommendedError(String msg);

        void searchResult (List<Product> products);

        void showSearchError (String msg);

        void showServerError (String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void getRecommended();

        void searchProduct(String keyWord);

        void addItem (Product product);

        void removeItem (Product product);

        HttpCalls getHttpCaller();
    }
}
