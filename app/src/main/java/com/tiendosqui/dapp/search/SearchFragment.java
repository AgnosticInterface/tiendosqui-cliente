package com.tiendosqui.dapp.search;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.entities.SellerSearch;
import com.tiendosqui.dapp.shop.ProductViewHolder;
import com.tiendosqui.dapp.shop.ShopController;
import com.tiendosqui.dapp.shop.ShopFragment;
import com.tiendosqui.dapp.shop.ShopPresenter;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Divait on 25/01/2017.
 *
 * The fragment to performa a search of products.
 */

public class SearchFragment extends TabChildFragment implements SearchContract.View, SearchAdapter.OnItemClickListener {

    private RecyclerView searchList;
    private View emptyView;

    private SearchAdapter searchAdapter;

    private EditText searchBox;

    private SearchContract.Presenter presenter;

    public SearchFragment() {
        // Require empty constructor
    }

    public static SearchFragment getInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchAdapter = SearchAdapter.newInstance(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        searchList = (RecyclerView) root.findViewById(R.id.list_search);
        searchBox = (EditText) root.findViewById(R.id.search_box);

        emptyView = root.findViewById(R.id.willy_view);
        ((TextView)emptyView.findViewById(R.id.willy_message)).setText(getString(R.string.empty_search));

        searchList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        searchList.setAdapter(searchAdapter);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    presenter.searchProduct(searchBox.getText().toString());
                }catch (NullPointerException ex) {
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return  root;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            presenter.start();
        }catch (NullPointerException ex){
        }
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showRecommended(List<Product> products) {
        emptyView.setVisibility(View.GONE);
        searchList.setVisibility(View.VISIBLE);
        searchAdapter.rechargeProducts(products);
    }

    @Override
    public void showLoadRecommendedError(String msg) {
        emptyView.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void searchResult(List<Product> products) {
        emptyView.setVisibility(View.GONE);
        searchList.setVisibility(View.VISIBLE);

        List<SellerSearch> sellers = new ArrayList<>();
        for(Product product : products) {
            Log.d("Data dev", "Shopid: " + product.getShopId());
            boolean shopExist = false;
            for(SellerSearch seller : sellers) {
                if(seller.getId() == product.getShopId()) {
                    seller.addProduct(product);

                    shopExist = true;
                    break;
                }
            }

            if(!shopExist) {
                Seller seller = SugarRecord.findById(Seller.class, product.getShopId());

                if(seller != null) {
                    SellerSearch sellerSearch = new SellerSearch(seller);
                    sellerSearch.addProduct(product);
                    sellers.add(sellerSearch);
                }
            }
        }

        searchAdapter.rechargeShops(sellers);
    }

    @Override
    public void showSearchError(String msg) {
        emptyView.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);

        searchAdapter.removeAll();
        //Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError(String msg) {
        emptyView.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);

        if(getActivity() == null)
            return;

        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        emptyView.setVisibility(View.VISIBLE);
        searchList.setVisibility(View.GONE);

        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(String data) {
        searchAdapter.removeAll();
        searchBox.setText(data);
        //presenter.searchProduct(data);
    }

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());

        if(mainOrder == null) {
            Seller seller = SugarRecord.findById(Seller.class, product.getShopId());

            Log.d("Data dev", "Payment:  " + seller.getMethods_payment());

            DataStorage.setOrder(getActivity(), new Order(seller, DataStorage.getAddress(getActivity())));
            presenter.addItem(product);
            holder.addQuantity();
            return;
        }

        if(mainOrder.hasOrder(product.getShopId())) {
            presenter.addItem(product);
            holder.addQuantity();
        }else {
            Seller seller = SugarRecord.findById(Seller.class, product.getShopId());

            Log.d("Data dev", "Payment1:  " + seller.getMethods_payment());

            UtilDialog.showDialog(getActivity(),
                    getString(R.string.dialog_delete_order_title),
                    getString(R.string.dialog_delete_order_desc),
                    R.string.dialog_delete_order,
                    deleteOrderClick(product, seller, holder),
                    R.string.dialog_cancel,
                    null
            );
        }
    }

    public DialogInterface.OnClickListener deleteOrderClick(final Product product, final Seller seller, final ProductViewHolder holder) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataStorage.setOrder(getActivity(), new Order(seller, DataStorage.getAddress(getActivity())));
                presenter.addItem(product);
                holder.addQuantity();
            }
        };
    }

    @Override
    public void onRemove(Product product) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());
        if(mainOrder !=null && mainOrder.hasOrder(product.getShopId()))
            presenter.removeItem(product);
    }

    @Override
    public void onOpenShop(SellerSearch seller) {
        ShopFragment shopFragment= ShopFragment.getInstance();
        ShopController shopController = new ShopController(getActivity(), presenter.getHttpCaller(), (BaseTabActivity) getActivity());
        ShopPresenter shopPresenter = new ShopPresenter(shopFragment, shopController);
        shopFragment.setPresenter(shopPresenter);
        shopFragment.setParent(parent);

        Bundle bundle = new Bundle();
        bundle.putString("seller", seller.toString());
        bundle.putString("category", getString(R.string.results_for, searchBox.getText().toString()));
        shopFragment.setArguments(bundle);

        parent.changeFragment(shopFragment);
    }

    public void showView() {
        searchAdapter.removeAll();

        if(searchBox.getText().length() <= 0)
            presenter.getRecommended();
        else
            presenter.searchProduct(searchBox.getText().toString());
    }
}
