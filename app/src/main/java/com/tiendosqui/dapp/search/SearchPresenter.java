package com.tiendosqui.dapp.search;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.HttpCalls;

import java.util.List;

/**
 * Created by Medycitas on 18/04/2017.
 */

public class SearchPresenter implements SearchContract.Presenter, SearchController.Callback {

    private SearchContract.View mp_view;
    private SearchController mp_controller;

    public SearchPresenter (@NonNull SearchContract.View searchView,
                          @NonNull SearchController searchController) {

        mp_view = searchView;
        mp_controller = searchController;
    }

    @Override
    public void start() {
        getRecommended();
    }

    @Override
    public void getRecommended() {
        mp_controller.getRecommended(this);
    }

    @Override
    public void searchProduct(String keyWord) {
        mp_controller.getProductsSearch(keyWord, this);
    }

    @Override
    public void addItem(Product product) {
        mp_controller.addItem(product);
    }

    @Override
    public void removeItem(Product product) {
        mp_controller.removeItem(product);
    }

    @Override
    public HttpCalls getHttpCaller() {
        return mp_controller.getHttpCalls();
    }

    // Callback
    @Override
    public void onNetworkConnectFailed() {
        mp_view.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        mp_view.showServerError(msg);
    }

    @Override
    public void onNoAddress(String msg) {
        mp_view.showServerError(msg);
    }

    @Override
    public void onLoadRecommendedFail(String msg) {
        mp_view.showLoadRecommendedError(msg);
    }

    @Override
    public void onLoadRecommended(List<Product> products) {
        mp_view.showRecommended(products);
    }

    @Override
    public void onSearchFail(String msg) {
        mp_view.showSearchError(msg);
    }

    @Override
    public void onSearchResults(List<Product> products) {
        mp_view.searchResult(products);
    }
}
