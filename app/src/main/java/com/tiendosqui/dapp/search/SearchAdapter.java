package com.tiendosqui.dapp.search;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.SellerSearch;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.shop.ProductViewHolder;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Divait on 25/01/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ProductViewHolder.onItemClicked  {
    public static final int PRODUCT = 0x001;
    public static final int SHOP = 0x002;

    private List<Object> items;
    private Context context;
    private OnItemClickListener listener;

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        listener.onAdd(product, holder);
    }

    @Override
    public void onRemove(Product product) {
        listener.onRemove(product);
    }

    public interface OnItemClickListener {
        void onItemClick(String data);
        void onAdd(Product product, ProductViewHolder holder);
        void onRemove(Product product);
        void onOpenShop(SellerSearch seller);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;
        public ImageView image;

        public View view;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.search_title);
            description = (TextView) view.findViewById(R.id.search_details);
            image = (ImageView) view.findViewById(R.id.search_image);

            this.view = view;

        }
    }

    public class ShopViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView description;
        public TextView time;
        public TextView seeMore;
        public ImageView image;
        public RatingBar rateBar;

        public View item;
        public LinearLayout products;

        public ShopViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.seller_name);
            description = (TextView) view.findViewById(R.id.seller_desc);
            time = (TextView) view.findViewById(R.id.seller_time);
            seeMore = (TextView) view.findViewById(R.id.see_more);
            image = (ImageView) view.findViewById(R.id.seller_image);
            rateBar = (RatingBar) view.findViewById(R.id.seller_rating);

            products = (LinearLayout) view.findViewById(R.id.products_list);
            item = view;
        }
    }

    public static SearchAdapter newInstance(Context context, OnItemClickListener listener) {
        return new SearchAdapter(new ArrayList<Object>(), context, listener);
    }

    public SearchAdapter(List<Object> items, Context context, OnItemClickListener listener) {
        this.items = items;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            default:
            case PRODUCT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_hint_search, parent, false);

                return new SearchAdapter.ViewHolder(itemView);
            case SHOP:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sellers_search, parent, false);

                return new SearchAdapter.ShopViewHolder(itemView);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        switch (holder.getItemViewType()) {
            case PRODUCT:
                final Product product = (Product) items.get(position);

                final ViewHolder h = (ViewHolder) holder;
                h.title.setText(product.getName());
                h.description.setText(product.getDescription());

                h.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(h.title.getText().toString() );
                    }
                });

                ImageLoader.loadCircleImageFromURL(product.getPicture(), h.image, R.drawable.will, context);
                break;
            case SHOP:
                final ShopViewHolder sh = (ShopViewHolder) holder;
                sh.products.removeAllViews();

                final SellerSearch seller = (SellerSearch) items.get(position);

                for (Product p : seller.getProducts()) {
                    sh.products.addView(createProduct(p));
                }

                String dPrice = seller.getMin_shipping_price();
                String minPrice = Float.toString(seller.getMin_price());

                try {
                    dPrice = NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(dPrice));
                    minPrice = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(minPrice));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                sh.name.setText(seller.getName());
                sh.description.setText(seller.getCat_shop());
                sh.time.setText(context.getString(R.string.shop_average_time,
                        minPrice,
                        dPrice,
                        seller.getAverage_deliveries())
                );

                sh.rateBar.setRating(seller.getRate());

                ImageLoader.loadImageFromURL(seller.getPicture(), sh.image, R.drawable.ic_shopping_cart, context);

                sh.seeMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onOpenShop(seller);
                    }
                });
                break;
        }


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(items.get(position) instanceof Product) {
            return PRODUCT;
        } else if(items.get(position) instanceof SellerSearch) {
            return SHOP;
        }

        return PRODUCT;
    }

    public void removeAll() {
        this.items.clear();

        this.notifyDataSetChanged();
    }

    public void rechargeProducts(List<Product> items) {
        this.items.clear();
        this.items.addAll(items);

        this.notifyDataSetChanged();
    }

    public void rechargeShops(List<SellerSearch> items) {
        this.items.clear();
        this.items.addAll(items);

        this.notifyDataSetChanged();
    }

    private View createProduct(Product product) {
        View child = ((Activity)context).getLayoutInflater().inflate(R.layout.item_product_shop, null);
        ProductViewHolder productVH = new ProductViewHolder(child, context, this);

        productVH.configureViewHolder(product);

        return child;
    }
}
