package com.tiendosqui.dapp.settings;

import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by Divait on 04/04/2017.
 *
 * Setting methods.
 */

public interface SettingsContract {
    interface View extends BaseView<Presenter> {

        void showNoUser ();

        void showUser (User user);

        void showSessionClose ();

        void showSessionCloseError ();

    }

    interface Presenter extends BasePresenter {
        void closeSession();
    }
}
