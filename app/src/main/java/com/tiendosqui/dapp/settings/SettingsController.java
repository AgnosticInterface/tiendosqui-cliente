package com.tiendosqui.dapp.settings;

import android.content.Context;

import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DataStorage;

/**
 * Created by divait on 04/04/2017.
 *
 * The controller for the settings.
 */

public class SettingsController {

    private Context sc_context;
    private HttpCalls sc_httpCalls;

    public interface Callback {

        void onCloseSessionFail (String msg);

        void onCloseSession();

        void onUserData(User user);
    }

    public SettingsController(Context context, HttpCalls httpCalls) {
        sc_context = context;

        if (httpCalls != null) {
            sc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void closeSession(Callback callback) {
        DataStorage.deleteUserData(sc_context);

        callback.onCloseSession();
    }

    public void getUserData(Callback callback) {
        callback.onUserData(DataStorage.getUser(sc_context));
    }
}
