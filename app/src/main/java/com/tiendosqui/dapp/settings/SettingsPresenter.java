package com.tiendosqui.dapp.settings;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.User;

/**
 * Created by Medycitas on 04/04/2017.
 */

public class SettingsPresenter implements SettingsContract.Presenter, SettingsController.Callback {

    private SettingsContract.View sp_view;
    private SettingsController sp_controller;

    public SettingsPresenter (@NonNull SettingsContract.View settingsView,
                          @NonNull SettingsController settingsController) {

        sp_view = settingsView;
        sp_controller = settingsController;
    }

    @Override
    public void start() {
        checkUserExist();
    }

    @Override
    public void closeSession() {
        sp_controller.closeSession(this);
    }

    // Callback
    @Override
    public void onCloseSessionFail(String msg) {

    }

    @Override
    public void onCloseSession() {
        sp_view.showNoUser();
        sp_view.showSessionClose();
    }

    @Override
    public void onUserData(User user) {
        if(user == null)
            sp_view.showNoUser();
        else
            sp_view.showUser(user);
    }

    public void checkUserExist() {
        sp_controller.getUserData(this);
    }
}
