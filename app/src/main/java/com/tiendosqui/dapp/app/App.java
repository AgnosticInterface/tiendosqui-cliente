package com.tiendosqui.dapp.app;

import com.orm.SugarApp;
import com.segment.analytics.Analytics;

/**
 * Created by divai on 8/06/2017.
 */

public class App extends SugarApp {
    private static final String WRITE_KEY = "m59N4z3pSL6IuBrVg1XALw8jGWRldLxb";

    @Override
    public void onCreate() {
        super.onCreate();
        //NukeSSLCerts.nuke();

        // Create an analytics client with the given context and Segment write key.
        Analytics analytics = new Analytics.Builder(getApplicationContext(), WRITE_KEY)
                .trackApplicationLifecycleEvents() // Enable this to record certain application events automatically!
                .recordScreenViews() // Enable this to record screen views automatically!
                .build();

        // Set the initialized instance as a globally accessible instance.
        Analytics.setSingletonInstance(analytics);

    }
}
