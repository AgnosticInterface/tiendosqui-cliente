package com.tiendosqui.dapp.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tiendosqui.dapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by divait on 30/08/2016.
 *
 * Controller for http network calls
 *
 */
public class HttpCalls {
    private Context nhc_context;

    public HttpCalls(Context context) {
        this.nhc_context = context;
    }

    public interface Callback {

        void successResponse(String response);
        void errorResponse(String error);
    }

    public void standardPostCall(String url, String tag, final Map<String, String> headers, final String body, final Callback callback) {

        // Request a string response from the provided URL.
        Utf8StringResponse deliveriesRequest = new Utf8StringResponse(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // TODO: Hacer funcional el invalid toke
                    if(response.equals("invalid_token")){
                        return;
                    }

                    callback.successResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    if(error.networkResponse != null && error.networkResponse.data!=null) {
                        try {
                            String body = new String(error.networkResponse.data,"UTF-8");
                            Log.d("Data dev", "error body: " + body );

                            JSONObject jsonError = new JSONObject(body);
                            if(jsonError.has("email")) {
                                callback.errorResponse(nhc_context.getString(R.string.error_invalid_email));
                                return;
                            }
                            if(jsonError.has("username")) {
                                callback.errorResponse(nhc_context.getString(R.string.error_email_register));
                                return;
                            }
                            if(jsonError.has("non_field_errors")) {
                                callback.errorResponse(jsonError.getString("non_field_errors"));
                                return;
                            }
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.d("Data dev", "Full error: " + error.getMessage() + ", " + error.networkResponse);
                    Log.d("Data dev", "Error: " + error);
                    callback.errorResponse(nhc_context.getString(R.string.server_error));
                }
        })
        {
            @Override
            public Map<String, String> getHeaders() {

                if(headers != null)
                    return headers;
                else
                    return new HashMap<>();
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return body == null ? null : body.getBytes("utf-8");
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }

            }
        };
        deliveriesRequest.setTag(tag);

        // Add the request to the RequestQueue.
        NetworkQueue.getInstance(nhc_context).getRequestQueue().add(deliveriesRequest);

    }

    public void standardPostCall(String url, String tag, final Map<String, String> params, final Map<String, String> headers, final Callback callback) {

        // Request a string response from the provided URL.
        Utf8StringResponse deliveriesRequest = new Utf8StringResponse(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // TODO: Hacer funcional el invalid toke
                        if(response.equals("invalid_token")){
                            return;
                        }

                        callback.successResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if(error.networkResponse != null && error.networkResponse.data!=null) {
                    try {
                        String body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Data dev", "error body: " + body );

                        JSONObject jsonError = new JSONObject(body);
                        if(jsonError.has("email")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_invalid_email));
                            return;
                        }
                        if(jsonError.has("username")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_email_register));
                            return;
                        }
                        if(jsonError.has("non_field_errors")) {
                            callback.errorResponse(jsonError.getString("non_field_errors"));
                            return;
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("Data dev", "Full error: " + error.getMessage() + ", " + error.networkResponse);
                Log.d("Data dev", "Error: " + error);
                callback.errorResponse(nhc_context.getString(R.string.server_error));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {

                if(headers != null)
                    return headers;
                else
                    return new HashMap<>();
            }

            @Override
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                return params;
            }
        };
        deliveriesRequest.setTag(tag);

        // Add the request to the RequestQueue.
        NetworkQueue.getInstance(nhc_context).getRequestQueue().add(deliveriesRequest);

    }

    public void standardGetCall (String url, String tag, final Map<String, String> params, final Map<String, String> headers, final Callback callback) {
        // TODO: Add params
        Utf8StringResponse deliveriesRequest = new Utf8StringResponse(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // TODO: Hacer funcional el invalid toke
                        if(response.equals("invalid_token")){
                            return;
                        }

                        callback.successResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if(error.networkResponse != null && error.networkResponse.data!=null) {
                    try {
                        String body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Data dev", "error body: " + body );

                        JSONObject jsonError = new JSONObject(body);
                        callback.errorResponse(jsonError.getString("non_field_errors"));
                        return;
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }

                callback.errorResponse(nhc_context.getString(R.string.server_error));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {
                return headers;
            }
        };
        deliveriesRequest.setTag(tag);

        // Add the request to the RequestQueue.
        NetworkQueue.getInstance(nhc_context).getRequestQueue().add(deliveriesRequest);
    }

    public void standardPutCall(String url, String tag, final Map<String, String> params, final Map<String, String> headers, final String body, final Callback callback) {

        // Request a string response from the provided URL.
        Utf8StringResponse deliveriesRequest = new Utf8StringResponse(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // TODO: Hacer funcional el invalid toke
                        if(response.equals("invalid_token")){
                            return;
                        }

                        callback.successResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if(error.networkResponse != null && error.networkResponse.data!=null) {
                    try {
                        String body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Data dev", "error body: " + body );

                        JSONObject jsonError = new JSONObject(body);
                        if(jsonError.has("email")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_invalid_email));
                            return;
                        }
                        if(jsonError.has("username")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_email_register));
                            return;
                        }
                        if(jsonError.has("non_field_errors")) {
                            callback.errorResponse(jsonError.getString("non_field_errors"));
                            return;
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("Data dev", "Full error: " + error.getMessage() + ", " + error.networkResponse);
                Log.d("Data dev", "Error: " + error);
                callback.errorResponse(nhc_context.getString(R.string.server_error));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {

                if(headers != null)
                    return headers;
                else
                    return new HashMap<>();
            }

            @Override
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() {
                try {
                    return body == null ? null : body.getBytes("utf-8");
                } catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }

            }
        };
        deliveriesRequest.setTag(tag);

        // Add the request to the RequestQueue.
        NetworkQueue.getInstance(nhc_context).getRequestQueue().add(deliveriesRequest);

    }

    public void standardPutCall(String url, String tag, final Map<String, String> params, final Map<String, String> headers, final Callback callback) {

        // Request a string response from the provided URL.
        Utf8StringResponse deliveriesRequest = new Utf8StringResponse(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // TODO: Hacer funcional el invalid toke
                        if(response.equals("invalid_token")){
                            return;
                        }

                        callback.successResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                if(error.networkResponse != null && error.networkResponse.data!=null) {
                    try {
                        String body = new String(error.networkResponse.data,"UTF-8");
                        Log.d("Data dev", "error body: " + body );

                        JSONObject jsonError = new JSONObject(body);
                        if(jsonError.has("email")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_invalid_email));
                            return;
                        }
                        if(jsonError.has("username")) {
                            callback.errorResponse(nhc_context.getString(R.string.error_email_register));
                            return;
                        }
                        if(jsonError.has("non_field_errors")) {
                            callback.errorResponse(jsonError.getString("non_field_errors"));
                            return;
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("Data dev", "Full error: " + error.getMessage() + ", " + error.networkResponse);
                Log.d("Data dev", "Error: " + error);
                callback.errorResponse(nhc_context.getString(R.string.server_error));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() {

                if(headers != null)
                    return headers;
                else
                    return new HashMap<>();
            }

            @Override
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                return params;
            }
        };
        deliveriesRequest.setTag(tag);

        // Add the request to the RequestQueue.
        NetworkQueue.getInstance(nhc_context).getRequestQueue().add(deliveriesRequest);

    }

    public Context getContext() {
        return nhc_context;
    }
}
