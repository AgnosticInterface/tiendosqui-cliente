package com.tiendosqui.dapp.network;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.utils.CircleTransform;

/**
 * Created by Ditvait on 06/02/2017.
 *
 *  A class to call everything related with images in the cloud.
 */

public class ImageLoader {
    public static void loadImageFromURL(String url, ImageView imageView, int error_drawable, Context context) {
        String finalUrl = url.startsWith("/")?url:"/"+url;

        Picasso.with(context)
                .load(NetworkInfo.getImageURL() + finalUrl)
                .placeholder(R.drawable.circular_progress)
                .error(error_drawable)
                .into(imageView);
    }

    public static void loadCircleImageFromURL(String url, ImageView imageView, int error_drawable, Context context){
        if(url == null || imageView == null)
            return;

        String finalUrl = url.startsWith("/")?url:"/"+url;

        Picasso.with(context)
                .load(NetworkInfo.getImageURL() + finalUrl)
                .transform(new CircleTransform())
                .placeholder(R.drawable.circular_progress)
                .error(error_drawable)
                .into(imageView);
    }
 }
