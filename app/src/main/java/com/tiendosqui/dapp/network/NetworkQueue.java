package com.tiendosqui.dapp.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Divait on 25/08/2016.
 *
 * All the Http request are made in this queue
 *
 */
public class NetworkQueue {
    private static NetworkQueue nq_Instance;
    private RequestQueue nq_RequestQueue;
    private ImageLoader nq_ImageLoader;
    private static Context nt_Context;

    private NetworkQueue(Context context) {
        nt_Context = context;

        nq_RequestQueue = getRequestQueue ();

        // Network element to get image async
        nq_ImageLoader = new ImageLoader( nq_RequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<>(20);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);
            }
        });
    }

    // Get the instance of the singleton to put all the request in the same queue
    public static synchronized NetworkQueue getInstance (Context context) {
        if(nq_Instance == null) {
            nq_Instance = new NetworkQueue(context);
        }

        return  nq_Instance;
    }

    // Get the network queue
    public RequestQueue getRequestQueue() {
        if (nq_RequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the Activity or BroadcastReceiver if someone passes one in.
            nq_RequestQueue = Volley.newRequestQueue(nt_Context.getApplicationContext());
        }

        return nq_RequestQueue;
    }

    // Add a query to queue
    public <T> void addToQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return nq_ImageLoader;
    }

}
