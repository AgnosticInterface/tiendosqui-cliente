package com.tiendosqui.dapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.map.MapActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.orders.OrdersFragment;
import com.tiendosqui.dapp.search.SearchFragment;
import com.tiendosqui.dapp.shop.ShopFragment;
import com.tiendosqui.dapp.shop_car.CarFragment;
import com.tiendosqui.dapp.tabs.TabFragment;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import java.util.Calendar;

public class MainActivity extends BaseTabActivity {

    private TabsAdapter adapter;
    private ViewPager vp;

    private HttpCalls ma_httpCalls;

    boolean doubleClick = false;
    private long clickTime;

    private boolean oldUse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(oldUse) {
            oldUse = false;

            startActivity(new Intent(this, MapActivity.class));
            finish();
        }

        Intent intent = getIntent();
        int tab = intent.getIntExtra("tab", 0); // TODO

        if(tab == 3){ // TODO order id to fragment
            long oId = intent.getLongExtra("order_id", 0);

        }

        // Create the http handler
        ma_httpCalls = new HttpCalls(this);

        adapter = new TabsAdapter(getSupportFragmentManager(), ma_httpCalls, this, this);
        vp = (ViewPager) findViewById(R.id.container);
        vp.setAdapter(adapter);
        vp.setOffscreenPageLimit(4);

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                TabFragment fragment = adapter.getFragment(position);

                if(fragment == null) {
                    setToolbar(false, getToolbarTitle(position), R.color.colorAccentSecondary);
                    return;
                }

                switch (position){

                    case TabFragment.HOME:
                        if (fragment.size() == 2) {
                            //showAppBar();
                            ShopFragment shopFragment = (ShopFragment) fragment.getFragment(1);
                            //shopFragment.rechargeProducts();
                        }
                        break;
                    case TabFragment.CAR:
                        if(fragment.size() == 1) {
                            CarFragment carFragment = (CarFragment) fragment.getFragment(0);
                            carFragment.showProducts();
                        }
                        break;
                    case TabFragment.SEARCH:
                        if(fragment.size() == 1) {
                            SearchFragment searchFragment = (SearchFragment) fragment.getFragment(0);
                            searchFragment.showView();
                        }
                        break;

                    case TabFragment.FAVS:
                        if(DataStorage.getUser(MainActivity.this) == null) {
                            OrdersFragment orderFragment = (OrdersFragment) fragment.getFragment(0);
                            orderFragment.showNoOrders();
                        }
                        break;
                }

                setTabToolbar(position, fragment);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        initTabs(vp);
        vp.setCurrentItem(tab);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateCar(DataStorage.getOrders(this));
        setToolbar(false);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //startActivity(new Intent(this, LaunchActivity.class));
        //finish();
    }

    @Override
    protected void onStop() {
        super.onStop();

        oldUse = true;
        //Intent intent = new Intent(this, LaunchActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        TabFragment currentFragment = adapter.getFragment(vp.getCurrentItem());

        if(currentFragment != null && vp.getCurrentItem() == 0 && currentFragment.size() == 3)
            ((ShopFragment)currentFragment.getFragment(1)).back = true;

        if (currentFragment == null || !currentFragment.getChildFragmentManager().popBackStackImmediate()) {
            if (doubleClick) {

                if (Calendar.getInstance().getTimeInMillis() - clickTime < 700) {
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                    //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else
                    doubleClick = false;
            }
            if (!doubleClick) {
                clickTime = Calendar.getInstance().getTimeInMillis();
                final Toast toast = Toast.makeText(this, getString(R.string.close), Toast.LENGTH_SHORT);
                toast.show();
                doubleClick = true;

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 600);
            }

        } else {

            currentFragment.removeBackItem();
            setTabToolbar(vp.getCurrentItem(), currentFragment);
        }

    }

    @Override
    public void onReSelected(int position) {
        super.onReSelected(position);

        TabFragment currentFragment = adapter.getFragment(vp.getCurrentItem());

        if (currentFragment != null && currentFragment.getChildFragmentManager().popBackStackImmediate()) {
            currentFragment.removeBackItem();
            setTabToolbar(position, currentFragment);
        }
    }
}
