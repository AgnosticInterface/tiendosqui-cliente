package com.tiendosqui.dapp.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.tiendosqui.dapp.search.SearchController;
import com.tiendosqui.dapp.search.SearchPresenter;
import com.tiendosqui.dapp.shop_car.CarController;
import com.tiendosqui.dapp.shop_car.CarFragment;
import com.tiendosqui.dapp.shop_car.CarPresenter;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.orders.OrdersController;
import com.tiendosqui.dapp.orders.OrdersFragment;
import com.tiendosqui.dapp.orders.OrdersPresenter;
import com.tiendosqui.dapp.search.SearchFragment;
import com.tiendosqui.dapp.settings.SettingsController;
import com.tiendosqui.dapp.settings.SettingsFragment;
import com.tiendosqui.dapp.settings.SettingsPresenter;
import com.tiendosqui.dapp.tabs.TabFragment;
import com.tiendosqui.dapp.utils.EmptyFragment;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

/**
 * Created by divait on 19/01/2017.
 *
 * Main tab manager.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {
    public static final int HOME      = 0;
    public static final int SEARCH    = 1;
    public static final int CAR       = 2;
    public static final int FAV       = 3;
    public static final int SETTINGS  = 4;

    private HttpCalls httpCalls;
    private Context context;

    private BaseTabActivity activity;

    private SparseArray<Fragment> mapPage;
    private SparseArray<Fragment> mapPageInstance;

    public TabsAdapter(FragmentManager fm, HttpCalls httpCalls, Context context, BaseTabActivity activity) {
        super(fm);

        this.httpCalls = httpCalls;
        this.context = context;
        this.activity = activity;

        mapPage = new SparseArray<>();
        mapPageInstance = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        if(mapPage.get(position) != null) {
            return mapPage.get(position);
        }

        TabFragment tab;
        switch (position) {
            default:
                Fragment fragment = EmptyFragment.getInstance();

                mapPage.put(position, fragment);
                return fragment;
            case HOME:
                MainFragment mainFragment = MainFragment.getInstance();
                MainController mainController = new MainController(context, httpCalls);
                MainPresenter mainPresenter = new MainPresenter(mainFragment, mainController);
                mainFragment.setPresenter(mainPresenter);

                tab = TabFragment.getNewInstance(mainFragment, position);
                mapPage.put(position, tab);
                return tab;
            case SEARCH:
                SearchFragment searchFragment = SearchFragment.getInstance();
                SearchController searchController = new SearchController(context, httpCalls, activity);
                SearchPresenter searchPresenter = new SearchPresenter(searchFragment, searchController);
                searchFragment.setPresenter(searchPresenter);

                tab = TabFragment.getNewInstance(searchFragment, position);
                mapPage.put(position, tab);
                return tab;
            case CAR:
                CarFragment carFragment= CarFragment.getInstance();
                CarController carController = new CarController(context, activity);
                CarPresenter shopPresenter = new CarPresenter(carFragment, carController);
                carController.setListener(shopPresenter);
                carFragment.setPresenter(shopPresenter);

                tab = TabFragment.getNewInstance(carFragment, position);
                mapPage.put(position, tab);
                return tab;

            case FAV:
                OrdersFragment ordersFragment= OrdersFragment.getInstance();
                OrdersController ordersController = new OrdersController(context, httpCalls);
                OrdersPresenter ordersPresenter = new OrdersPresenter(ordersFragment, ordersController);
                ordersFragment.setPresenter(ordersPresenter);

                tab = TabFragment.getNewInstance(ordersFragment, position);
                mapPage.put(position, tab);
                return tab;

            case SETTINGS:
                SettingsFragment settingsFragment = SettingsFragment.getInstance();
                SettingsController settingsController = new SettingsController(context, httpCalls);
                SettingsPresenter settingsPresenter = new SettingsPresenter(settingsFragment, settingsController);
                settingsFragment.setPresenter(settingsPresenter);

                tab = TabFragment.getNewInstance(settingsFragment, position);
                mapPage.put(position, tab);
                return tab;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if(mapPageInstance.get(position) != null)
            return mapPageInstance.get(position);
        else {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            mapPageInstance.put(position, fragment);

            return fragment;
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mapPageInstance.delete(position);
    }

    @Override
    public int getCount() {
        return 5;
    }

    public TabFragment getFragment(int position) {
        try {
            return (TabFragment) mapPage.get(position);
        }catch (ClassCastException ex) {
            ex.printStackTrace();
            return null;
        }

    }

}
