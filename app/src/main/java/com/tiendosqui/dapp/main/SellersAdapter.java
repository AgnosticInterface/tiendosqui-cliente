package com.tiendosqui.dapp.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.network.ImageLoader;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Divait on 24/01/2017.
 */

public class SellersAdapter extends RecyclerView.Adapter<SellersAdapter.ViewHolder> {

    private List<Seller> sellers;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Seller seller);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView description;
        public TextView time;
        public ImageView image;
        public RatingBar rateBar;

        public View item;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.seller_name);
            description = (TextView) view.findViewById(R.id.seller_desc);
            time = (TextView) view.findViewById(R.id.seller_time);
            image = (ImageView) view.findViewById(R.id.seller_image);
            rateBar = (RatingBar) view.findViewById(R.id.seller_rating);

            item = view;
        }
    }

    public static SellersAdapter newInstance(Context context, OnItemClickListener listener) {
        return new SellersAdapter(new ArrayList<Seller>(), context, listener);
    }


    public SellersAdapter(List<Seller> sellers, Context context, OnItemClickListener listener) {
        this.sellers = sellers;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sellers, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Seller seller = sellers.get(position);

        String dPrice = seller.getMin_shipping_price();
        String minPrice = Float.toString(seller.getMin_price());

        try {
            dPrice = NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(dPrice));
            minPrice = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(minPrice));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.d("Data dev", "numbers: " + dPrice + " " + minPrice);

        holder.name.setText(seller.getName());
        holder.description.setText(seller.getCat_shop());
        holder.time.setText(context.getString(R.string.shop_average_time,
                minPrice,
                dPrice,
                seller.getAverage_deliveries())
        );

        holder.rateBar.setRating(seller.getRate());

        ImageLoader.loadImageFromURL(
                sellers.get(position).getPicture(),
                holder.image,
                R.drawable.ic_shopping_cart, context
        );

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(sellers.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellers.size();
    }

    public void rechargeShops(List<Seller> sellers) {
        this.sellers.clear();
        this.sellers.addAll(sellers);

        this.notifyDataSetChanged();
    }
}
