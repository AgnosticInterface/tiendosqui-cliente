package com.tiendosqui.dapp.main;

import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The contract for main fragment view.
 */

public interface MainContract {

    interface View extends BaseView<MainContract.Presenter> {

        void showShops (List<Seller> sellers);

        void showLoadShopsError (String msg);

        void showServerError (String msg);

        void showNetworkError(boolean show);
    }

    interface Presenter extends BasePresenter {
        void getShops();

        HttpCalls getHttpCaller();
    }
}
