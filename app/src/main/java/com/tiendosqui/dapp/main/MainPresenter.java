package com.tiendosqui.dapp.main;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.network.HttpCalls;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The presenter for the main view.
 */

public class MainPresenter implements MainContract.Presenter, MainController.Callback {

    private MainContract.View mp_view;
    private MainController mp_controller;

    public MainPresenter (@NonNull MainContract.View mainView,
                                @NonNull MainController mainController) {

        mp_view = mainView;
        mp_controller = mainController;
    }

    @Override
    public void getShops() {
        mp_controller.getShops(this);
    }

    @Override
    public HttpCalls getHttpCaller() {
        return mp_controller.getMc_httpCalls();
    }

    @Override
    public void start() {
        getShops();
    }

    @Override
    public void onNetworkConnectFailed() {
        mp_view.showNetworkError(true);
    }

    @Override
    public void onServerError(String msg) {
        mp_view.showServerError(msg);
    }

    @Override
    public void onLoadShopsFail(String msg) {
        mp_view.showLoadShopsError(msg);
    }

    @Override
    public void onNoAddress(String msg) {
        // TODO: show error
    }

    @Override
    public void onLoadShops(List<Seller> sellers) {
        mp_view.showShops(sellers);
    }

}
