package com.tiendosqui.dapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.shop.ShopController;
import com.tiendosqui.dapp.shop.ShopFragment;
import com.tiendosqui.dapp.shop.ShopPresenter;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import java.util.List;

/**
 * Created by Divait on 19/01/2017.
 *
 * The main activity that manage the app and the tabs.
 */

public class MainFragment extends TabChildFragment implements MainContract.View, SellersAdapter.OnItemClickListener {

    private RecyclerView sellersList;
    private View emptyView;

    private SellersAdapter sellersAdapter;

    private MainContract.Presenter presenter;

    public MainFragment() {
        // Require empty constructor
    }

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sellersAdapter = SellersAdapter.newInstance(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        sellersList = root.findViewById(R.id.list_sellers);

        emptyView = root.findViewById(R.id.willy_view);
        ((TextView)emptyView.findViewById(R.id.willy_message)).setText(getString(R.string.empty_stores));

        sellersList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        sellersList.setAdapter(sellersAdapter);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            presenter.start();
        }catch (NullPointerException ex) {
            Intent intent = getActivity().getIntent();
            getActivity().finish();
            startActivity(intent);
        }
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showShops(List<Seller> sellers) {
        emptyView.setVisibility(View.GONE);
        sellersList.setVisibility(View.VISIBLE);
        sellersAdapter.rechargeShops(sellers);
    }

    @Override
    public void showLoadShopsError(String msg) {
        emptyView.setVisibility(View.VISIBLE);
        sellersList.setVisibility(View.GONE);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError(String msg) {
        emptyView.setVisibility(View.VISIBLE);
        sellersList.setVisibility(View.GONE);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError(boolean show) {
        emptyView.setVisibility(View.VISIBLE);
        sellersList.setVisibility(View.GONE);
        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(Seller seller) {
        ShopFragment shopFragment= ShopFragment.getInstance();
        ShopController shopController = new ShopController(getActivity(), presenter.getHttpCaller(), (BaseTabActivity) getActivity());
        ShopPresenter shopPresenter = new ShopPresenter(shopFragment, shopController);
        shopFragment.setPresenter(shopPresenter);
        shopFragment.setParent(parent);

        Bundle bundle = new Bundle();
        bundle.putString("seller", seller.toString());
        shopFragment.setArguments(bundle);

        parent.changeFragment(shopFragment);

        BaseTabActivity activity = (BaseTabActivity) getActivity();
        activity.showAppBar();
        activity.setToolbar(true, seller.getName(), R.color.colorAccentSecondary);

    }

    public MainContract.Presenter getPresenter() {
        return presenter;
    }
}
