package com.tiendosqui.dapp.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.network.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Divait on 24/01/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private List<Category> categories;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Category category);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;

        public View item;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.cat_title);
            image = (ImageView) view.findViewById(R.id.cat_image);
            item = view;
        }
    }

    public static CategoryAdapter newInstance(Context context, CategoryAdapter.OnItemClickListener listener) {
        return new CategoryAdapter(new ArrayList<Category>(), context, listener);
    }


    public CategoryAdapter(List<Category> categories, Context context, OnItemClickListener listener) {
        this.categories = categories;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Category category = categories.get(position);

        holder.title.setText(category.getTitle());
        ImageLoader.loadCircleImageFromURL(category.getImage(), holder.image, R.drawable.will, context);

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(category);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void rechargeCategories(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);

        this.notifyDataSetChanged();
    }
}
