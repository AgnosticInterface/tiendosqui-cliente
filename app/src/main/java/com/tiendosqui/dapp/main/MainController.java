package com.tiendosqui.dapp.main;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The controller for the main fragment.
 */

public class MainController {

    private Context mc_context;
    private HttpCalls mc_httpCalls;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onLoadShopsFail (String msg);

        void onNoAddress (String msg);

        void onLoadShops(List<Seller> sellers);

    }

    public MainController(Context context, HttpCalls httpCalls) {
        mc_context = context;

        if (httpCalls != null) {
            mc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getShops(Callback callback) {
        Address address = DataStorage.getAddress(mc_context);

        if(address == null) {
            callback.onNoAddress("No address");
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(mc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadShops(address.getLatitude(), address.getLongitude(), callback);
    }

    private void attemptLoadShops(double latitude, double longitude, final Callback callback) {
        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.SHOPS_POST_PARAMETER_POINT, longitude + " " + latitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Data dev", "body: " + body.toString());

        mc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SHOPS_GEO_URL), NetworkInfo.SHOPS_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data shops: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Data shops: " +json);
        } catch (JSONException e) {
            callback.onServerError(mc_context.getString(R.string.error_loading_shops));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            Seller[] sellers = gson.fromJson(json.toString(), Seller[].class);

            List<Seller>  sellersList = Arrays.asList(sellers);
            for (Seller seller : sellersList) {
                seller.setPayments();
                SugarRecord.save(seller);
            }

            callback.onLoadShops(sellersList);
        } catch (Exception ex) {
            callback.onLoadShopsFail(mc_context.getString(R.string.error_loading_shops));
            ex.printStackTrace();
        }
    }

    public HttpCalls getMc_httpCalls() {
        return mc_httpCalls;
    }
}
