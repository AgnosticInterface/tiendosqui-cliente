package com.tiendosqui.dapp.checkout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Order;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by divait on 15/05/2017.
 */

public class SubOrderViewHolder extends RecyclerView.ViewHolder {
    private final TextView shopView;
    private final TextView subTitleView;
    private final TextView priceView;

    private Order order;
    private Context context;

    public SubOrderViewHolder(View v, final Context context) {
        super(v);
        this.context = context;

        shopView = (TextView) v.findViewById(R.id.txt_shop_name);
        subTitleView = (TextView) v.findViewById(R.id.txt_products_subtotal);
        priceView = (TextView) v.findViewById(R.id.txt_price);

    }

    public TextView getShopView() {
        return shopView;
    }

    public TextView getSubTitleView() {
        return subTitleView;
    }

    public TextView getPriceView() {
        return priceView;
    }

    public void configureViewHolder (Order order, int type) {
        this.order = order;

        String subtitle = "";
        String name = "";
        String cost = "$";

        if(type == OrderViewHolder.ORDER) {
            subtitle = context.getString(R.string.products_count, order.getProductsCount());
            name = order.getSellerName();
            cost = context.getString(R.string.price, NumberFormat.getNumberInstance(Locale.US).format(order.getSubTotalPrice()));


        } else if(type == OrderViewHolder.PRICE) {
            String dPrice = order.getSellerDeliveryCost();

            try {
                dPrice = NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(dPrice));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            subtitle = context.getString(R.string.delivery_average, order.getSellerDeliveryTime());
            name = order.getSellerName();
            cost = context.getString(R.string.price, dPrice);
        }

        // Get element from your dataset at this position and replace the contents of the view with that element
        getSubTitleView().setText(subtitle);
        getShopView().setText(name);
        getPriceView().setText(cost);
    }
}
