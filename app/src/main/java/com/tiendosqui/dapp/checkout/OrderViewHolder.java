package com.tiendosqui.dapp.checkout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tiendosqui.dapp.R;

/**
 * Created by divait on 26/03/2017.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder {
    static final int ORDER = 0x002;
    static final int PRICE = 0x003;

    private Context context;

    private final TextView titleView;
    private final LinearLayout ordersView;


    private final View view;

    public OrderViewHolder(View v, final Context context) {
        super(v);
        this.view = v;
        this.context = context;

        titleView = (TextView) v.findViewById(R.id.txt_products_total);
        ordersView = (LinearLayout) v.findViewById(R.id.data_container);

    }

    public TextView getTitleView() {
        return titleView;
    }

    public LinearLayout getOrdersView() {
        return ordersView;
    }

    public View getView() {
        return view;
    }

    public void configureViewHolder (int count, int type) {

        String title = "";

        if(type == ORDER) {
            title = context.getString(R.string.subtotal_order, count);


        } else if(type ==  PRICE) {
            title = context.getString(R.string.delivery_cost);
        }

        // Get element from your dataset at this position and replace the contents of the view with that element
        getTitleView().setText(title);
    }

}
