package com.tiendosqui.dapp.checkout;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tiendosqui.dapp.R;

/**
 * Created by divai on 31/03/2017.
 */

public class TotalViewHolder extends RecyclerView.ViewHolder {

    public TextView total;

    public TotalViewHolder(View view) {
        super(view);
        total = (TextView) view.findViewById(R.id.txt_full_price);

    }

    public TextView getTotal() {
        return total;
    }
}
