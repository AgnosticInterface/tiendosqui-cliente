package com.tiendosqui.dapp.checkout;

import android.support.annotation.NonNull;

/**
 * Created by divait on 31/03/2017.
 */

public class CheckoutPresenter implements CheckoutContract.Presenter, CheckoutController.Callback {

    private CheckoutContract.View mp_view;
    private CheckoutController mp_controller;

    public CheckoutPresenter (@NonNull CheckoutContract.View checkoutView,
                          @NonNull CheckoutController checkoutController) {

        mp_view = checkoutView;
        mp_controller = checkoutController;
    }

    @Override
    public void getData() {
        mp_view.showOrder(mp_controller.getData());
    }

    @Override
    public void createOrder() {
        mp_controller.createOrder(this);
    }

    @Override
    public void start() {
        getData();
    }

    @Override
    public void onNetworkConnectFailed() {
        mp_view.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        mp_view.showServerError(msg);
    }

    @Override
    public void onNoUser(String msg) {
        mp_view.showErrorOrderCreation(msg);
    }

    @Override
    public void onNoOrder(String msg) {
        mp_view.showErrorOrderCreation(msg);
    }

    @Override
    public void onNoPayment(String msg) {
        mp_view.showErrorOrderCreation(msg);
    }

    @Override
    public void onCreateOrderFail(String msg) {
        mp_view.showErrorOrderCreation(msg);
    }

    @Override
    public void onCreateOrder(String msg) {
        mp_view.showOrderCreate(msg);
    }
}
