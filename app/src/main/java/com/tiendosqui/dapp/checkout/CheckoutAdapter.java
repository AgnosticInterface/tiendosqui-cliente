package com.tiendosqui.dapp.checkout;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.AddressViewHolder;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by divai on 31/03/2017.
 */

public class CheckoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AddressViewHolder.OnAddressClickedListener {

    static final int OTHER = 0x000;
    static final int ADDRESS = 0x001;
    static final int ORDER = 0x002;
    static final int PRICE = 0x003;
    static final int TOTAL = 0x004;
    static final int PAYMENT = 0x005;

    private List<Object> objects;
    private Context context;
    private CheckoutAdapter.onItemClicked listener;

    public class TitleViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public TitleViewHolder(View view) {
            super(view);
            title = (TextView) view;

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        public TextView text;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            this.text = (TextView) view.findViewById(R.id.txt_payment_name);


        }

        public View getView() {
            return view;
        }
    }

    public interface onItemClicked {
        public void onAddress();
        public void onOrder();
        public void onDeliveryCost();
        public void onPayment();
        public void onDiscount();
    }

    public static CheckoutAdapter newInstance(Context context, CheckoutAdapter.onItemClicked listener) {
        return new CheckoutAdapter(new ArrayList<>(), context, listener);
    }

    public CheckoutAdapter(List<Object> objects, Context context, CheckoutAdapter.onItemClicked listener) {
        this.objects = objects;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        switch (viewType) {
            case ADDRESS:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity_address, parent, false);

                return new AddressViewHolder(v, this);
            case ORDER:
            case PRICE:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity_checkout, parent, false);
                return new OrderViewHolder(v, context);
            case TOTAL:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity_checkout_total, parent, false);
                return new TotalViewHolder(v);
            case PAYMENT:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity_checkout_pay, parent, false);
                return new ViewHolder(v); // TODO Change
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_title, parent, false);
                return new CheckoutAdapter.TitleViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ADDRESS:
                Address address = (Address) objects.get(position);
                ((AddressViewHolder) holder).getTitleView().setText(address.getName());
                ((AddressViewHolder) holder).getDescriptionView().setText(address.toAddressString());
                ((AddressViewHolder) holder).getAddressImage().setImageDrawable(context.getResources().getDrawable(address.getImageId()));
                ((AddressViewHolder) holder).getAddressImage().setTag(address.getImageId());
                ((AddressViewHolder) holder).setAddress(new AddressItem(address.getId(), address.getName(), address.toAddressString(), address.getImageId(), address));
                break;
            case ORDER:
                MainOrder orders = (MainOrder) objects.get(position);
                ((OrderViewHolder) holder).getOrdersView().removeAllViews();

                int count = 0;

                for (Order order : orders.getOrders()) {
                    ((OrderViewHolder) holder).getOrdersView().addView(createSubItem(order, ORDER));
                    count += order.getProductsCount();
                }

                ((OrderViewHolder) holder).configureViewHolder(count, ORDER);
                ((OrderViewHolder) holder).getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onOrder();
                    }
                });
                break;
            case PRICE:
                MainOrder orders1 = (MainOrder) objects.get(position);
                ((OrderViewHolder) holder).getOrdersView().removeAllViews();

                for(Order order : orders1.getOrders()) {
                    ((OrderViewHolder)holder).getOrdersView().addView(createSubItem(order, PRICE));
                }

                ((OrderViewHolder)holder).configureViewHolder(0, PRICE);
                ((OrderViewHolder)holder).getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onDeliveryCost();
                    }
                });
                break;
            case TOTAL:
                MainOrder orders2 = (MainOrder) objects.get(position);
                float total = 0.0f;
                String totalTxt = "";

                for (Order ord : orders2.getOrders()) {
                    total += ord.getTotalPrice() ;
                }
                try {
                    totalTxt = NumberFormat.getNumberInstance(Locale.US).format(total);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                ((TotalViewHolder)holder).getTotal().setText(context.getString(R.string.price, totalTxt));
                break;
            case PAYMENT:
                MainOrder orders3 = (MainOrder) objects.get(position);
                String payment = "";
                for (Order ord : orders3.getOrders()) {
                    if(ord.getPayMethod() == null || ord.getPayMethod().length() <= 0) {
                        payment = context.getString(R.string.select_pay_method);
                        break;
                    }

                    payment += ord.getSellerName() + ": " + ord.getPayMethod() + " | ";
                }

                ((ViewHolder)holder).text.setText(payment);

                ((ViewHolder)holder).getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onPayment();
                    }
                });
                break;
            default:
                String title = (String) objects.get(position);
                ((CheckoutAdapter.TitleViewHolder)holder).title.setText(title);
                break;
        }


    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof MainOrder) {
            return ((MainOrder) objects.get(position)).getType();
        } else if(objects.get(position) instanceof Address) {
            return ADDRESS;
        }

        return OTHER;
    }

    public void rechargeProducts( List<Object> objects) {
        this.objects.clear();
        this.objects.addAll(objects);

        notifyDataSetChanged();
    }

    private View createSubItem(Order order, int type) {
        View child = ((Activity)context).getLayoutInflater().inflate(R.layout.sub_item_checkout, null);
        SubOrderViewHolder subVH = new SubOrderViewHolder(child, context);

        subVH.configureViewHolder(order, type);

        return child;
    }

    @Override
    public void onAddressClicked(AddressItem addressItem, int imageId) {
        listener.onAddress();
    }
}
