package com.tiendosqui.dapp.checkout;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by divai on 31/03/2017.
 */

public interface CheckoutContract {

    interface View extends BaseView<Presenter> {

        void showOrder (List<Object> objects);

        void showOrderCreate (String msg);

        void showErrorOrderCreation (String msg);

        void showServerError (String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void getData();

        void createOrder();
    }
}
