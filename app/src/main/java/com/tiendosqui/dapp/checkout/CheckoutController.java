package com.tiendosqui.dapp.checkout;

import android.content.Context;
import android.util.Log;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.json.OrderJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by divait on 31/03/2017.
 */

public class CheckoutController {

    private Context context;
    private HttpCalls httpCalls;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onNoUser(String msg);

        void onNoOrder (String msg);

        void onNoPayment (String msg);

        void onCreateOrderFail (String msg);

        void onCreateOrder(String msg);
    }

    public CheckoutController(Context context, HttpCalls httpCalls) {
        this.context = context;

        if (httpCalls != null) {
            this.httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public List<Object> getData(){
        List<Object> data = new ArrayList<>();
        Address address = DataStorage.getAddress(context);

        MainOrder orders = DataStorage.getOrders(context);
        if(orders == null)
            return data;

        orders.setType(CheckoutAdapter.ORDER);

        MainOrder orders1 = new MainOrder(orders);
        orders1.setType(CheckoutAdapter.PRICE);

        MainOrder orders2 = new MainOrder(orders);
        orders2.setType(CheckoutAdapter.TOTAL);

        MainOrder orders3 = new MainOrder(orders);
        orders3.setType(CheckoutAdapter.PAYMENT);

        data.add(context.getString(R.string.delivery_address));
        data.add(address);
        data.add(context.getString(R.string.details_order));
        data.add(orders);
        data.add(orders1);
        data.add(orders2);
        data.add(orders3);

        return data;
    }

    public void createOrder(Callback callback) {
        long userId = DataStorage.getUserId(context);
        MainOrder orders = DataStorage.getOrders(context);

        if(userId < 0) {
            callback.onNoUser("No user");
            return;
        }

        if(orders == null) {
            callback.onNoOrder("No order");
            return;
        }

        for(Order order : orders.getOrders()) {
            if (order.getPayMethod() == null || order.getPayMethod().length() <= 0) {
                callback.onNoPayment(context.getString(R.string.error_payment_msg));
                return;
            }
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptCreateOrder(userId, orders, callback);
    }

    private void attemptCreateOrder(long userId, MainOrder mainOrder, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(context));

        JSONObject body = new JSONObject();
        try {
            JSONObject user = new JSONObject();
            user.put("id", userId);
            user.put("address_id", DataStorage.getAddress(context).getId());

            JSONArray orders = new JSONArray();
            for (Order order : mainOrder.getOrders()) {
                JSONObject o = new JSONObject(OrderJson.fromOrder(order).toString());
                orders.put(o);
            }

            body.put(NetworkInfo.ORDER_POST_PARAMETER_USER, user);
            body.put(NetworkInfo.ORDER_POST_PARAMETER_ORDER, orders);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Data dev", "body: " + body.toString());

        httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.ORDER_URL), NetworkInfo.ORDER_TAG, headers, "[" + body.toString() + "]", new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data order response: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, Callback callback) {
        JSONObject json;
        try {
            json = new JSONObject(response);
            Log.d("Data dev", "Order response Json: " +json);

            if(json.has("detail")){
                if(json.getString("detail").trim().equals("orden creada con exito!")) {
                    DataStorage.deleteOrders(context);
                    callback.onCreateOrder(json.getString("detail"));
                    return;
                }
            }
        } catch (JSONException e) {
            callback.onServerError(context.getString(R.string.error_creating_order));
            e.printStackTrace();
            return;
        }

        callback.onCreateOrderFail(context.getString(R.string.error_creating_order));
    }
}
