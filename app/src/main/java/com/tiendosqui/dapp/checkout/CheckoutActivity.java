package com.tiendosqui.dapp.checkout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.OldAddressActivity;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.payment.PayActivity;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseActivity;

import java.util.List;

/**
 * Created by divait on 31/03/2017.
 */

public class CheckoutActivity extends BaseActivity implements CheckoutAdapter.onItemClicked, CheckoutContract.View {
    private HttpCalls httpCalls;

    private RecyclerView checkoutList;
    private CheckoutAdapter checkoutAdapter;
    LinearLayoutManager layoutManager;

    private SeekBar btnSlide;
    private TextView slideText;

    private View onProcessView;

    private CheckoutContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        setToolbar(true);

        checkoutList = (RecyclerView) findViewById(R.id.list_checkout);

        btnSlide = (SeekBar) findViewById(R.id.slide_button);
        slideText = (TextView) findViewById(R.id.slide_text);

        onProcessView = findViewById(R.id.on_progress_view);

        btnSlide.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 95) {
                    seekBar.setEnabled(false);
                    onProcessView.setVisibility(View.VISIBLE);

                    presenter.createOrder();
                } else {
                    enableSlideButton();
                    seekBar.setThumb(getResources().getDrawable(R.drawable.willy_settings));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                slideText.setVisibility(View.GONE);

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if(progress>95){
                    // seekBar.setThumb(getResources().getDrawable(R.drawable.ic_settings));
                }

            }
        });

        checkoutAdapter =CheckoutAdapter.newInstance(this, this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        checkoutList.setLayoutManager(layoutManager);
        checkoutList.setAdapter(checkoutAdapter);

        // Create the http handler
        httpCalls = new HttpCalls(this);

        CheckoutController mainController = new CheckoutController(this, httpCalls);
        CheckoutPresenter mainPresenter = new CheckoutPresenter(this, mainController);
        setPresenter(mainPresenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    public void showOrder(List<Object> objects) {
        if(objects.size() < 3) {
            Toast.makeText(this, getString(R.string.error_no_order), Toast.LENGTH_LONG).show();
            finish();
        }
        checkoutAdapter.rechargeProducts(objects);
    }

    @Override
    public void showOrderCreate(String msg) {

        UtilDialog.showDialog(this,
                getString(R.string.end_process_title),
                getString(R.string.end_process_msg),
                R.string.ok,
                endOrderCreation(msg),
                -1,
                null
        );
    }

    @Override
    public void showErrorOrderCreation(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        enableSlideButton();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        enableSlideButton();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
        enableSlideButton();
    }

    @Override
    public void setPresenter(CheckoutContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void onAddress() {
        UtilDialog.showDialog(this,
                getString(R.string.support_address_title),
                getString(R.string.support_address_msg),
                R.string.next_action,
                deleteOrderClick(),
                R.string.dialog_cancel,
                null
        );

    }

    public DialogInterface.OnClickListener deleteOrderClick() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataStorage.deleteOrders(CheckoutActivity.this);

                Intent intent = new Intent(CheckoutActivity.this ,OldAddressActivity.class);
                startActivity(intent);
            }
        };
    }

    @Override
    public void onOrder() {
        onBackPressed();
    }

    @Override
    public void onDeliveryCost() {
        UtilDialog.showDialog(this,
                getString(R.string.support_cost_title),
                getString(R.string.support_cost_msg),
                R.string.next_action,
                null,
                -1,
                null
        );
    }

    @Override
    public void onPayment() {
        Intent intent = new Intent(this , PayActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDiscount() {
        // TODO
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 2);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }

    private void enableSlideButton() {
        btnSlide.setEnabled(true);
        slideText.setVisibility(View.VISIBLE);
        btnSlide.setProgress(0);

        onProcessView.setVisibility(View.GONE);
    }

    public DialogInterface.OnClickListener endOrderCreation(final String msg) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(CheckoutActivity.this, msg, Toast.LENGTH_LONG).show();

                finish();

                Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                intent.putExtra("tab", 3);

                startActivity(intent);
            }
        };
    }
}
