package com.tiendosqui.dapp.map;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.AddressActivity;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.utils.AddressCreator;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 1/02/2018.
 *
 * Activity to select the location of the user.
 */

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    static final int REQUEST_ACCESS_FINE_LOCATION = 0x0001;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location lastLocation;
    private LatLng selectLocation;

    private EditText addressText;
    private Button selectAddress;
    private boolean cameraMoving = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);

        // Toolbar
        setToolbar(false);

        // Map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Location
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();

        // Request current location
        createLocationRequest();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (lastLocation == null) {
                    for (Location location : locationResult.getLocations()) {
                        lastLocation = location;
                    }
                    moveCamera();
                }
            }
        };

        // Center Marker
        addressText = findViewById(R.id.address_text);
        addressText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectAddress.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        selectAddress = findViewById(R.id.save_address);
        selectAddress.setEnabled(false);
        selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MapActivity.this, AddressActivity.class);

                intent.putExtra("address", new Address(
                        addressText.getText().toString(),
                        selectLocation.latitude,
                        selectLocation.longitude
                ).toString());

                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        moveCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
            return;
        }

        if (mFusedLocationClient == null)
            return;
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null);
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
            return;
        }

        if (mFusedLocationClient == null)
            return;
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            lastLocation = location;
                            moveCamera();
                        }
                    }
                });
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient == null)
            return;
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        if(mMap == null)
            return;

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                selectLocation = mMap.getCameraPosition().target;
                cameraMoving = true;
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (!cameraMoving)
                    return;

                cameraMoving = false;
                Address address = AddressCreator.fromLocationToAddress(MapActivity.this, selectLocation.latitude, selectLocation.longitude);

                if (address == null)
                    return;
                addressText.setText(address.getStreet());

                if (address.getStreet().contains("-")) {
                    if (address.getStreet().split("-").length > 2)
                        selectAddress.setEnabled(false);
                    else
                        selectAddress.setEnabled(true);
                } else
                    selectAddress.setEnabled(false);

                addressText.setSelection(address.getStreet().length());
            }
        });
        cameraMoving = false;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestLocationPermission();
            return;
        }
        mMap.setMyLocationEnabled(true);
        moveCamera();
    }

    private void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                showRequestPermission(explanationCallback());

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }

    public void showRequestPermission(DialogInterface.OnClickListener clickListener) {
        UtilDialog.showDialog(this,
                this.getString(R.string.dialog_why_location_title),
                this.getString(R.string.dialog_why_location_message),
                R.string.dialog_why_location_ok,
                clickListener,
                -1,
                null
        );
    }

    private DialogInterface.OnClickListener explanationCallback() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(MapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
        };
    }

    private void moveCamera() {
        if(lastLocation == null || mMap == null)
            return;

        selectLocation = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lastLocation.getLatitude(),
                        lastLocation.getLongitude()))      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
}
