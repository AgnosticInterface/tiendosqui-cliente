package com.tiendosqui.dapp.login;

import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Created by divait on 30/03/2017.
 */

public class LoginPresenter implements LoginContract.Presenter, LoginController.Callback {

    private final LoginContract.View lp_loginView;
    private LoginController lp_loginController;
    private Activity activity;

    public LoginPresenter(@NonNull LoginContract.View loginView,
                          @NonNull LoginController loginController, Activity activity) {

        lp_loginView = loginView;
        loginView.setPresenter(this);
        lp_loginController = loginController;

        this.activity = activity;
    }

    @Override
    public void attemptLogin(String email, String password) {
        lp_loginView.showProgress(true);
        lp_loginController.login(email, password, this);
    }

    @Override
    public void start() {
    }

    @Override
    public void onEmailError(String msg) {
        lp_loginView.showProgress(false);
        lp_loginView.setEmailError(msg);
    }

    @Override
    public void onPasswordError(String msg) {
        lp_loginView.showProgress(false);
        lp_loginView.setPasswordError(msg);
    }

    @Override
    public void onNetworkConnectFailed() {
        lp_loginView.showProgress(false);
        lp_loginView.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        lp_loginView.showProgress(false);
        lp_loginView.showServerError(msg);
    }

    @Override
    public void onAuthFailed(String msg) {
        lp_loginView.showProgress(false);
        lp_loginView.showLoginError(msg);
    }

    @Override
    public void onAuthSuccess() {
        lp_loginView.showProgress(false);
        lp_loginView.showMainActivity();
        // AlarmSystem.startAlert(activity);
    }

    @Override
    public void onNoAddress(String msg) {
        // TODO
    }
}
