package com.tiendosqui.dapp.address;

import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by Divait on 22/03/2017.
 *
 * The contract for methods in launch.
 */

public interface AddressContract {

    interface View extends BaseView<Presenter> {
        void showAddAddress(Address address);

        void showNoAddress();

        void showToastError(String message);

        void showMainActivity();

        void showServerError (String msg);

        void showNetworkError();

        void showAddresses(List<AddressItem> address);
    }

    interface Presenter extends BasePresenter {
        void saveAddress(Address address, boolean hasChange);

        void getAddress();
    }
}
