package com.tiendosqui.dapp.address;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.map.MapActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by divait on 23/03/2017.
 *
 * The view to select the address.
 */

public class AddressActivity extends BaseActivity implements AddressContract.View, AddressViewHolder.OnAddressClickedListener, MainAddressViewHolder.OnChangeListener {

    private RecyclerView listAddress;
    private View panelEmpty;

    private AddressAdapter adapter;
    private AddressContract.Presenter aa_presenter;

    private Address address;
    private boolean hasChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        // Map the views
        listAddress = findViewById(R.id.list_addresses);
        panelEmpty = findViewById(R.id.empty_address);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listAddress.setLayoutManager(layoutManager);

        // Get Data
        Intent intent = getIntent();
        String jsonAddress = intent.getStringExtra("address");

        if(jsonAddress != null) {
            address = Address.JsonToAddress(jsonAddress);
            showAddAddress(address);
        } else {
            address = new Address(
                    ""
            );
            showNoAddress();
        }

        List<Object> data = getMainItems();
        data.addAll(getAddressList());
        adapter = new AddressAdapter(data, this, this, this);
        listAddress.setAdapter(adapter);

        // Create the http handler
        HttpCalls httpCalls = new HttpCalls(this);

        // Connect the view with the interactor
        AddressController aa_controller = new AddressController(this, httpCalls);
        aa_presenter = new AddressPresenter(this, aa_controller);

        //if(DataStorage.getCity(this) == null)

        Log.d("Data dev", "City; " + DataStorage.getCity(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        aa_presenter.start();
        hasChange = false;
    }

    @Override
    public void onBackPressed() {
        if(DataStorage.getAddress(this) != null)
            startActivity(new Intent(this, MainActivity.class));
        else
            startActivity(new Intent(this, MapActivity.class));
    }

    @Override
    public void showAddAddress(Address address) {
        panelEmpty.setVisibility(View.GONE);
    }

    @Override
    public void showNoAddress() {
        panelEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showToastError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showAddresses(List<AddressItem> address) {
        // Do nothing
    }

    @Override
    public void setPresenter(AddressContract.Presenter presenter) {
        if (presenter != null) {
            aa_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void onAddressClicked(AddressItem addressItem, int imageId) {
        if(address == null)
            return;

        address.setName(addressItem.name);
        address.setImageId(imageId);
        aa_presenter.saveAddress(address, hasChange);
    }

    public List<Object> getAddressList() {
       String[] places =getResources().getStringArray(R.array.places);
        List<Object> addresses = new ArrayList<>();

        int i = 0;
        for(String place : places) {
            addresses.add(new AddressItem(
                1,
                place,
                "",
                getImage(i),
                null
            ));
            i++;
        }

        return  addresses;
    }

    public List<Object> getMainItems() {
        List<Object> addresses = new ArrayList<>();
        addresses.add(address);

        return  addresses;
    }

    private int getImage(int pos){
        switch (pos){
            default:
            case 0:
                return R.drawable.home;
            case 1:
                return R.drawable.work;
            case 2:
                return R.drawable.friend;
            case 3:
                return R.drawable.other;

        }
    }

    @Override
    public void onPutAddressClicked() {
        showAddAddress(null);
    }

    @Override
    public void onStreetChange(String nom) {
        if(!address.getStreet().equals(nom))
            hasChange = true;

        address.setStreet(nom);
        adapter.changeDetail(address.toAddressString() + " | " + address.getComment());
    }

    @Override
    public void onStreetClick() {
        UtilDialog.showDialog(this, getString(R.string.change_address), getString(R.string.change_address_msg), R.string.ok, changeAdressOnPositiveClick(), R.string.cancel, null);
    }

    @Override
    public void onCommentChange(String nom) {
        address.setComment(nom);
        adapter.changeDetail(address.toAddressString()  + " | " + address.getComment());
    }

    public DialogInterface.OnClickListener cityOnPositiveClick() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Spinner spinner = ((AlertDialog) dialog).findViewById(R.id.spinner_cities);
                String city = "";

                try {
                    if (spinner == null) return;
                    city = DataStorage.getCities(AddressActivity.this)[which];
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(city != null && city.length() > 0)
                        DataStorage.setCity(AddressActivity.this, city);
                }
            }
        };
    }

    public DialogInterface.OnClickListener changeAdressOnPositiveClick() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(AddressActivity.this, MapActivity.class));
                finish();
            }
        };
    }
}
