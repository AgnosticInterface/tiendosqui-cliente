package com.tiendosqui.dapp.address;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 24/03/2017.
 */

public class AddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int MAIN = 0x000;
    private static final int ADDRESS = 0x001;
    private static final int OTHER = 0x002;

    private List<Object> addresses;
    private Context context;
    private AddressViewHolder.OnAddressClickedListener listener;
    private MainAddressViewHolder.OnChangeListener otherListener;

    public AddressAdapter(List<AddressItem> addresses, Context context, AddressViewHolder.OnAddressClickedListener listener) {
        this.addresses = new ArrayList<>();
        this.addresses.addAll(addresses);

        this.context = context;

        this.listener = listener;
    }

    public AddressAdapter(List<Object> addresses, Context context, AddressViewHolder.OnAddressClickedListener listener, MainAddressViewHolder.OnChangeListener mainListener) {
        this.addresses = addresses;

        this.context = context;

        this.listener = listener;
        this.otherListener = mainListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        switch (viewType) {
            default:
            case ADDRESS:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity_address, parent, false);

                return new AddressViewHolder(v, listener);
            case MAIN:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_main_address, parent, false);

                return new MainAddressViewHolder(v, context, otherListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ADDRESS:
                AddressItem address = (AddressItem) addresses.get(position);

                String name = address.name;
                String details = address.address;

                ((AddressViewHolder)holder).getTitleView().setText(name);
                ((AddressViewHolder)holder).getDescriptionView().setText(details);
                ((AddressViewHolder)holder).getAddressImage().setImageDrawable(context.getResources().getDrawable(address.image));
                ((AddressViewHolder)holder).getAddressImage().setTag(address.image);
                ((AddressViewHolder)holder).setAddress(address);
                break;
            case MAIN:
                Address addressMain = (Address) addresses.get(position);
                ((MainAddressViewHolder)holder).configureAddress(addressMain);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public void changeDetail(String address) {
        int i = 0;
        for(Object addressItem : addresses) {
            if(addressItem instanceof AddressItem) {
                ((AddressItem) addressItem).address = address;

                try {
                    notifyItemChanged(i);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            i++;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (addresses.get(position) instanceof AddressItem) {
            return ADDRESS;
        } else if(addresses.get(position) instanceof Address) {
            return MAIN;
        }
        return OTHER;
    }

    public void rechargeDataSet(List<AddressItem> addresses) {
        this.addresses.clear();
        this.addresses.addAll(addresses);

        notifyDataSetChanged();
    }

    public void rechargeAddressDataSet(List<Object> addresses) {
        this.addresses.addAll(addresses);

        notifyDataSetChanged();
    }
}
