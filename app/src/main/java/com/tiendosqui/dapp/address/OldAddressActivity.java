package com.tiendosqui.dapp.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.map.MapActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 25/04/2017.
 */

public class OldAddressActivity extends BaseActivity implements AddressContract.View, AddressViewHolder.OnAddressClickedListener {

    private TextView txtAddAddress;
    private EditText editStreet;
    private EditText editNumber;
    private EditText editSubNumber;
    private EditText editDetails;
    private Spinner spStreet;
    private RecyclerView listAddress;

    private AddressAdapter adapter;
    private AddressContract.Presenter aa_presenter;

    private String[] streets;
    private Address address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_address);

        streets = getResources().getStringArray(R.array.streets);
        address = DataStorage.getAddress(this);

        // Map the views
        txtAddAddress = findViewById(R.id.btn_add_address);
        editStreet = findViewById(R.id.edit_street);
        editNumber = findViewById(R.id.edit_number);
        editSubNumber = findViewById(R.id.edit_sub_number);
        editDetails = findViewById(R.id.edit_details);
        spStreet = findViewById(R.id.sp_street);
        listAddress = findViewById(R.id.list_addresses);

        // Set adapters
        ArrayAdapter<CharSequence> spAdapter = ArrayAdapter.createFromResource(this,
                R.array.streets, android.R.layout.simple_spinner_item);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStreet.setAdapter(spAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listAddress.setLayoutManager(layoutManager);

        adapter = new AddressAdapter(getAddressList(), this, this);
        listAddress.setAdapter(adapter);

        // Listeners // TODO check location (lat, lot)
        txtAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddAddress(null);
            }
        });

        spStreet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // TODO address.setNomenclature(streets[i]);
                adapter.changeDetail(address.toAddressString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editDetails.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                address.setComment(charSequence.toString());
                // TODO adapter.changeDetail(address.toAddressString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                address.setStreet(charSequence.toString());
                adapter.changeDetail(address.toAddressString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // TODO address.setNumber(charSequence.toString());
                adapter.changeDetail(address.toAddressString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editSubNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // TODO address.setSub_number(charSequence.toString());
                adapter.changeDetail(address.toAddressString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        // Create the http handler
        HttpCalls httpCalls = new HttpCalls(this);

        // Connect the view with the interactor
        AddressController aa_controller = new AddressController(this, httpCalls);
        aa_presenter = new AddressPresenter(this, aa_controller);
    }

    @Override
    protected void onStart() {
        super.onStart();
        aa_presenter.start();

    }

    @Override
    public void setPresenter(AddressContract.Presenter presenter) {
        if (presenter != null) {
            aa_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showAddAddress(Address address) {
        Intent intent = new Intent(this, MapActivity.class);

        intent.putExtra("near", false);
        intent.putExtra("new", true);

        startActivity(intent);
    }

    @Override
    public void showNoAddress() {
        // TODO
    }

    @Override
    public void showToastError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showAddresses(List<AddressItem> address) {
        if(this.address != null) {

            address.add(0, new AddressItem(
                    this.address.getId(),
                    this.address.getName(),
                    this.address.toAddressString() + " | " + this.address.getComment(),
                    this.address.getImageId(),
                    this.address
            ));
        }

        adapter.rechargeDataSet(address);
    }

    @Override
    public void onAddressClicked(AddressItem addressItem, int imageId) {
        Log.d("Data dev", "Address : " + address.getName());
        if(address == null)
            return;

        address.setName(addressItem.name);

        // String a = addressItem.address;

        // String[] splitA = a.split(" ");
        // TODO address.setNomenclature(splitA[0].trim());
        // splitA = a.substring(splitA[0].length(), a.length()).split(" # ");
        address.setStreet(addressItem.address);
        // splitA = splitA[1].split("-");
        // TODO address.setNumber(splitA[0].trim());
        // TODO address.setSub_number(splitA[1].trim());

        address.setImageId(imageId);
        address.setId(addressItem.id);

        DataStorage.setAddress(this, address);
        showMainActivity();
    }

    public List<AddressItem> getAddressList() {
        List<AddressItem> addresses = new ArrayList<>();

        if(address == null)
            return addresses;

        addresses.add(new AddressItem(
                address.getId(),
                address.getName(),
                address.toAddressString() + " | " + address.getComment(),
                address.getImageId(),
                address
        ));

        return  addresses;
    }
}
