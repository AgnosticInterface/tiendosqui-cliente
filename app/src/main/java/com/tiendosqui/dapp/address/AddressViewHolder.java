package com.tiendosqui.dapp.address;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.AddressItem;

/**
 * Created by divai on 24/03/2017.
 */

public class AddressViewHolder extends RecyclerView.ViewHolder {
    private AddressItem address;

    private final TextView titleView;
    private final TextView descriptionView;

    private final ImageView addressImage;

    private final View view;

    public interface OnAddressClickedListener {
        void onAddressClicked(AddressItem addressItem, int image);
    }

    public AddressViewHolder(View v, final OnAddressClickedListener callback) {
        super(v);
        this.view = v;

        titleView = (TextView) v.findViewById(R.id.txt_address_name);
        descriptionView = (TextView) v.findViewById(R.id.txt_Address_desc);

        addressImage = (ImageView) v.findViewById(R.id.address_image);

        // Define click listener for the TitleViewHolder's View.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onAddressClicked(address, (Integer) addressImage.getTag());
            }
        });
    }

    public TextView getTitleView() {
        return titleView;
    }

    public TextView getDescriptionView() {
        return descriptionView;
    }

    public ImageView getAddressImage() {
        return addressImage;
    }

    public View getView() {
        return view;
    }

    public void setAddress(AddressItem address) {
        this.address = address;
    }
}
