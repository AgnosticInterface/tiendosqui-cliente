package com.tiendosqui.dapp.address;

import android.content.Context;
import android.location.Geocoder;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.entities.json.AddressJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by divait on 24/03/2017.
 */

public class AddressController {
    private static String country = ", Colombia";

    private Context ac_context;
    private HttpCalls ac_httpCalls;

    interface Callback{
        void onCommentError(String message);

        void onStreetError(String message);

        void onNumberError(String message);

        void onSubNumberError(String message);

        void onAddressSave();

        void onAddressSaveError(Context context);

        void onServerError(String msg);

        void onNoUser(String msg);

        void onNetworkConnectFailed();

        void onAddresses(List<AddressItem> addressItems);
    }

    public AddressController(Context context, HttpCalls httpCalls){
        this.ac_context = context;
        ac_httpCalls = httpCalls;
    }

    public void saveAddress(Address address, boolean hasChange, Callback callback) {

        if(address.getStreet() == null || address.getStreet().isEmpty()) {
            callback.onStreetError(ac_context.getString(R.string.street_empty_error));
            return;
        }
/*
        if(address.getComment() == null || address.getComment().isEmpty()) {
            callback.onCommentError(ac_context.getString(R.string.comment_empty_error));
            return;
        }
*/

        // Check lat and lon
        if(hasChange) {
            HashMap<String, Double> location = getLocationFromAddress(address.toAddressString());

            if(location.size() > 0 && location.containsKey("lat") && location.containsKey("lon")) {
                address.setLatitude(location.get("lat"));
                address.setLongitude(location.get("lon"));
            } else {
                callback.onAddressSaveError(ac_context);
                return;
            }
        } else {
            if(address.getLongitude() == 0 || address.getLatitude() == 0) {
                HashMap<String, Double> location = getLocationFromAddress(address.toAddressString());


                if(location.size() > 0 && location.containsKey("lat") && location.containsKey("lon")) {
                    address.setLatitude(location.get("lat"));
                    address.setLongitude(location.get("lon"));
                } else {
                    callback.onAddressSaveError(ac_context);
                    return;
                }
            }
        }

        User user = DataStorage.getUser(ac_context);
        if(user == null) {
            DataStorage.setAddress(ac_context, address);
            callback.onAddressSave();
        } else {
            attemptAddAddress(address, user, callback);
        }
    }

    public void getAddresses(Callback callback) {
        Long id = DataStorage.getUserId(ac_context);

        if(id < 0) {
            callback.onNoUser("No user");
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(ac_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadAddresses(id, callback);
    }

    private void attemptLoadAddresses(long id, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(ac_context));

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_ID, Long.toString(id) );
        params.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_CANT, "10" );

        JSONObject body = new JSONObject();

        try {
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_ID, id);
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_USER_CANT, "10");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());

        ac_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.GET_ADDRESSES_URL), NetworkInfo.ADDRESS_TAG, headers, body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data address: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Address Json: " +json);
        } catch (JSONException e) {
            try {
                JSONObject job = new JSONObject(response);
                Log.d("Data dev", "Address Json: " + job);

                if(job.getString("petition").trim().equals("OK")) {
                    callback.onAddresses(new ArrayList<AddressItem>());
                    return;
                }

            } catch (JSONException ex) {
                callback.onServerError(ac_context.getString(R.string.server_error));
                ex.printStackTrace();
                return;
            }

            callback.onServerError(ac_context.getString(R.string.server_error));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            AddressJson[] addressesJson = gson.fromJson(json.toString(), AddressJson[].class);

            List<AddressItem> addresses = new ArrayList<>();

            for (AddressJson addressJson : addressesJson) {
                addresses.add(new AddressItem(
                        addressJson.getId(),
                        addressJson.getAddress_alias(),
                        addressJson.getAddress(),
                        getImage(addressJson.getAddress_alias()),
                        AddressJson.toAddress(addressJson)
                ));
            }

            callback.onAddresses(addresses);
        } catch (Exception ex) {
            callback.onServerError(ac_context.getString(R.string.error_address_loading));
            ex.printStackTrace();
        }
    }

    private void attemptAddAddress(final Address address, User user, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(ac_context));

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.ADDRESS_PARAMETER_USER_ID, user.getId());
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_NAME, address.getName());
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_FORMATED, address.toAddressString());
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_DETAILS, address.getComment());
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_LAT, Double.toString(address.getLatitude()));
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_LON, Double.toString(address.getLongitude()));
            body.put(NetworkInfo.ADDRESS_PARAMETER_ADDRESS_UNIQUE, UUID.randomUUID().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());

        ac_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.ADD_ADDRESS_URL), NetworkInfo.ADDRESS_TAG, headers, body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data4: " + response);
                long addressId = 1;

                try {
                    JSONObject addressJson = new JSONObject(response);
                    addressId = addressJson.getLong("idaddress");
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onServerError(ac_context.getString(R.string.error_add_address));
                }

                address.setId(addressId);
                DataStorage.setAddress(ac_context, address);
                callback.onAddressSave();
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(ac_context.getString(R.string.server_error));
            }
        });
    }

    private int getImage(String name){
        switch (name){
            case "Casa":
                return R.drawable.home;
            case "Oficina":
                return R.drawable.work;
            case "Novi@":
                return R.drawable.friend;
            default:
            case "Otro":
                return R.drawable.other;

        }
    }

    public HashMap<String, Double> getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(ac_context);
        List<android.location.Address> address;
        HashMap<String, Double> p1 = new HashMap<>();

        try {
            address = coder.getFromLocationName(strAddress +", " + DataStorage.getCity(ac_context) +country, 5);
            if (address==null) {
                return null;
            }

            android.location.Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1.put("lat", location.getLatitude());
            p1.put("lon", location.getLongitude());
        } catch (IOException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return p1;
    }
}
