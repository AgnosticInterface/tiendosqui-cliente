package com.tiendosqui.dapp.address;

import android.content.Context;
import android.support.annotation.NonNull;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.AddressItem;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The presenter of the address view.
 */

public class AddressPresenter implements AddressContract.Presenter, AddressController.Callback {
    private AddressContract.View ap_view;
    private AddressController ap_controller;

    public AddressPresenter(@NonNull AddressContract.View addressView,
                           @NonNull AddressController addressController) {

        ap_view = addressView;
        ap_controller = addressController;

        ap_view.setPresenter(this);
    }

    @Override
    public void start() {
        getAddress();
    }

    @Override
    public void saveAddress(Address address, boolean hasChange) {
        ap_controller.saveAddress(address, hasChange, this);
    }

    @Override
    public void getAddress() {
        ap_controller.getAddresses(this);
    }

    @Override
    public void onCommentError(String message) {
        ap_view.showToastError(message);
    }

    @Override
    public void onStreetError(String message) {
        ap_view.showToastError(message);
    }

    @Override
    public void onNumberError(String message) {
        ap_view.showToastError(message);
    }

    @Override
    public void onSubNumberError(String message) {
        ap_view.showToastError(message);
    }

    @Override
    public void onAddressSave() {
        ap_view.showMainActivity();
        ap_view.showToastError("Dirección guardada correctamente.");
    }

    @Override
    public void onAddressSaveError(Context context) {
        ap_view.showToastError(context.getString(R.string.address_error));
    }

    @Override
    public void onServerError(String msg) {
        ap_view.showServerError(msg);
    }

    @Override
    public void onNoUser(String msg) {
        // TODO
    }

    @Override
    public void onNetworkConnectFailed() {
        ap_view.showNetworkError();
    }

    @Override
    public void onAddresses(List<AddressItem> addressItems) {
        ap_view.showAddresses(addressItems);
    }
}
