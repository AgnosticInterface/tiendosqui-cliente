package com.tiendosqui.dapp.address;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;

/**
 * Created by divai on 3/05/2017.
 */

public class MainAddressViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    private OnChangeListener listener;

    private TextView txtAddAddress;
    private TextView txtTitleDetails;
    private TextView txtTitleSave;
    private EditText editStreet;
    private EditText editDetails;

    private View panelAddAddress;

    public interface OnChangeListener {
        void onPutAddressClicked();

        void onStreetChange(String nom);

        void onStreetClick();

        void onCommentChange(String nom);
    }

    public MainAddressViewHolder(View view, Context context, final OnChangeListener listener) {
        super(view);

        this.listener = listener;
        this.context = context;

        txtAddAddress = view.findViewById(R.id.btn_add_address);
        txtTitleDetails = view.findViewById(R.id.txt_title_details);
        txtTitleSave = view.findViewById(R.id.txt_title_save);
        editStreet = view.findViewById(R.id.edit_street);
        editDetails =  view.findViewById(R.id.edit_details);

        panelAddAddress = view.findViewById(R.id.panel_address);

        // Listeners
        txtAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddAddress(null);
                listener.onPutAddressClicked();
            }
        });

        editDetails.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.onCommentChange(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        editStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.onStreetChange(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        editStreet.setFocusable(false);
        editStreet.setClickable(true);
        editStreet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onStreetClick();
            }
        });
    }

    public void showAddAddress(Address address) {
        txtAddAddress.setVisibility(View.GONE);
        panelAddAddress.setVisibility(View.VISIBLE);
        txtTitleDetails.setVisibility(View.VISIBLE);
        txtTitleSave.setVisibility(View.VISIBLE);
        editDetails.setVisibility(View.VISIBLE);

        if(address != null) {
            editStreet.setText(address.getStreet());
            editStreet.requestFocus();
        }
    }

    public void configureAddress(Address address) {
        if(address != null && address.getLatitude() != 0 && address.getLongitude() != 0) {
            txtAddAddress.setVisibility(View.GONE);
            panelAddAddress.setVisibility(View.VISIBLE);
            txtTitleDetails.setVisibility(View.VISIBLE);
            txtTitleSave.setVisibility(View.VISIBLE);
            editDetails.setVisibility(View.VISIBLE);

            editStreet.setText(address.getStreet());
            editStreet.requestFocus();
        }
    }
}
