package com.tiendosqui.dapp.launch;

import android.content.DialogInterface;
import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.network.HttpCalls;

/**
 * Created by Divait on 22/03/2017.
 *
 * The presenter of the launch view.
 */

public class LaunchPresenter implements LaunchContract.Presenter, LaunchController.Callback {

    private LaunchContract.View lp_view;
    private LaunchController lp_controller;

    public LaunchPresenter(@NonNull LaunchContract.View launchView,
                             @NonNull LaunchController launchController) {

        lp_view = launchView;
        lp_controller = launchController;

        lp_view.setPresenter(this);
    }

    // Presenter Contract
    @Override
    public void start() {
        lp_controller.setCallback(this);
    }

    @Override
    public void createApiClient() {
        lp_controller.setApiClient();
    }

    @Override
    public void connectApiClient() {
        lp_controller.connectApiClient();
    }

    @Override
    public void disconnectApiClient() {
        lp_controller.disconnectApiClient();
    }

    @Override
    public void startLocationUpdates() {
        lp_controller.startLocationUpdates();
    }

    @Override
    public void stopLocationUpdates() {
        lp_controller.stopLocationUpdates();
    }

    @Override
    public void getSupportNumber(HttpCalls httpCalls) {
        lp_controller.getSupportNumbers(httpCalls);
    }

    @Override
    public void getCities(HttpCalls httpCalls) {
        lp_controller.getCities(httpCalls);
    }

    // Controller Callbacks
    @Override
    public void onRequestPermission(DialogInterface.OnClickListener clickListener) {
        lp_view.showRequestPermission(clickListener);
    }

    @Override
    public void onEmptyAddress(String message) {
        lp_view.showAddressActivity();
    }

    @Override
    public void onAddress(Address address) {
        lp_view.showAddressActivity(address);
    }

    @Override
    public void onNearAddress() {
        lp_view.showHomeActivity();
    }

    @Override
    public void onGetCities() {
        lp_view.setFlagCities();
    }

    @Override
    public void onGetPhones() {
        lp_view.setFlagPhones();
    }

    @Override
    public boolean getShowNear() {
        return lp_view.showNearAddress();
    }

    @Override
    public void onServerError(String msg) {
        lp_view.showToastError(msg);
    }
}
