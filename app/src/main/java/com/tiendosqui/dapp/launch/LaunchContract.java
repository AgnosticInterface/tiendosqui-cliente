package com.tiendosqui.dapp.launch;

import android.content.DialogInterface;

import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by Divait on 22/03/2017.
 *
 * The contract for methods in launch.
 */

public interface LaunchContract {

    interface View extends BaseView<Presenter> {
        void showToastError(String message);

        void showBoxMessage(String title, String message);

        void showRequestPermission(DialogInterface.OnClickListener clickListener);

        void showAddressActivity();

        void showHomeActivity();

        void showAddressActivity(Address address);

        boolean showNearAddress();

        void setFlagPhones();

        void setFlagCities();
    }

    interface Presenter extends BasePresenter {
        void createApiClient();

        void connectApiClient();

        void disconnectApiClient();

        void startLocationUpdates();

        void stopLocationUpdates();

        void getSupportNumber(HttpCalls httpCalls);

        void getCities(HttpCalls httpCalls);
    }
}
