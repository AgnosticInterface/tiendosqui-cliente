package com.tiendosqui.dapp.launch;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.AddressActivity;
import com.tiendosqui.dapp.address.OldAddressActivity;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.onboarding.OnBoardingActivity;
import com.tiendosqui.dapp.utils.ActivityUtils;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;


/**
 * Created by Divait on 21/03/2017.
 *
 * The initial Activity.
 */

public class LaunchActivity extends AppCompatActivity implements LaunchContract.View {
    private LaunchContract.Presenter la_presenter;

    private TextView boxTitle;
    private TextView boxMessage;

    private boolean showNear;

    private boolean isPhone;
    private boolean isCities;
    private boolean isAddress;

    private boolean isNewAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        isNewAddress = getIntent().getBooleanExtra("new", false);

        boxTitle = findViewById(R.id.title_box);
        boxMessage = findViewById(R.id.message_box);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            showBoxMessage(getString(R.string.dialog_thanks_title), getString(R.string.dialog_thanks_message));

        // Connect the view with the interactor
        LaunchController la_controller = new LaunchController(this);
        la_presenter = new LaunchPresenter(this, la_controller);

        la_presenter.createApiClient();

        showNear = getIntent().getBooleanExtra("near", true);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            // Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
        }else{
            showGPSDisabledAlertToUser();
        }

        Log.d("Data dev", "Version: " + ActivityUtils.lastVersion());
    }

    protected void onStart() {
        la_presenter.connectApiClient();
        super.onStart();

        la_presenter.start();
        isPhone = false;
        isCities = true;

        HttpCalls httpCalls = new HttpCalls(this);
        la_presenter.getSupportNumber(httpCalls);
        la_presenter.getCities(httpCalls);
    }

    @Override
    protected void onResume() {
        super.onResume();
        la_presenter.startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        la_presenter.stopLocationUpdates();
    }

    protected void onStop() {
        la_presenter.disconnectApiClient();
        super.onStop();
    }

    @Override
    public void setPresenter(LaunchContract.Presenter presenter) {
        if (presenter != null) {
            la_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showToastError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showBoxMessage(String title, String message) {
    boxTitle.setText(title);
        boxMessage.setText(message);
    }

    @Override
    public void showRequestPermission(DialogInterface.OnClickListener clickListener) {
        UtilDialog.showDialog(this,
                this.getString(R.string.dialog_why_location_title),
                this.getString(R.string.dialog_why_location_message),
                R.string.dialog_why_location_ok,
                clickListener,
                -1,
                null
        );
    }

    @Override
    public void showAddressActivity() {
        isAddress = true;
        showAddressActivity(null);
    }

    @Override
    public void showHomeActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void showAddressActivity(Address address) {
        Intent intent;

        if(DataStorage.getUser(this) == null || isNewAddress)
            intent = new Intent(this, AddressActivity.class);
        else
            intent = new Intent(this, OldAddressActivity.class);

        if(address != null)
            intent.putExtra("address", address.toString());

        startActivity(intent);
        finish();
    }

    @Override
    public boolean showNearAddress() {
        return showNear;
    }

    @Override
    public void setFlagPhones() {
        isPhone = true;
    }

    @Override
    public void setFlagCities() {
        isCities = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LaunchController.REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    showBoxMessage(getString(R.string.dialog_thanks_title), getString(R.string.dialog_thanks_message));
                    la_presenter.startLocationUpdates();
                } else {
                    showToastError(getString(R.string.error_permission_no_allow));
                    showAddressActivity();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void showGPSDisabledAlertToUser(){
        UtilDialog.showDialog(
                this,
                getString(R.string.dialog_title_gps),
                getString(R.string.dialog_message_gps),
                R.string.enable_gps,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                },
                R.string.dialog_cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showAddressActivity();
                    }
                }
        );
    }
}
