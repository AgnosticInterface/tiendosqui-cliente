package com.tiendosqui.dapp.launch;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.json.JsonCity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.AddressCreator;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Divait on 22/03/2017.
 *
 * Controller of the launch view.
 */

class LaunchController implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    static final int REQUEST_ACCESS_FINE_LOCATION = 0x0001;

    private Activity li_activity;
    private Callback li_callback;
    private GoogleApiClient li_googleApiClient;
    private LocationRequest li_locationRequest;
    private Location li_location;

    interface Callback {

        void onRequestPermission(DialogInterface.OnClickListener clickListener);

        void onEmptyAddress(String message);

        void onAddress(Address address);

        void onNearAddress();

        void onGetCities();

        void onGetPhones();

        boolean getShowNear();

        void onServerError(String error);
    }

    LaunchController(Activity activity) {
        li_activity = activity;
    }

    void setCallback(Callback callback) {
        li_callback = callback;
    }

    GoogleApiClient setApiClient() {
        li_googleApiClient = new GoogleApiClient.Builder(li_activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        return li_googleApiClient;
    }

    void connectApiClient() {
        // TODO: check internet connection
        li_googleApiClient.connect();
    }

    void disconnectApiClient() {
        li_googleApiClient.disconnect();
    }

    private void RequestLocationPermission(Activity activity) {
        if (ContextCompat.checkSelfPermission(li_activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                li_callback.onRequestPermission(explanationCallback(activity));

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }

    private DialogInterface.OnClickListener explanationCallback(final Activity activity) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
        };
    }

    private void createLocationRequest() {
        li_locationRequest = new LocationRequest();
        li_locationRequest.setInterval(10000);
        li_locationRequest.setFastestInterval(5000);
        li_locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    void startLocationUpdates() {
        if(li_googleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(li_activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(li_activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                RequestLocationPermission(li_activity);
                return;
            }
            createLocationRequest();
            LocationServices.FusedLocationApi.requestLocationUpdates(li_googleApiClient, li_locationRequest, this);
        }
    }

    void stopLocationUpdates() {
        if(li_googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    li_googleApiClient, this);
        }
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(li_activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(li_activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            RequestLocationPermission(li_activity);
            return;
        }
        li_location = LocationServices.FusedLocationApi.getLastLocation(
                li_googleApiClient);
        /*
        if (li_location != null) {
            li_callback.onLocationGet(li_location.getLatitude(), li_location.getLongitude());
        }
        */
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {
        li_callback.onEmptyAddress(li_activity.getString(R.string.error_getting_address));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        li_callback.onEmptyAddress(li_activity.getString(R.string.error_getting_address));
    }

    @Override
    public void onLocationChanged(Location location) {
        li_location = location;
        Address address = AddressCreator.fromLocationToAddress(li_activity, li_location.getLatitude(), location.getLongitude()); // String to Address

        if(address == null)
            li_callback.onEmptyAddress(li_activity.getString(R.string.error_getting_address));
        else {
            Address oldAddress = DataStorage.getAddress(li_activity);
            if(oldAddress == null || oldAddress.getLatitude() == 0 || oldAddress.getLongitude() == 0 )
                li_callback.onAddress(address);
            else {
                if(CalculationByDistance(address.getLatitude(), address.getLongitude(), oldAddress.getLatitude(), oldAddress.getLongitude()) < 2 && li_callback.getShowNear())
                    li_callback.onNearAddress();
                else
                    li_callback.onAddress(address);
            }

        }
    }

    public double CalculationByDistance(double lat1, double lon1, double lat2, double lon2) {
        int Radius = 6371;// radius of earth in Km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Data dev", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return valueResult;
    }

    public void getSupportNumbers(HttpCalls httpCalls) {

        httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.SUPPORT_NUM_URL), NetworkInfo.SUPPORT_TAG, null, new HashMap<String, String>(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data phones: " +response);
                try {
                    JSONObject obj = (new JSONArray(response)).getJSONObject(0);

                    Log.d("Data dev", "Phone: " + obj.getString("phone1"));
                    DataStorage.setSupportNumber(li_activity, obj.getString("phone1"));
                    li_callback.onGetPhones();
                } catch (Exception e) {
                    e.printStackTrace();
                    li_callback.onServerError(li_activity.getString(R.string.error_phones));
                }
            }

            @Override
            public void errorResponse(String error) {
                li_callback.onServerError(li_activity.getString(R.string.server_error));
            }
        });
    }

    public void getCities(HttpCalls httpCalls) {
        Map<String, String> headers = new HashMap<>();

        httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.CITIES_URL), NetworkInfo.SUPPORT_TAG, null, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data4: " + response);
                try {
                    JsonCity[] cities = new Gson().fromJson(response, JsonCity[].class);

                    for (JsonCity city : cities) {
                        SugarRecord.save(city);
                    }

                    li_callback.onGetCities();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    li_callback.onServerError(li_activity.getString(R.string.error_cities));
                }
            }

            @Override
            public void errorResponse(String error) {
                li_callback.onServerError(li_activity.getString(R.string.server_error));
            }
        });
    }
}
