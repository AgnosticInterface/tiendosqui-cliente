package com.tiendosqui.dapp.profile;

import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Created by divait on 17/04/2017.
 */

public class ProfilePresenter implements ProfileContract.Presenter, ProfileController.Callback {

    private final ProfileContract.View profileView;
    private ProfileController profileController;
    private Activity activity;

    public ProfilePresenter(@NonNull ProfileContract.View profileView,
                             @NonNull ProfileController profileController, Activity activity) {

        this.profileView = profileView;
        profileView.setPresenter(this);
        this.profileController = profileController;

        this.activity = activity;
    }

    @Override
    public void start() {
        getUser();
    }

    @Override
    public void updateUser(String name, String email, String phone) {
        profileController.register(name, email, phone, this);
    }

    @Override
    public void getUser() {
        profileView.setUserData(profileController.getUSer());
    }

    @Override
    public void onNameError(String msg) {
        profileView.setNameError(msg);
    }

    @Override
    public void onEmailError(String msg) {
        profileView.setEmailError(msg);
    }

    @Override
    public void onPhoneError(String msg) {
        profileView.setPhoneError(msg);
    }

    @Override
    public void onNetworkConnectFailed() {
        profileView.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        profileView.showServerError(msg);
    }

    @Override
    public void onUpdateFailed(String msg) {
        profileView.showUpdateError(msg);
    }

    @Override
    public void onUpdateSuccess() {
        profileView.showUpdateCompleted();
    }
}
