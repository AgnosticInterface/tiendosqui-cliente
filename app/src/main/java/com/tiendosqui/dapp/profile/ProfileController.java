package com.tiendosqui.dapp.profile;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by divai on 17/04/2017.
 */

public class ProfileController {

    private final Context context;
    private HttpCalls httpCalls;

    public interface Callback {

        void onNameError(String msg);

        void onEmailError(String msg);

        void onPhoneError(String msg);

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onUpdateFailed(String msg);

        void onUpdateSuccess();
    }

    public ProfileController(Context context, HttpCalls httpCalls) {
        this.context = context;
        if (httpCalls != null) {
            this.httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public User getUSer() {
        return DataStorage.getUser(context);
    }

    public void register(String name, String email, String phone, final ProfileController.Callback callback) {
        // Check logic
        boolean validName = isValidData(name, callback, 1);
        boolean validEmail = isValidEmail(email, callback);
        boolean validPhone = isValidData(phone, callback, 2);
        if (!( validName && validEmail && validPhone)) {
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        // Make register call
        updateUser(name, email, phone, callback);

    }

    /**
     * Check if the data is useful.
     *
     * @param data the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidData(String data, ProfileController.Callback callback, int position) {
        boolean isValid = true;
        if (TextUtils.isEmpty(data)) {
            switch (position) {
                default:
                case 1:
                    callback.onNameError(context.getString(R.string.error_empty_name));
                    break;
                case 2:
                    callback.onPhoneError(context.getString(R.string.error_empty_phone));
                    break;
            }
            isValid = false;
        }
        return isValid;
    }

    /**
     * Check if the username is useful.
     *
     * @param email the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidEmail(String email, ProfileController.Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(context.getString(R.string.error_empty_email));
            isValid = false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(context.getString(R.string.error_reg_invalid_email));
            isValid = false;
        }
        return isValid;
    }

    private void updateUser(final String name, final String email, final String phone, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(context));

        User user = DataStorage.getUser(context);

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_ID, String.valueOf(user.getId()));
        params.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_EMAIL, email);
        params.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_PHONE, phone);
        params.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_USER_NAME, name);
        params.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_USER_IMG, user.getPicture()== null?"":user.getPicture());

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_ID, user.getId());
            body.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_EMAIL, email);
            body.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_PHONE, phone);
            body.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_USER_NAME, name);
            body.put(NetworkInfo.UPDATE_USER_POST_PARAMETER_USER_IMG, user.getPicture()== null?"":user.getPicture());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Data dev", "body: " + body.toString());

        httpCalls.standardPutCall(NetworkInfo.getURL(NetworkInfo.UPDATE_USER_URL), NetworkInfo.UPDATE_USER_TAG, params, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Update user: " +response);
                onSuccessResponse(response, name, email, phone, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }


    private void onSuccessResponse(String response, String name, String email, String phone, Callback callback) {
        JSONObject json;
        try {
            json = new JSONObject(response);
            Log.d("Data dev", "Support response Json: " +json);

            if(json.has("petition")){
                if(json.getString("petition").trim().equals("OK")) {
                    User user = getUSer();

                    user.setName(name);
                    user.setEmail(email);
                    user.setPhone(phone);

                    DataStorage.saveUserData(context, user);

                    callback.onUpdateSuccess();
                    return;
                }
            }
        } catch (JSONException e) {
            callback.onServerError(context.getString(R.string.error_updating_profile));
            e.printStackTrace();
            return;
        }

        callback.onUpdateFailed(context.getString(R.string.error_updating_profile));

    }
}

