package com.tiendosqui.dapp.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divai on 17/04/2017.
 */

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

    private EditText editName;
    private EditText editEmail;
    private EditText editPhone;

    private TextInputLayout errorName;
    private TextInputLayout errorEmail;
    private TextInputLayout errorPhone;

    private ImageView userImage;

    private Button btnUpdate;

    private View progressView;
    private View profileFormView;

    private HttpCalls ra_httpCalls;
    private ProfileContract.Presenter ra_presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setToolbar(true);

        // Create the http handler
        ra_httpCalls = new HttpCalls(this);

        ProfileController profileController = new ProfileController(getApplicationContext(), ra_httpCalls);
        ra_presenter = new ProfilePresenter(this, profileController, this);

        editName = (EditText) findViewById(R.id.profile_edit_name);
        editEmail= (EditText) findViewById(R.id.profile_edit_email);
        editPhone = (EditText) findViewById(R.id.profile_edit_phone);

        errorName = (TextInputLayout) findViewById(R.id.profile_error_name);
        errorEmail = (TextInputLayout) findViewById(R.id.profile_error_email);
        errorPhone = (TextInputLayout) findViewById(R.id.profile_error_phone);

        userImage = (ImageView) findViewById(R.id.profile_picture);

        btnUpdate = (Button) findViewById(R.id.update_button);

        profileFormView = findViewById(R.id.profile_form);
        progressView = findViewById(R.id.profile_progress);

        // Events
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptUpdate();
            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorName.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorEmail.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorPhone.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ra_presenter.start();
    }

    @Override
    public void setPresenter(ProfileContract.Presenter presenter) {
        if (presenter != null) {
            ra_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showProgress(boolean show) {
        profileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNameError(String error) {
        errorName.setError(error);
        editName.requestFocus();
    }

    @Override
    public void setEmailError(String error) {
        errorEmail.setError(error);
        editEmail.requestFocus();
    }

    @Override
    public void setPhoneError(String error) {
        errorPhone.setError(error);
        editPhone.requestFocus();
    }

    @Override
    public void showUpdateError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showUpdateCompleted() {
        Toast.makeText(this, getString(R.string.profile_updated), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setUserData(User user) {
        if(user == null)
            return;

        editName.setText(user.getName());
        editEmail.setText(user.getEmail());
        editPhone.setText(user.getPhone());

        ImageLoader.loadCircleImageFromURL(user.getPicture(), userImage, R.drawable.will, this);
    }

    private void attemptUpdate() {
        ra_presenter.updateUser(
                editName.getText().toString(),
                editEmail.getText().toString(),
                editPhone.getText().toString()
        );
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 4);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }
}
