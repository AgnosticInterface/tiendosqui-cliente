package com.tiendosqui.dapp.profile;

import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by divai on 17/04/2017.
 */

public interface ProfileContract {

    interface View extends BaseView<Presenter> {

        void showProgress(boolean show);

        void setNameError(String error);

        void setEmailError(String error);

        void setPhoneError(String error);

        void showUpdateError(String msg);

        void showUpdateCompleted();

        void showServerError(String msg);

        void showNetworkError();

        void setUserData(User user);
    }

    interface Presenter extends BasePresenter {
        void updateUser(String name, String email, String phone);

        void getUser();
    }
}
