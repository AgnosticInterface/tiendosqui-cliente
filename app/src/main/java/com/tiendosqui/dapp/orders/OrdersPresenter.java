package com.tiendosqui.dapp.orders;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.SmallOrder;

import java.util.List;

/**
 * Created by Medycitas on 05/04/2017.
 */

public class OrdersPresenter implements OrdersContract.Presenter, OrdersController.Callback {

    private OrdersContract.View op_view;
    private OrdersController op_controller;

    public OrdersPresenter (@NonNull OrdersContract.View ordersView,
                          @NonNull OrdersController ordersController) {

        op_view = ordersView;
        op_controller = ordersController;
    }

    @Override
    public void start() {
        op_view.setTab(0);
        getOrders();
    }

    @Override
    public void getOrders() {
        op_controller.getHistoryOrders(false, this);
    }

    @Override
    public void getHistoryOrders() {
        op_controller.getHistoryOrders(true, this);
    }

    // Callback
    @Override
    public void onNetworkConnectFailed() {
        op_view.showNetworkError(true);
    }

    @Override
    public void onServerError(String msg) {
        op_view.showServerError(msg);
    }

    @Override
    public void onLoadOrdersFail(String msg) {
        op_view.showLoadOrdersError(msg);
    }

    @Override
    public void onLoadOrders(List<SmallOrder> orders) {
        op_view.showOrders(orders);
    }

    @Override
    public void onLoadHistoryOrders(List<SmallOrder> orders, boolean isH) {
        op_view.showHistoryOrders(orders, isH);
    }

    @Override
    public void noUser() {
        op_view.showNoOrders();
    }
}
