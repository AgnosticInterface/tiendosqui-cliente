package com.tiendosqui.dapp.orders;

import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by divait on 05/04/2017.
 */

public interface OrdersContract {
    public interface View extends BaseView<Presenter> {

        void showOrders (List<SmallOrder> orders);

        void showHistoryOrders (List<SmallOrder> orders, boolean isH);

        void showLoadOrdersError (String msg);

        void showServerError (String msg);

        void showNetworkError(boolean show);

        void setTab(int position);

        void showNoOrders();
    }

    interface Presenter extends BasePresenter {
        void getOrders();

        void getHistoryOrders();
    }
}
