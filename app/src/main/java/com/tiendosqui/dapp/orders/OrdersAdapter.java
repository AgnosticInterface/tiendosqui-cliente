package com.tiendosqui.dapp.orders;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.SmallOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Divait on 23/08/2016.
 *
 * Adapter for the RecyclerView in OrdersListFragment, to show all the orders
 *
 */
public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OrdersListViewHolder.OnOrderClickedListener {
    private final int OTHER = 0x000;
    private final int ORDER = 0x001;

    private List<Object> oa_orders;
    private Context oa_context;
    private OrdersContract.View oa_parentView;

    private onItemClicked listener;

    public interface onItemClicked {
        public void onOrder(SmallOrder order);
    }

    public static OrdersAdapter newInstance(Context context, OrdersContract.View parent, onItemClicked listener) {
        return new OrdersAdapter(new ArrayList<>(), context, parent, listener);
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param orders String[] containing the data to populate views to be used by RecyclerView.
     */
    private OrdersAdapter(ArrayList<Object> orders, Context context, OrdersContract.View parent, onItemClicked listener) {
        oa_context = context;
        oa_orders = orders;
        oa_parentView = parent;

        this.listener = listener;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v;

        // Add the corresponding element to the list
        switch (viewType) {
            case ORDER:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.element_order_list, parent, false);
                return new OrdersListViewHolder(v, oa_context, this);
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.element_no_internet_connection, parent, false);
                return new OrdersListViewHolder.NoInternetViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ORDER:

                final SmallOrder order = (SmallOrder) oa_orders.get(position);
                ((OrdersListViewHolder) holder).setData(order, (Activity) oa_context);

                ((OrdersListViewHolder) holder).getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onOrder(order);
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return oa_orders.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (oa_orders.get(position) instanceof SmallOrder) {
            return ORDER;
        }
        return OTHER;
    }

    @Override
    public void onOrderClicked(SmallOrder order, View item) {
        // oa_parentView.showOrderDetails(order, item);
    }

    public void noInternetConnection (boolean isConnected) {
        if(isConnected) {
            if(oa_orders.size() > 0 && getItemViewType(0) == OTHER)
                oa_orders.remove(0);
        } else {
            if(oa_orders.size() > 0) {
                if (getItemViewType(0) != OTHER)
                    oa_orders.add(0, "No internet");
            } else
                oa_orders.add(0, "No internet");
        }
        notifyDataSetChanged();
    }

    public void rechargeOrders(List<SmallOrder> orders) {
        oa_orders.clear();
        oa_orders.addAll(orders);

        notifyDataSetChanged();
    }

    public void removeOrders() {
        oa_orders.clear();
        notifyDataSetChanged();
    }
}
