package com.tiendosqui.dapp.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.order_details.OrderDetailsActivity;
import com.tiendosqui.dapp.tabs.TabChildFragment;

import java.util.Collections;
import java.util.List;

/**
 * Created by divait on 05/04/2017.
 */

public class OrdersFragment extends TabChildFragment implements OrdersContract.View, OrdersAdapter.onItemClicked {

    private TabLayout tabLayout;
    private OrdersContract.Presenter presenter;

    private OrdersAdapter ordersAdapter;

    private RecyclerView ordersListView;
    private View emptyView;

    public OrdersFragment() {
        // Require empty constructor
    }

    public static OrdersFragment getInstance() {
        return new OrdersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ordersAdapter = OrdersAdapter.newInstance(getActivity(), this, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_orders, container, false);

        ordersListView = (RecyclerView) root.findViewById(R.id.list_orders);
        emptyView = root.findViewById(R.id.willy_view);
        ((TextView)emptyView.findViewById(R.id.willy_message)).setText(getString(R.string.empty_orders));

        ordersListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ordersListView.setAdapter(ordersAdapter);

        tabLayout = (TabLayout) root.findViewById(R.id.tab_parent);
        initTabs();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            presenter.start();
        }catch (NullPointerException ex){
        }
    }

    private void initTabs() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_active)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_history)));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ordersAdapter.removeOrders();

                if (tab.getPosition() == 0) {
                    presenter.getOrders();
                } else {
                    presenter.getHistoryOrders();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void setPresenter(OrdersContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showOrders(List<SmallOrder> orders) {
        if(orders.size() >0) {
            ordersListView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            orders.removeAll(Collections.singleton(null));
            Collections.sort(orders);

            ordersAdapter.rechargeOrders(orders);
            setTab(0);
        } else {
            ordersListView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showHistoryOrders(List<SmallOrder> orders, boolean isH) {
        Log.d("Data dev", "Data: "+ orders.size());

        if(orders.size() >0) {
            ordersListView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            orders.removeAll(Collections.singleton(null));
            Collections.sort(orders);

            ordersAdapter.rechargeOrders(orders);

            if(isH)
                setTab(1);
            else
                setTab(0);

        } else {
            ordersListView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoadOrdersError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError(boolean show) {
        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTab(int position) {
        if(tabLayout.getTabCount() <= position)
            return;

        tabLayout.getTabAt(position).select();
    }

    @Override
    public void showNoOrders() {
        ordersListView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onOrder(SmallOrder order) {
        Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);

        if(order != null)
            intent.putExtra("order", order.toString());

        startActivityForResult(intent, 0);
    }
}
