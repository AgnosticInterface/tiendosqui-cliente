package com.tiendosqui.dapp.orders;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.utils.ActivityUtils;
import com.tiendosqui.dapp.utils.StateBar;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by divait on 31/10/2016.
 *
 * The view controller for the items in the orders list.
 *
 */

public class OrdersListViewHolder extends RecyclerView.ViewHolder {
    private SmallOrder order;
    private Context context;

    private View view;

    private final TextView titleView;
    private final TextView openView;
    private final TextView shopNameView;
    private final TextView detailsView;
    private final TextView timeView;
    private final TextView time1View;
    private final TextView time2View;
    private final TextView time3View;
    private final ImageView imageView;
    private final StateBar stateView;

    private OnOrderClickedListener callback;

    // Interface to get the event of item click in the list
    public interface OnOrderClickedListener {
        void onOrderClicked(SmallOrder order, View item);
    }

    public OrdersListViewHolder(View v, Context context, OnOrderClickedListener callback) {
        super(v);
        this.context = context;
        this.callback = callback;

        view = v;

        titleView = (TextView) v.findViewById(R.id.order_title);
        shopNameView = (TextView) v.findViewById(R.id.order_shop_name);
        detailsView = (TextView) v.findViewById(R.id.order_details);
        timeView = (TextView) v.findViewById(R.id.order_time);
        time1View = (TextView) v.findViewById(R.id.state_1_time);
        time2View = (TextView) v.findViewById(R.id.state_2_time);
        time3View = (TextView) v.findViewById(R.id.state_3_time);
        openView = (TextView) v.findViewById(R.id.order_open);
        stateView = (StateBar) v.findViewById(R.id.state_bar);
        imageView = (ImageView) v.findViewById(R.id.user_img);

        // Define click listener for the ViewHolder's View.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOrderClicked(v);
            }
        });

    }

    private TextView getTitleView() {
        return titleView;
    }

    private TextView getOpenView() {
        return openView;
    }

    private TextView getDetailsView() {
        return detailsView;
    }

    private TextView getShopNameView() {
        return shopNameView;
    }

    private StateBar getStateView() {
        return stateView;
    }

    public SmallOrder getOrder() {
        return order;
    }

    public View getView() {
        return view;
    }

    public TextView getTime1View() {
        return time1View;
    }

    public TextView getTime2View() {
        return time2View;
    }

    public TextView getTime3View() {
        return time3View;
    }

    public TextView getTimeView() {
        return timeView;
    }

    private void onOrderClicked (View v) {
        if(order != null) {
            callback.onOrderClicked(order, v);
        }
    }

    public void setData (SmallOrder order, Activity activity) {
        this.order = order;

        String total = order.getTotal();

        try {
            total = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(total));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        getTitleView().setText(context.getString(R.string.order_title, order.getId()));
        getDetailsView().setText(context.getString(R.string.order_desc, order.getTotalProducts(), total));
        getShopNameView().setText(order.getSeller().getName());
        getTimeView().setText("");

        if(order.getStatus() == 3) { // Reject
            getStateView().setCurrentState(changeState(StateBar.STATE_DELIVERED), true);
            ((TextView)getView().findViewById(R.id.last_state)).setText(R.string.reject_state);
        } else {
            getStateView().setCurrentState(changeState(order.getStatus()));
            ((TextView)getView().findViewById(R.id.last_state)).setText(R.string.end_state);

            if(order.getStatus() == 2)
                getTimeView().setText(context.getString(R.string.time_delivery, order.getTime()));
        }

        if(order.getDateSend() != null)
            getTime1View().setText(ActivityUtils.formatTimeToList(order.getDateSend(), context));
        else
            getTime1View().setText(context.getString(R.string.pending));

        if(order.getDateConfirm() != null)
            getTime2View().setText(ActivityUtils.formatTimeToList(order.getDateConfirm(), context));
        else
            getTime2View().setText(context.getString(R.string.pending));

        if(order.getDateEnd() != null)
            getTime3View().setText(ActivityUtils.formatTimeToList(order.getDateEnd(), context));

        if(order.getDateReject() != null)
            getTime3View().setText(ActivityUtils.formatTimeToList(order.getDateReject(), context));

        if (order.getDateEnd() == null && order.getDateReject() == null)
            getTime3View().setText(context.getString(R.string.pending));

        ImageLoader.loadCircleImageFromURL(order.getSeller().getPicture(), imageView, R.drawable.will, activity);
    }

    private int changeState(int status) {
        switch (status) {
            default:
            case 1:
                return StateBar.STATE_ARRIVE;
            case 2:
                return StateBar.STATE_CONFIRMED;
            case 3:
                return StateBar.STATE_REJECTED;
            case 4:
                return StateBar.STATE_DELIVERED;
        }
    }
/*
    private void setTime(String time) {
        getStateView().setText(time);
    }

    private void setDefaultView () {
        getOpenView().setTypeface(null, Typeface.NORMAL);
        getTitleView().setTypeface(null, Typeface.NORMAL);
        getShopNameView().setTypeface(null, Typeface.NORMAL);
        getStateView().setTypeface(null, Typeface.NORMAL);
        getProductsView().setTypeface(null, Typeface.NORMAL);
        getAddressView().setTypeface(null, Typeface.NORMAL);
        getTotalView().setTypeface(null, Typeface.NORMAL);

        getStateView().setBackgroundResource(R.color.grey);
        getOpenView().setTextColor(context.getResources().getColor(R.color.grey));
        getTitleView().setTextColor(context.getResources().getColor(android.R.color.black));
        getShopNameView().setTextColor(context.getResources().getColor(R.color.grey));
        getProductsView().setTextColor(context.getResources().getColor(R.color.grey));
        getAddressView().setTextColor(context.getResources().getColor(R.color.grey));
        getTotalView().setTextColor(context.getResources().getColor(R.color.grey));
    }
*/
    public static class NoInternetViewHolder extends RecyclerView.ViewHolder {
        public NoInternetViewHolder(View v) {
            super(v);
        }

    }

}
