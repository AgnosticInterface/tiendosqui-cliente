package com.tiendosqui.dapp.orders;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.entities.json.OrderProcessJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by divait on 05/04/2017.
 */

public class OrdersController {
    private Context oc_context;
    private HttpCalls oc_httpCalls;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onLoadOrdersFail (String msg);

        void onLoadOrders(List<SmallOrder> orders);

        void onLoadHistoryOrders(List<SmallOrder> orders, boolean isH);

        void noUser();
    }

    public OrdersController(Context context, HttpCalls httpCalls) {
        oc_context = context;

        if (httpCalls != null) {
            oc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getOrders(Callback callback) {
        User user = DataStorage.getUser(oc_context);

        if(user == null) {
            callback.noUser();
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(oc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadOrders(user.getId(), callback);
    }

    private void attemptLoadOrders(long id, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(oc_context));

        oc_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.ORDERS_URL_START) +  id + NetworkInfo.ORDERS_URL_END, NetworkInfo.ORDERS_TAG, null, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data orders: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }
    private void onSuccessResponse(String response, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Json orders: " +json);
        } catch (JSONException e) {
            callback.onServerError(oc_context.getString(R.string.error_loading_orders));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            OrderProcessJson[] orders = gson.fromJson(json.toString(), OrderProcessJson[].class);

            List<SmallOrder>  ordersList = new ArrayList<>();

            for(OrderProcessJson order: orders) {
                ordersList.add(OrderProcessJson.toOrder(order));
            }
            callback.onLoadOrders(ordersList);
        } catch (Exception ex) {
            callback.onLoadOrdersFail(oc_context.getString(R.string.error_loading_orders));
            ex.printStackTrace();
        }
    }

    public void getHistoryOrders(boolean isH, Callback callback) {
        User user = DataStorage.getUser(oc_context);

        if(user == null) {
            callback.noUser();
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(oc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadHistoryOrders(user.getId(), isH, callback);
    }

    private void attemptLoadHistoryOrders(long id, final boolean isH, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(oc_context));

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.ORDERS_POST_PARAMETER_ID,  Long.toString(id));
        params.put(NetworkInfo.ORDERS_POST_PARAMETER_OFFSET, "20");
        if(!isH)
            params.put(NetworkInfo.ORDERS_POST_PARAMETER_IDS, "1,2,0,0");
        else
            params.put(NetworkInfo.ORDERS_POST_PARAMETER_IDS, "3,4,0,0");

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.ORDERS_POST_PARAMETER_ID,  id);
            body.put(NetworkInfo.ORDERS_POST_PARAMETER_OFFSET, 20);
            if(!isH)
                body.put(NetworkInfo.ORDERS_POST_PARAMETER_IDS, "1,2,0,0");
            else
                body.put(NetworkInfo.ORDERS_POST_PARAMETER_IDS, "3,4,0,0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Data dev", "body: " + body.toString());

        oc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.ORDERS_URL), NetworkInfo.ORDERS_TAG, params, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data orders: " +response);
                onSuccessResponseH(response, isH, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }
    private void onSuccessResponseH(String response, boolean isH, Callback callback) {
        JSONArray json;
        try {
            JSONArray fJson = new JSONArray(response);

            json = fJson.getJSONObject(0).getJSONArray("orders");
            Log.d("Data dev", "Json orders history: " +json);
        } catch (JSONException e) {
            callback.onServerError(oc_context.getString(R.string.error_loading_orders));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            OrderProcessJson[] orders = gson.fromJson(json.toString(), OrderProcessJson[].class);

            List<SmallOrder>  ordersList = new ArrayList<>();

            for(OrderProcessJson order: orders) {
                if(isH) {
                    if(order.getStatus_order().getId() == 3 || order.getStatus_order().getId() == 4)
                        ordersList.add(OrderProcessJson.toOrder(order));
                } else {
                    if(order.getStatus_order().getId() == 1 || order.getStatus_order().getId() == 2)
                        ordersList.add(OrderProcessJson.toOrder(order));
                }
            }

            callback.onLoadHistoryOrders(ordersList, isH);
        } catch (Exception ex) {
            callback.onLoadOrdersFail(oc_context.getString(R.string.error_loading_orders));
            ex.printStackTrace();
        }
    }
}
