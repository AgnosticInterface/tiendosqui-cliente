package com.tiendosqui.dapp.utils;

import android.app.Activity;
import android.location.Geocoder;
import android.util.Log;

import com.tiendosqui.dapp.entities.Address;

import java.util.List;
import java.util.Locale;

/**
 * Created by Divait on 22/03/2017.
 *
 * Manage Address from google and user.
 */

public class AddressCreator {

    public static Address fromLocationToAddress(Activity activity, double latitude, double longitude) { // return Address
        try{
            Geocoder geo = new Geocoder(activity.getApplicationContext(), Locale.getDefault());
            List<android.location.Address> addresses = geo.getFromLocation(latitude, longitude, 1);
            if (addresses.isEmpty()) {
                return null;
            }
            else {
                if (addresses.size() > 0) {
                    Log.d("Data dev", addresses.get(0).toString());
                    DataStorage.setCity(activity, addresses.get(0).getLocality());
                    return new Address(
                        addresses.get(0).getAddressLine(0),
                        latitude,
                        longitude
                    );

                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    private static String findNomenclature(String data) {
        String[] split = data.split(" ");

        if(split.length > 0)
            return split[0];

        return data;
    }

    private static String findStreet(String data) {
        String[] split = data.split(" ");

        if(split.length > 1)
            return split[1];

        return "";
    }

    private static String findNumber(String data) {
        String[] split = data.split(" ");

        if(split.length > 0)
            for (String element : split) {
                if (element.contains("-")) {
                    String[] splitNum = element.replace("#", "").split("-");
                    if(splitNum.length > 0)
                        return splitNum[0];
                }
            }

        return "";
    }
}
