package com.tiendosqui.dapp.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tiendosqui.dapp.R;

/**
 * Created by divait on 24/03/2017.
 *
 * Empty fragment.
 */

public class EmptyFragment extends Fragment {

    public static EmptyFragment getInstance() {
        return new EmptyFragment();
    }

    public EmptyFragment() {
        // Require empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.element_empty_willy, container, false);

        TextView text = (TextView) root.findViewById(R.id.willy_message);
        text.setText(getString(R.string.coming_soon));

        root.setVisibility(View.VISIBLE);
        return root;
    }
}
