package com.tiendosqui.dapp.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by divait on 05/05/2017.
 */

public class DatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private Callback callback;

    public interface Callback{
        void onDateSelect(String date);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
        // Do something with the date chosen by the user

        if(callback != null)
            callback.onDateSelect((dayOfMonth<10?"0":"") + dayOfMonth + "-" + (month<10?"0":"") + month + "-" + year);

    }

    public void setOnDateSelected(Callback callback) {
        this.callback = callback;
    }
}
