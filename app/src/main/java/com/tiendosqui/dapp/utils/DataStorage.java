package com.tiendosqui.dapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.entities.json.JsonCity;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 */

public class DataStorage {
    private static final String PREFERENCES = "prefs";

    private static final String PREFERENCE_ADDRESS = "address";
    private static final String PREFERENCE_ONBOARDING = "on_boarding";
    private static final String PREFERENCE_ORDER = "order";
    private static final String PREFERENCE_SUPPORT_NUMBER = "support_num";

    private static final String PREFERENCE_DEVICE_ID = "device_id";
    private static final String PREFERENCE_CITY = "city";

    // Address
    public static void setAddress (Context context, Address address) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Gson gson = new Gson();

        editor.putString(PREFERENCE_ADDRESS, gson.toJson(address));
        editor.apply();
    }

    public static Address getAddress(Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);

        Gson gson = new Gson();

        return gson.fromJson(settings.getString(PREFERENCE_ADDRESS, null), Address.class);
    }

    public static void deleteAddress(Context context) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_ADDRESS, null);
        editor.apply();
    }

    // Order
    static MainOrder mainOrder;

    public static void setOrder (Context context, Order order) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        if(getOrders(context) == null) {
            mainOrder = new MainOrder();
        }

        if(order != null)
            mainOrder.addOrder(order);

        Gson gson = new Gson();

        editor.putString(PREFERENCE_ORDER, gson.toJson(mainOrder));
        editor.apply();
    }

    public static MainOrder getOrders(Context context) {
        if(mainOrder != null)
            return mainOrder;

        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        mainOrder = gson.fromJson(settings.getString(PREFERENCE_ORDER, null), MainOrder.class);

        return mainOrder;
    }

    public static Order getOrder(Context context, long sellerId) {
        if(getOrders(context) == null)
            return null;

        Log.d("Data dev", "Get Order: " + getOrders(context) + ", " + sellerId);
        return getOrders(context).getOrder(sellerId);
    }

    public static void deleteOrder (Context context, long sellerId) {
        if(getOrders(context) == null)
            return;

        if(getOrders(context).getOrder(sellerId) == null)
            return;

        if(getOrders(context).getOrders().size() == 1) {
            DataStorage.deleteOrders(context);
        } else {
            getOrders(context).removeOrder(sellerId);
            saveMain(context, getOrders(context));
        }


    }

    public static void deleteOrders (Context context) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_ORDER, null);
        editor.apply();

        mainOrder = null;

    }

    private static void saveMain (Context context, MainOrder mainOrder) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Gson gson = new Gson();

        editor.putString(PREFERENCE_ORDER, gson.toJson(mainOrder));
        editor.apply();
    }

    // Token
    private static final String PREFERENCE_TOKEN = "token";

    public static void setToken(Context context, String deviceId) {
        SharedPreferences preferences = context.getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_TOKEN, deviceId);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences settings = context.getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        return settings.getString(PREFERENCE_TOKEN, null);
    }

    // Login
    private static final String PREFERENCE_IS_LOGGED = "isLogged";

    public static boolean isLogged (Context context) {
        SharedPreferences settings = context.getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        return settings.getBoolean(PREFERENCE_IS_LOGGED, false);
    }

    // User
    private static final String PREFERENCE_USER = "user";
    private static User user = null;

    /**
     * Save all the shop data in Share preference to keep the login session
     *
     * @param context the context of the App
     * @param user the User info to save
     */
    public static void saveUserData (Context context, User user) {
        // Get app preferences
        SharedPreferences settings = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        // Add preferences
        editor.putBoolean(PREFERENCE_IS_LOGGED, true);
        editor.putString(PREFERENCE_USER, user.toString());

        // Commit the edits!
        editor.apply();

        // Set Shop
        DataStorage.user = user;
    }

    /**
     * Destroy all the persistent data of the shopkeeper to close session
     *
     * @param context the context of the app
     */
    public static void deleteUserData (Context context) {
        // Get app preferences
        SharedPreferences settings = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        // Add preferences
        editor.putBoolean(PREFERENCE_IS_LOGGED, false);
        editor.putString(PREFERENCE_USER, null);

        // Commit the edits!
        editor.apply();

        // Set User
        DataStorage.user = null;
    }

    /**
     * get the object user save as String in a shared preference
     *
     * @param context the context of the App
     * @return the User object active in the session
     */
    public static User getUser(Context context) {

        if(user == null) {

            SharedPreferences settings = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
            String userString = settings.getString(PREFERENCE_USER, null);

            if (userString == null)
                return null;

            Gson gson = new Gson();
            user = gson.fromJson(userString, User.class);
        }

        return  user;
    }

    public static long getUserId(Context context) {
        User shop = getUser(context);
        if(shop == null) return -1;
        return shop.getId();
    }

    // Support number
    public static void setSupportNumber (Context context, String number) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_SUPPORT_NUMBER, number);
        editor.apply();
    }

    public static String getSupportNumber (Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);

        Gson gson = new Gson();
        return  settings.getString(PREFERENCE_SUPPORT_NUMBER, "300 2010137");
    }

    public static void deleteSupportNumber(Context context) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_SUPPORT_NUMBER, null);
        editor.apply();
    }

    public static void setDeviceID (Context context, String deviceId) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_DEVICE_ID, deviceId);
        editor.apply();
    }

    public static String getDeviceID(Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        return settings.getString(PREFERENCE_DEVICE_ID, null);
    }

    // onBoarding
    public static void setOnBoarding (Context context, boolean onBoarding) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(PREFERENCE_ONBOARDING, onBoarding);
        editor.apply();
    }

    public static boolean getOnBoarding(Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);


        return settings.getBoolean(PREFERENCE_ONBOARDING, false);
    }

    // City
    public static void setCity (Context context, String city) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_CITY, city);
        editor.apply();
    }

    public static String getCity(Context context) {
        SharedPreferences settings = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        return settings.getString(PREFERENCE_CITY, null);
    }

    public static void deleteCity(Context context) {
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences (PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_CITY, null);
        editor.apply();
    }

    public static String[] getCities(Context context) {
        String[] cities;
        List<JsonCity> citiesList = SugarRecord.listAll(JsonCity.class);
        if(citiesList.size() >0) {
            cities = new String[citiesList.size()];
            for(int i=0; i<cities.length;i++){
                cities[i] = citiesList.get(i).getName();
            }
        } else {
            cities = context.getResources().getStringArray(R.array.cities);
        }

        return cities;
    }
}
