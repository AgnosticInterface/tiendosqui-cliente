package com.tiendosqui.dapp.utils.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tiendosqui.dapp.utils.DataStorage;

public class TFirebaseInstanceIDService extends FirebaseInstanceIdService {
    public TFirebaseInstanceIDService() {
    }

    @Override
    public void onTokenRefresh() {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Data dev", "FCM Token: " + fcmToken);

        sendTokenToServer(fcmToken);
    }

    public void  sendTokenToServer(String token) {
        DataStorage.setDeviceID(this, token);
    }
}
