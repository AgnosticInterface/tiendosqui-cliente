package com.tiendosqui.dapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.tiendosqui.dapp.R;

/**
 * Created by Divait on 22/03/2017.
 *
 * All dialog creator.
 */

public class UtilDialog {

    public static void showDialog(Context context, String title, String message, int positiveButtonTextID, DialogInterface.OnClickListener positiveButtonCallBack, int negativeButtonTextID, DialogInterface.OnClickListener negativeButtonCallBack) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(positiveButtonTextID), positiveButtonCallBack);

        if(negativeButtonTextID >= 0)
            builder.setNegativeButton(context.getString(negativeButtonTextID), negativeButtonCallBack);

        builder.show();
    }

    public static void showDialogInput(Activity activity, String title, String message, int positiveButtonTextID, DialogInterface.OnClickListener positiveButtonCallBack, int negativeButtonTextID, DialogInterface.OnClickListener negativeButtonCallBack) {
        final FrameLayout item = (FrameLayout) activity.getLayoutInflater().inflate(R.layout.dialog_input, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(item);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(positiveButtonTextID), positiveButtonCallBack);

        if(negativeButtonTextID >= 0)
            builder.setNegativeButton(activity.getString(negativeButtonTextID), negativeButtonCallBack);

        builder.show();
    }

    public interface SupportCallback{
        void callSupport();
        void requestCallFromSupport();
    }

    public static void showDialogSupport(Activity activity, long id, final SupportCallback callback) {
        final FrameLayout item = (FrameLayout) activity.getLayoutInflater().inflate(R.layout.dialog_support, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setView(item);
        builder.setTitle( activity.getString(R.string.request_support_title, id));
        builder.setMessage("");

        final AlertDialog dialog = builder.create();

        item.findViewById(R.id.call_assistance_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.callSupport();
                dialog.dismiss();
            }
        });
        item.findViewById(R.id.request_assistance_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.requestCallFromSupport();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static void showDialogSelector(Activity activity, String title, String message, int positiveButtonTextID, DialogInterface.OnClickListener positiveButtonCallBack, int negativeButtonTextID, DialogInterface.OnClickListener negativeButtonCallBack) {
        final FrameLayout item = (FrameLayout) activity.getLayoutInflater().inflate(R.layout.dialog_selector, null);

        ArrayAdapter<String> spAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, DataStorage.getCities(activity));//ArrayAdapter.createFromResource(activity, arrayId, android.R.layout.simple_spinner_item);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((Spinner)item.findViewById(R.id.spinner_cities)).setAdapter(spAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setView(item);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(positiveButtonTextID), positiveButtonCallBack);

        if(negativeButtonTextID >= 0)
            builder.setNegativeButton(activity.getString(negativeButtonTextID), negativeButtonCallBack);

        builder.show();
    }
}
