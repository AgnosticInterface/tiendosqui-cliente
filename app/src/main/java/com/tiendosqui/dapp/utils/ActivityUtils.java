package com.tiendosqui.dapp.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.tiendosqui.dapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by Medycitas on 19/01/2017.
 */

public class ActivityUtils {

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    public static Date getUtilDate(String date) {
        if(date == null)
            return null;

        // yyyy-MM-dd'T'HH:mm:ss
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        format.setLenient(false);
        format.setTimeZone(TimeZone.getTimeZone("GMT-5:00"));

        Date mDate;

        Log.d("Data dev", "Date: " + date);
        try {
            mDate = format.parse(date);

        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
            format.setLenient(true);

            try {
                mDate = format.parse(date);
            } catch (ParseException | NullPointerException e1) {
                mDate = null;
                e1.printStackTrace();
            }
        }
        return mDate;
    }

    public static String formatTimeToList (Date date, Context context) {
        if(date == null)
            return "";

        String time = "";

        Calendar now = Calendar.getInstance();

        Calendar mDate = Calendar.getInstance();
        Log.d("Data dev", "date: "+ date);
        mDate.setTime(date);

        if (now.get(Calendar.YEAR) == mDate.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == mDate.get(Calendar.DAY_OF_YEAR)) {
            time = context.getString(R.string.today);

            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(" h:mm a", Locale.getDefault());
                time += dateFormat.format(date);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else {
            now.add(Calendar.DAY_OF_YEAR, -1);

            if (now.get(Calendar.YEAR) == mDate.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == mDate.get(Calendar.DAY_OF_YEAR)) {
                time = context.getString(R.string.yesterday);

                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(" h:mm a", Locale.getDefault());
                    time += dateFormat.format(date);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, h:mm a", Locale.getDefault());
                    time = dateFormat.format(date);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
        return time;
    }

    public static String lastVersion() {
        VersionChecker versionChecker = new VersionChecker();
        String latestVersion = null;

        try {
            latestVersion = versionChecker.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return latestVersion;
    }

    public static Calendar getCalendar(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        format.setLenient(false);

        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar;
    }
}
