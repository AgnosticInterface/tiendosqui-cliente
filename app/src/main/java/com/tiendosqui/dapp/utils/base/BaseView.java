package com.tiendosqui.dapp.utils.base;

/**
 * Created by divait on 7/10/2016.
 *
 * Interface normal View behavior.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
