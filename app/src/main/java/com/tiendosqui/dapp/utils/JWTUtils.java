package com.tiendosqui.dapp.utils;

import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by divait on 2/11/2016.
 *
 * Decode server token jwt.
 */

public class JWTUtils {

    public static JSONObject decoded(String JWTEncoded) throws Exception {
        JSONObject data = null;
        try {
            String[] split = JWTEncoded.split("\\.");
            Log.d("JWT_DECODED", "Header: " + getJson(split[0]));
            Log.d("JWT_DECODED", "Body: " + getJson(split[1]));

            data = new JSONObject( getJson(split[1]) );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.NO_WRAP);
        return new String(decodedBytes, "UTF-8");
    }
}
