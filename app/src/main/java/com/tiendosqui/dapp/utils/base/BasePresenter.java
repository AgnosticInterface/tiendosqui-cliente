package com.tiendosqui.dapp.utils.base;

import java.io.Serializable;

/**
 * Created by divait on 7/10/2016.
 *
 * Base methods for all Presenters.
 */

public interface BasePresenter extends Serializable {
    void start();
}
