package com.tiendosqui.dapp.utils.fcm;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class TFirebaseMessagingService extends FirebaseMessagingService {
    private static final int TYPE_ORDER = 1;
    private static final int TYPE_FIVE_STARTS = 2;

    private static int id_count = 12000;

    public TFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("Data dev", "From: " + remoteMessage.getFrom());
        Log.d("Data dev", "Data: " + remoteMessage.getData()); // {message={"message":"Tu orden fue confirmada","order":953}}

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0 && DataStorage.getUser(this) != null) {
            id_count++;

            JSONObject data = null;
            int orderId;
            String m = remoteMessage.getData().get("message");
            try {
                data = new JSONObject(m);
                orderId = data.getInt("order");
            } catch (JSONException e) {
                orderId = -1;
                e.printStackTrace();
            }

            if(orderId > 0 && data != null) {
                String message;

                try {
                    message = data.getString("message");

                    if(isAppForground(this)) {
                        updateOrder(message, orderId);
                    }

                    String title = null;
                    try {

                        if (data.has("order")) {
                            title = "Orden #" + data.get("order");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    createNotification(
                            orderId,
                            title != null ? title : remoteMessage.getData().get("title"),
                            message != null ? message : "Mira tus pedidos.",
                            message != null ? message : "Mira tus pedidos.",
                            remoteMessage.getData().get("title"),
                            mainPendingIntent()
                    );
                } catch (JSONException e) {
                    message = m;
                    e.printStackTrace();
                }

            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("Data dev", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    public boolean isAppForground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
        }

        return true;
    }

    private PendingIntent mainPendingIntent() {
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        resultIntent.putExtra("tab", 3);

        return PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
    }

    private void createNotification(int id, String title, String message, String bigMessage, String ticker, PendingIntent resultPendingIntent) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_willy)
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        //.setDefaults(Notification.DEFAULT_ALL)
                        //.setVibrate(new long[]{0, 500, 110, 500, 110, 450, 110, 200, 110, 170, 40, 450, 110, 200, 110, 170, 40, 500})
                        .setVibrate(new long[]{1000,200,500, 200, 500, 200, 500, 100, 500})
                        .setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notificacion))
                        .setContentTitle(title)
                        .setContentText(message)
                        .setTicker(ticker)
                        .setContentIntent(resultPendingIntent)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(bigMessage))
                        //.setUsesChronometer(true)
                        .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id, mBuilder.build());

    }

    private void updateOrder(String message, long order_id) {
        Log.d("Data dev", "Message final: " + message);

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("tab", 3);
        intent.putExtra("order_id", order_id);

        startActivity(intent);

    }
    public static void cancelNotification(Context context, int id) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(id);
    }

    public static void cancelAllNotification(Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancelAll();
    }
}
