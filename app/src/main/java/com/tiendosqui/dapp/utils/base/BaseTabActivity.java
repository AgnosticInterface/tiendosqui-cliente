package com.tiendosqui.dapp.utils.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.address.OldAddressActivity;
import com.tiendosqui.dapp.entities.Address;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.shop.ShopFragment;
import com.tiendosqui.dapp.shop_search.ShopSearchFragment;
import com.tiendosqui.dapp.tabs.TabFragment;
import com.tiendosqui.dapp.utils.DataStorage;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

/**
 * Created by divait on 19/01/2017.
 *
 * The base activity with tabs and toolbar.
 */

public class BaseTabActivity extends BaseActivity {

    private TabLayout tabLayout;

    private Unregistrar unregistrar;

    private TextView carView;

    @Override
    protected void onStop() {
        super.onStop();

        // Unregistrar
        unregistrar.unregister();
    }

    protected void initTabs(ViewPager viewPager) {
        tabLayout = (TabLayout) findViewById(R.id.tab_parent);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText(getString(R.string.tab_home)).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(1).setText(getString(R.string.tab_search)).setIcon(R.drawable.ic_search);
        tabLayout.getTabAt(2).setText(getString(R.string.tab_shop_car)).setIcon(R.drawable.ic_shopping_cart).setCustomView(R.layout.item_tab);
        tabLayout.getTabAt(3).setText(getString(R.string.tab_fav)).setIcon(R.drawable.ic_favorite);
        tabLayout.getTabAt(4).setText(getString(R.string.tab_settings)).setIcon(R.drawable.ic_settings);

        int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.black);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        View v = tabLayout.getTabAt(2).getCustomView();
        if(v != null) {
            carView = (TextView) v.findViewById(R.id.item_count);
            ((TextView)v.findViewById(R.id.name_tab)).setText(getString(R.string.tab_shop_car));

            updateCar(DataStorage.getOrders(this));
        }

        tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.tab_highlight);
        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // TODO
                int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.tab_highlight);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);
                appBarLayout.setExpanded(true, true);

                final InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(appBarLayout.getWindowToken(), 0);

                //onReSelected(tab.getPosition());

                View tabView = tab.getCustomView();
                if(tabView != null) {
                    ((TextView)tabView.findViewById(R.id.name_tab)).setTextColor(getResources().getColor(R.color.colorAccentSecondary));
                    ((ImageView)tabView.findViewById(R.id.image_tab)).setColorFilter(tabIconColor);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(getApplicationContext(), R.color.black);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                View tabView = tab.getCustomView();
                if(tabView != null) {
                    ((TextView)tabView.findViewById(R.id.name_tab)).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                    ((ImageView)tabView.findViewById(R.id.image_tab)).setColorFilter(tabIconColor);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                onReSelected(tab.getPosition());
            }
        });

        unregistrar = KeyboardVisibilityEvent.registerEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        tabLayout.setVisibility(isOpen ? View.GONE : View.VISIBLE);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected void setToolbar(boolean back) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("");
        TextView titleView = (TextView)toolbar.findViewById(R.id.toolbar_title);

        if(titleView == null)
            return;

        titleView.setText(DataStorage.getAddress(this).toAddressString());
        titleView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down,0);
        titleView.setTextColor(getResources().getColor(R.color.address_text));

        titleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BaseTabActivity.this, OldAddressActivity.class);

                Address address = DataStorage.getAddress(BaseTabActivity.this);
                if(address != null)
                    intent.putExtra("address", address.toString());

                startActivity(intent);
            }
        });

        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(back);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_close_cross));
        }
    }

    public void setToolbar(boolean back, String title, int colorId) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("");
        TextView titleView = (TextView)toolbar.findViewById(R.id.toolbar_title);

        titleView.setText(title);
        titleView.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        titleView.setTextColor(getResources().getColor(colorId));

        titleView.setOnClickListener(null);

        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(back);
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_close_cross));
        }
    }

    public void showAppBar() {
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true, true);
    }

    public void showBackButton(boolean show) {
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(show);
    }

    public void onReSelected(int position) {
        //If code necessary
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getToolbarTitle(int position) {
        switch (position){
            default:
            case TabFragment.HOME:
                return getString(R.string.tab_home);
            case TabFragment.SEARCH:
                return getString(R.string.tab_search);
            case TabFragment.CAR:
                return getString(R.string.tab_shop_car);
            case TabFragment.FAVS:
                return getString(R.string.tab_fav);
            case TabFragment.SETTINGS:
                return getString(R.string.tab_settings);
        }
    }

    public void updateCar(MainOrder orders) {

        if(carView != null) {
            if (orders != null) {
                int count = 0;

                for (Order order : orders.getOrders()) {
                    count += order.getProductsCount();
                }

                if (count <= 0)
                    carView.setVisibility(View.GONE);
                else
                    carView.setVisibility(View.VISIBLE);

                carView.setText(Integer.toString(count));
            } else {
                carView.setVisibility(View.GONE);
                carView.setText("0");
            }
        }
    }

    public void setTabToolbar(int position, TabFragment fragment) {
        boolean showBack = false;

        if(fragment == null) {
            setToolbar(false, getToolbarTitle(position), R.color.colorAccentSecondary);
            return;
        } else {
            if(fragment.size() > 1)
                showBack = true;
            setToolbar(showBack, getToolbarTitle(position), R.color.colorAccentSecondary);
        }
        switch (position){

            case TabFragment.HOME:
                if(fragment.size() <= 1) {
                    setToolbar(false);
                } else if (fragment.size() == 2) {
                    ShopFragment shopFragment = (ShopFragment) fragment.getFragment(1);
                    setToolbar(true, shopFragment.getSellerName(), R.color.colorAccentSecondary);
                } else if (fragment.size() == 3) {
                    ShopSearchFragment shopFragment = (ShopSearchFragment) fragment.getFragment(2);
                    setToolbar(true, shopFragment.getSellerName(), R.color.colorAccentSecondary);
                }
                break;
            case TabFragment.SEARCH:
                break;

            case TabFragment.FAVS:
                break;
        }
    }
}
