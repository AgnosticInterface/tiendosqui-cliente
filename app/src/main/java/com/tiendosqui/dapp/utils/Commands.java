package com.tiendosqui.dapp.utils;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.orm.SugarRecord;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.entities.json.JsonCity;
import com.tiendosqui.dapp.network.NetworkInfo;

/**
 * Created by divait on 14/06/2017.
 */

public class Commands {

    private static final String COMMAND_DEV_URL = "tiendosqui_dev_url";
    private static final String COMMAND_TESTING_URL = "tiendosqui_testing_url";
    private static final String COMMAND_CURRENT_URL = "tiendosqui_url";
    private static final String COMMAND_LOGIN_URL = "tiendosqui_login_url";
    private static final String COMMAND_ = "tiendosqui_";

    public static boolean isCommand(String command) {
        if(     command.equals(COMMAND_DEV_URL) ||
                command.equals(COMMAND_TESTING_URL) ||
                command.equals(COMMAND_CURRENT_URL) ||
                command.equals(COMMAND_LOGIN_URL)
            ) {
            return true;
        }

        return  false;
    }

    public static boolean isDevMode(Context context) {
        // return context.getResources().getBoolean(R.bool.developer);
        return false;
    }

    public static void executeCommand(String command, Context context) {
        switch (command) {
            case COMMAND_DEV_URL:
                NetworkInfo.setURL(NetworkInfo.ID_DEV);
                deleteData(context);

                showMsg("Dev URL set, orders delete.", context);
                break;
            case COMMAND_TESTING_URL:
                NetworkInfo.setURL(NetworkInfo.ID_TEST);
                deleteData(context);

                showMsg("Testing URL set, orders delete.", context);
                break;
            case COMMAND_CURRENT_URL:

                showMsg("Current URL: " + NetworkInfo.getURL(), context);
                break;
            case COMMAND_LOGIN_URL:

                showMsg("Login URL: " + NetworkInfo.LOGIN_URL, context);
                break;
        }
    }

    private static void showMsg(String msg, Context context){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private static void deleteData(Context context) {
        DataStorage.deleteOrders(context);
        DataStorage.deleteUserData(context);
        DataStorage.deleteAddress(context);
        DataStorage.deleteSupportNumber(context);
        DataStorage.deleteCity(context);

        SugarRecord.deleteAll(Seller.class);
        SugarRecord.deleteAll(JsonCity.class);

        Intent i = context.getPackageManager()
                .getLaunchIntentForPackage( context.getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }
}
