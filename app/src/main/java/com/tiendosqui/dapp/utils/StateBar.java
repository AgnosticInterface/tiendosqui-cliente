package com.tiendosqui.dapp.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tiendosqui.dapp.R;

/**
 * Created by Divait on 16/02/2017.
 */

public class StateBar extends LinearLayout {
    public static final int STATE_ARRIVE = 0x000;
    public static final int STATE_CONFIRMED = 0x001;
    public static final int STATE_DELIVERED = 0x002;
    public static final int STATE_REJECTED = 0x003;

    private Context context;

    private int _items;
    private int _current;
    private int _icon;
    private int _icon_error;
    private int _colorDisable;
    private int _colorEnable;
    private int _colorError;

    public StateBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StateBar,
                0, 0);

        try {
            _items = a.getInteger(R.styleable.StateBar_items, 1);
            _current = a.getInteger(R.styleable.StateBar_currentState, -1);
            _icon = a.getResourceId(R.styleable.StateBar_iconItem, R.mipmap.ic_done);
            _icon_error = R.drawable.ic_close_cross_white;
            _colorDisable = a.getColor(R.styleable.StateBar_colorDisableItem, getResources().getColor(R.color.grey));
            _colorEnable = a.getColor(R.styleable.StateBar_colorEnableItem, getResources().getColor(R.color.green_confirm));
            _colorError = a.getColor(R.styleable.StateBar_colorErrorItem, getResources().getColor(R.color.red_remove));
        } finally {
            a.recycle();
        }

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        createView(false);
    }

    private void createView(boolean error) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for(int i =0; i<_items; i++) {
            if(i == _items - 1)
                addView(createDot(inflater, i <= _current, error));
            else {
                if(i != 0 && error) {
                    addView(createDot(inflater, false, false));
                } else
                    addView(createDot(inflater, i <= _current, false));
            }

            if(i < _items -1) {
                if(!error)
                    addView(createLine(i + 1 <= _current));
                else
                    addView(createLine(false));
            }
        }
    }

    private FrameLayout createDot(LayoutInflater inflater, boolean active, boolean error) {
        FrameLayout dot = (FrameLayout) inflater.inflate(R.layout.status_bar_dot, null);

        Drawable initialColor = context.getResources().getDrawable(R.drawable.circle_white);

        if(!error) {
            if (active) {
                initialColor.setColorFilter(new
                        PorterDuffColorFilter(_colorEnable, PorterDuff.Mode.MULTIPLY));

                ((ImageView)dot.findViewById(R.id.image_dot)).setImageResource(_icon);
            } else {
                initialColor.setColorFilter(new
                        PorterDuffColorFilter(_colorDisable, PorterDuff.Mode.MULTIPLY));

                ((ImageView)dot.findViewById(R.id.image_dot)).setImageResource(R.drawable.circle_white);
            }


        } else {
            initialColor.setColorFilter(new
                    PorterDuffColorFilter(_colorError, PorterDuff.Mode.MULTIPLY));

            ((ImageView)dot.findViewById(R.id.image_dot)).setImageResource(_icon_error);
        }

        dot.setBackground(initialColor);


        return dot;
    }

    private View createLine(boolean active) {
        View line = new View(context);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                0, 3, 1.0f);

        line.setLayoutParams(params);

        if(active)
            line.setBackgroundColor(_colorEnable);
        else
            line.setBackgroundColor(_colorDisable);

        return line;
    }

    public void setCurrentState(int state) {
        _current = state;
        removeAllViews();
        createView(false);
    }

    public void setCurrentState(int state, boolean error) {
        _current = state;
        removeAllViews();
        createView(error);
    }
}
