package com.tiendosqui.dapp.register;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by Divait on 10/04/2017.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter> {
        void showProgress(boolean show);

        void setNameError(String error);

        void setEmailError(String error);

        void setPasswordError(String error);

        void setConfPasswordError(String error);

        void setPhoneError(String error);

        void setBirthdateError(String error);

        void showRegisterError(String msg);

        void showPrevActivity();

        void showServerError(String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void attemptRegister(String name, String email, String password, String rePassword, String phone, String date, boolean isCheck);
    }
}
