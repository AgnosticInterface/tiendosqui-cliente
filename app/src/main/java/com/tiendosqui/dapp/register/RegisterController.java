package com.tiendosqui.dapp.register;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.ActivityUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Divait on 10/04/2017.
 *
 * The controller for the register.
 */

public class RegisterController {

    private final Context ri_context;
    private HttpCalls ri_httpCalls;

    public interface Callback {

        void onNameError(String msg);

        void onEmailError(String msg);

        void onPasswordError(String msg);

        void onRePasswordError(String msg);

        void onPhoneError(String msg);

        void onDateError(String msg);

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onAuthFailed(String msg);

        void onAuthSuccess();
    }

    public RegisterController(Context context, HttpCalls httpCalls) {
        ri_context = context;
        if (httpCalls != null) {
            ri_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    /**
     * Receive and validate the information to send a register request.
     * Show the error if exist or use http call to perform the login if data is correct.
     *
     * @param name to register.
     * @param email to register.
     * @param name to register.
     * @param password to register.
     * @param rePassword to register.
     * @param phone to register.
     * @param date to register.
     * @param callback where register request will be return.
     */
    public void register(String name, String email, String password, String rePassword, String phone, String date, boolean isCheck, final Callback callback) {
        // Check logic
        boolean validName = isValidData(name, callback, 1);
        boolean validEmail = isValidEmail(email, callback);
        boolean validPass = isValidPassword(password, rePassword, callback);
        boolean validPhone = isValidData(phone, callback, 2);
        boolean validDate = isValidData(date, callback, 3);
        if (!( validName && validEmail && validPass && validPhone && validDate)) {
            return;
        }

        if(!isCheck) {
            callback.onAuthFailed(ri_context.getString(R.string.check_terms_msg));
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(ri_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        // Make register call
        signUpUser(name, email, password, rePassword, phone, date, callback);

    }

    /**
     * Check if the data is useful.
     *
     * @param data the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidData(String data, Callback callback, int position) {
        boolean isValid = true;
        if (TextUtils.isEmpty(data)) {
            switch (position) {
                default:
                case 1:
                    callback.onNameError(ri_context.getString(R.string.error_empty_name));
                    break;
                case 2:
                    callback.onPhoneError(ri_context.getString(R.string.error_empty_phone));
                    break;
                case 3:
                    callback.onDateError(ri_context.getString(R.string.error_empty_date));
                    break;
            }
            isValid = false;
        } else {
            if(position == 3) {
                Calendar mCal = ActivityUtils.getCalendar(data);
                Calendar now = Calendar.getInstance();

                if(mCal.get(Calendar.YEAR) >= now.get(Calendar.YEAR)) {
                    callback.onDateError(ri_context.getString(R.string.error_incorrect_birthday));
                    isValid = false;
                }
            }
        }

        return isValid;
    }

    /**
     * Check if the password is useful.
     *
     * @param password the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidPassword(String password, String rePassword, Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(password)) {
            callback.onPasswordError(ri_context.getString(R.string.error_empty_password));
            isValid = false;
        } else if (!password.equals(rePassword)){
            callback.onRePasswordError(ri_context.getString(R.string.error_no_match_password));
            isValid = false;
        }
        return isValid;
    }

    /**
     * Check if the username is useful.
     *
     * @param email the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidEmail(String email, Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(ri_context.getString(R.string.error_empty_email));
            isValid = false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(ri_context.getString(R.string.error_reg_invalid_email));
            isValid = false;
        }
        return isValid;
    }

    /**
     * Make a call to register user in the system, report if any error or get token if login complete.
     *
     * @param name to register.
     * @param email to register.
     * @param password to register.
     * @param rePassword to register.
     * @param phone to register.
     * @param date to register.
     * @param callback to send the respond of the system.
     */
    private void signUpUser(String name, String email, String password, String rePassword, String phone, String date, final Callback callback) {
        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.REGISTER_POST_PARAMETER_NAME,  name);
            body.put(NetworkInfo.REGISTER_POST_PARAMETER_EMAIL, email);
            body.put(NetworkInfo.REGISTER_POST_PARAMETER_PASSWORD, password);
            body.put(NetworkInfo.REGISTER_POST_PARAMETER_BIRTHDATE, date);
            body.put(NetworkInfo.REGISTER_POST_PARAMETER_PHONE, phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ri_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.REGISTER_URL), NetworkInfo.REGISTER_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data1: " + response);
                JSONObject json;
                try {
                    json = new JSONObject(response);

                    if(json.has("petition")) {
                        if(json.getString("petition").trim().equals("OK"))
                            callback.onAuthSuccess();
                        else
                            callback.onAuthFailed(json.getString("detail"));
                    } else
                        callback.onAuthFailed(ri_context.getString(R.string.error_register));
                } catch (Exception e) {
                    e.printStackTrace();

                    callback.onServerError(ri_context.getString(R.string.error_register));
                }
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }
}
