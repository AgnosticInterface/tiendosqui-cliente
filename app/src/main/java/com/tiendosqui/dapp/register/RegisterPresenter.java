package com.tiendosqui.dapp.register;

import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Created by Divait on 10/04/2017.
 *
 * The Presenter for the register.
 */

public class RegisterPresenter implements RegisterContract.Presenter, RegisterController.Callback{

    private final RegisterContract.View rp_registerView;
    private RegisterController rp_registerController;
    private Activity activity;

    public RegisterPresenter(@NonNull RegisterContract.View registerView,
                          @NonNull RegisterController registerController, Activity activity) {

        rp_registerView = registerView;
        registerView.setPresenter(this);
        rp_registerController = registerController;

        this.activity = activity;
    }

    @Override
    public void start() {
    }

    @Override
    public void attemptRegister(String name, String email, String password, String rePassword, String phone, String date, boolean isCheck) {
        rp_registerView.showProgress(true);
        rp_registerController.register(name, email, password, rePassword, phone, date, isCheck, this);
    }

    // Callback
    @Override
    public void onNameError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setNameError(msg);
    }

    @Override
    public void onEmailError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setEmailError(msg);
    }

    @Override
    public void onPasswordError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setPasswordError(msg);
    }

    @Override
    public void onRePasswordError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setConfPasswordError(msg);
    }

    @Override
    public void onPhoneError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setPhoneError(msg);
    }

    @Override
    public void onDateError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.setBirthdateError(msg);
    }

    @Override
    public void onNetworkConnectFailed() {
        rp_registerView.showProgress(false);
        rp_registerView.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.showServerError(msg);
    }

    @Override
    public void onAuthFailed(String msg) {
        rp_registerView.showProgress(false);
        rp_registerView.showRegisterError(msg);
    }

    @Override
    public void onAuthSuccess() {
        rp_registerView.showProgress(false);
        rp_registerView.showPrevActivity();
    }
}
