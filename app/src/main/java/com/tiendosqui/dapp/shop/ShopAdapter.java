package com.tiendosqui.dapp.shop;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Category;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.OrderComment;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.shop_car.CommentViewHolder;
import com.tiendosqui.dapp.shop_car.ShopInfoViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Divait on 25/01/2017.
 */

public class ShopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ProductViewHolder.onItemClicked, CommentViewHolder.onCommentClicked {
    private final int OTHER = 0x000;
    private final int CATEGORY = 0x001;
    private final int PRODUCT = 0x002;
    private final int COMMENT = 0x003;
    private final int ORDER = 0x004;

    private List<Object> objects;
    private Context context;
    private ShopAdapter.onItemClicked listener;

    public class TitleViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public TitleViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);

        }
    }

    public interface onItemClicked {
        public void onAdd(Product product, ProductViewHolder holder);
        public void onRemove(Product product);
        public void onComment(CheckBox checkBox, long sellerId);
    }

    public static ShopAdapter newInstance(Context context, ShopAdapter.onItemClicked listener) {
        return new ShopAdapter(new ArrayList<>(), context, listener);
    }

    public ShopAdapter(List<Object> objects, Context context, ShopAdapter.onItemClicked listener) {
        this.objects = objects;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        switch (viewType) {
            case PRODUCT:
                v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_product_shop, parent, false);
                return new ProductViewHolder(v, context, this);
            case COMMENT:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_car_comment, parent, false);
                return new CommentViewHolder(v, this);

            case ORDER:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.element_shop_info, parent, false);
                return new ShopInfoViewHolder(v, context);
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_category_shop, parent, false);
                return new TitleViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case PRODUCT:

                Product product = (Product) objects.get(position);
                ((ProductViewHolder)holder).configureViewHolder(product);
                break;
            case CATEGORY:

                Category category = (Category) objects.get(position);
                ((TitleViewHolder)holder).title.setText(category.getTitle() + "(" + category.getQuantity() + ")");
                break;
            case COMMENT:
                OrderComment comment = (OrderComment) objects.get(position);
                ((CommentViewHolder) holder).setData(comment);
                break;
            case ORDER:
                Order order = (Order) objects.get(position);
                ((ShopInfoViewHolder) holder).configureView(order);
                break;
            default:
                ((TitleViewHolder)holder).title.setText(context.getString(R.string.app_name));
                break;
        }


    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof Product) {
            return PRODUCT;
        } else if(objects.get(position) instanceof Category) {
            return CATEGORY;
        } else if(objects.get(position) instanceof OrderComment) {
            return COMMENT;
        } else if(objects.get(position) instanceof Order) {
            return ORDER;
        }
        return OTHER;
    }

    public void rechargeObjects(List<Object> products) {
        this.objects.clear();
        this.objects.addAll(products);

        notifyDataSetChanged();
    }

    public void rechargeProducts(List<Product> products) {
        this.objects.clear();
        this.objects.addAll(products);

        notifyDataSetChanged();
    }

    public void removeAll() {
        this.objects.clear();

        notifyDataSetChanged();
    }

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        listener.onAdd(product, holder);
    }

    @Override
    public void onRemove(Product product) {
        listener.onRemove(product);
    }

    @Override
    public void onClick(CheckBox checkBox, long sellerId) {
        if(checkBox.isChecked()) {
            checkBox.setChecked(false);
            listener.onComment(checkBox, sellerId);
        } else {
            checkBox.setText(context.getString(R.string.add_comment));
        }

    }

    public List<Object> getObjects() {
        return objects;
    }
}
