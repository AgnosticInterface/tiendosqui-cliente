package com.tiendosqui.dapp.shop_search;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.shop.ProductViewHolder;
import com.tiendosqui.dapp.shop.ShopAdapter;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;

import java.util.List;

/**
 * Created by divait on 19/04/2017.
 */

public class ShopSearchFragment extends TabChildFragment implements ShopAdapter.onItemClicked, ShopSearchContract.View {
    private Seller seller;

    private RecyclerView searchList;
    private ShopAdapter searchAdapter;

    private EditText searchBox;

    private ShopSearchContract.Presenter presenter;

    public ShopSearchFragment() {
        // Require empty constructor
    }

    public static ShopSearchFragment getInstance(Seller seller) {
        ShopSearchFragment shopSearchFragment = new ShopSearchFragment();

        Bundle bundle = new Bundle();
        bundle.putString("seller", seller.toString());
        shopSearchFragment.setArguments(bundle);

        return shopSearchFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchAdapter = ShopAdapter.newInstance(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        Bundle args = getArguments();
        String sellerString = args.getString("seller", null);

        if(sellerString != null) {
            Gson gson = new Gson();
            seller = gson.fromJson(sellerString, Seller.class);
        }

        searchList = (RecyclerView) root.findViewById(R.id.list_search);
        searchBox = (EditText) root.findViewById(R.id.search_box);

        searchBox.setText(args.getString("word",""));
        searchBox.setSelection(searchBox.getText().length());

        searchList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        searchList.setAdapter(searchAdapter);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.searchProduct(seller.getId(), searchBox.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return  root;
    }

    @Override
    public void setPresenter(ShopSearchContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void searchResult(List<Product> products) {
        searchAdapter.rechargeProducts(products);
    }

    @Override
    public void showSearchError(String msg) {
        searchAdapter.removeAll();
        // Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());
        if(mainOrder != null && mainOrder.hasOrder(seller.getId())) {
            presenter.addItem(product);
            holder.addQuantity();
        }else {
            UtilDialog.showDialog(getActivity(),
                    getString(R.string.dialog_delete_order_title),
                    getString(R.string.dialog_delete_order_desc),
                    R.string.dialog_delete_order,
                    deleteOrderClick(product, holder),
                    R.string.dialog_cancel,
                    null
            );
        }
    }

    public DialogInterface.OnClickListener deleteOrderClick(final Product product, final ProductViewHolder holder) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataStorage.setOrder(getActivity(), new Order(seller, DataStorage.getAddress(getActivity())));
                presenter.addItem(product);
                holder.addQuantity();
            }
        };
    }

    @Override
    public void onRemove(Product product) {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());
        if(mainOrder != null && mainOrder.hasOrder(seller.getId()))
            presenter.removeItem(product);
    }

    @Override
    public void onComment(CheckBox checkBox, long sellerId) {
        // Not used here
    }

    public String getSellerName() {
        if(seller == null)
            return getString(R.string.app_name);

        return seller.getName();
    }
}
