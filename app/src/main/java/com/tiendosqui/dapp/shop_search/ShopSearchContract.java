package com.tiendosqui.dapp.shop_search;

import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

import java.util.List;

/**
 * Created by divai on 24/03/2017.
 */

public interface ShopSearchContract {
    interface View extends BaseView<ShopSearchContract.Presenter> {

        void searchResult(List<Product> products);

        void showSearchError(String msg);

        void showServerError(String msg);

        void showNetworkError();

    }

    interface Presenter extends BasePresenter {

        void searchProduct(long id, String keyWord);

        void addItem (Product product);

        void removeItem (Product product);
    }
}
