package com.tiendosqui.dapp.shop_search;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Product;

import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The presenter for
 */

public class ShopSearchPresenter implements ShopSearchContract.Presenter, ShopSearchController.Callback {

    private ShopSearchContract.View sp_view;
    private ShopSearchController sp_controller;

    public ShopSearchPresenter(@NonNull ShopSearchContract.View mainView,
                               @NonNull ShopSearchController mainController) {

        sp_view = mainView;
        sp_controller = mainController;
    }

    @Override
    public void start() {
    }

    @Override
    public void onNetworkConnectFailed() {
        sp_view.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        sp_view.showServerError(msg);
    }

    @Override
    public void onSearchFail(String msg) {
        sp_view.showSearchError(msg);
    }

    @Override
    public void onSearchResults(List<Product> products) {
        sp_view.searchResult(products);
    }

    @Override
    public void searchProduct(long id, String keyWord) {
        sp_controller.getProductsSearch(id, keyWord, this);
    }

    @Override
    public void addItem(Product product) {
        sp_controller.addItem(product);
    }

    @Override
    public void removeItem(Product product) {
        sp_controller.removeItem(product);
    }

}
