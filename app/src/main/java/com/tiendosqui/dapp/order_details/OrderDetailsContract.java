package com.tiendosqui.dapp.order_details;

import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by divai on 20/04/2017.
 */

public interface OrderDetailsContract {

    interface View extends BaseView<Presenter> {

        void showOrderServer(SmallOrder order);

        void showRequestAssistance(String msg);

        void showErrorRequestingAssistance (String msg);

        void showServerError (String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void getData(SmallOrder order);

        void requestAssistance(long orderId);
    }
}
