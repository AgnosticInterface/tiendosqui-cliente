package com.tiendosqui.dapp.order_details;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.network.ImageLoader;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by divait on 20/04/2017.
 */

public class OrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int PRODUCT = 0x000;
    private static final int OTHER = 0x001;

    private List<Object> products;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Product product);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView count;
        public TextView name;
        public TextView description;
        public TextView quantityX1;
        public TextView price;
        public TextView unitPrice;
        public ImageView image;

        public View item;

        public ViewHolder(View view) {
            super(view);
            count = (TextView) view.findViewById(R.id.item_count);
            name = (TextView) view.findViewById(R.id.product_name);
            description = (TextView) view.findViewById(R.id.product_details);
            quantityX1 = (TextView) view.findViewById(R.id.product_quantity);
            price = (TextView) view.findViewById(R.id.product_price);
            unitPrice = (TextView) view.findViewById(R.id.product_unit_price);
            image = (ImageView) view.findViewById(R.id.product_image);

            item = view;
        }
    }

    public class ViewHolderPrice extends RecyclerView.ViewHolder {
        public TextView subtotal;
        public TextView send_cost;
        public TextView total;

        public View item;

        public ViewHolderPrice(View view) {
            super(view);
            subtotal = (TextView) view.findViewById(R.id.products_subtotal);
            send_cost = (TextView) view.findViewById(R.id.products_send_cost);
            total = (TextView) view.findViewById(R.id.products_total);

            item = view;
        }
    }

    public static OrderDetailsAdapter newInstance(Context context, OnItemClickListener listener) {
        return new OrderDetailsAdapter(new ArrayList<Object>(), context, listener);
    }


    public OrderDetailsAdapter(List<Object> products, Context context, OnItemClickListener listener) {
        this.products = products;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            default:
            case PRODUCT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_product_order, parent, false);

                return new ViewHolder(itemView);
            case OTHER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.element_products_prices , parent, false);

                return new ViewHolderPrice(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case PRODUCT:
                Product product = (Product) products.get(position);

                ViewHolder nHolder = ((ViewHolder)holder);

                String price = product.getPrice();
                String unitPrice = product.getPrice();

                try {
                    float fPrice = Float.valueOf(price) * product.getQuantity();

                    price = NumberFormat.getNumberInstance(Locale.US).format(fPrice);
                    unitPrice = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(unitPrice));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                nHolder.name.setText(product.getName());
                nHolder.description.setText(product.getDescription());
                nHolder.count.setText(Integer.toString(product.getQuantity()));
                nHolder.quantityX1.setText(context.getString(R.string.multiplier, product.getQuantity()));
                nHolder.price.setText(context.getString(R.string.price, price));
                nHolder.unitPrice.setText(context.getString(R.string.unit, unitPrice));

                ImageLoader.loadCircleImageFromURL(product.getPicture(), nHolder.image, R.drawable.ic_shopping_cart, context);
                break;
            case OTHER:
                SmallOrder order = (SmallOrder) products.get(position);
                ViewHolderPrice pHolder = ((ViewHolderPrice) holder);

                String subtotal = order.getSubtotal();
                String total = order.getTotal();
                String dPrice = order.getSeller().getMin_shipping_price();

                try {

                    float s;
                    float d;
                    float t;

                    if(subtotal == null) {
                        s = 0;
                    } else {
                        s = Float.valueOf(subtotal);
                    }

                    if(dPrice == null) {
                        d = 0;
                    } else {
                        d = Float.valueOf(total);
                    }

                    if(total == null) {
                        t = s + d;
                    } else {
                        t = Float.valueOf(total);
                    }

                    if(d == 0)
                        d = t - s;

                    if(s == 0)
                        s = t - d;

                    subtotal = NumberFormat.getNumberInstance(Locale.US).format(s);
                    total = NumberFormat.getNumberInstance(Locale.US).format(t);
                    dPrice = NumberFormat.getNumberInstance(Locale.US).format(d);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                pHolder.subtotal.setText(context.getString(R.string.price, subtotal));
                pHolder.send_cost.setText(context.getString(R.string.price, dPrice));
                pHolder.total.setText(context.getString(R.string.price, total));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (products.get(position) instanceof Product) {
            return PRODUCT;
        }
        return OTHER;
    }

    public void rechargeOrders(List<Object> products) {
        this.products.clear();
        this.products.addAll(products);

        notifyDataSetChanged();
    }
}
