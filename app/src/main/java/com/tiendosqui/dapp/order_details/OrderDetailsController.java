package com.tiendosqui.dapp.order_details;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.entities.User;
import com.tiendosqui.dapp.entities.json.OProductJson;
import com.tiendosqui.dapp.entities.json.ProductJson;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by divait on 20/04/2017.
 */

public class OrderDetailsController {

    private Context oc_context;
    private HttpCalls oc_httpCalls;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onLoadOrdersFail (String msg);

        void onOrderDetails(SmallOrder order);

        void onRequestSupportFail (String msg);

        void onRequestSupportComplete (String msg);
    }

    public OrderDetailsController(Context context, HttpCalls httpCalls) {
        oc_context = context;

        if (httpCalls != null) {
            oc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void getOrderDetails(SmallOrder order, OrderDetailsController.Callback callback) {
        User user = DataStorage.getUser(oc_context);

        if(user == null) {
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(oc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptLoadOrderDetails(order, callback);
    }

    private void attemptLoadOrderDetails(final SmallOrder order, final OrderDetailsController.Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(oc_context));

        Log.d("Data dev", "Data url details: " + NetworkInfo.getURL(NetworkInfo.ORDER_DETAILS_URL_START) +  order.getId() + NetworkInfo.ORDER_DETAILS_URL_END);
        oc_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.ORDER_DETAILS_URL_START) +  order.getId() + NetworkInfo.ORDER_DETAILS_URL_END, NetworkInfo.ORDER_DETAILS_TAG, null, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data order details: " + response);
                onSuccessResponse(response, order, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, SmallOrder order, Callback callback) {
        JSONArray json;
        try {
            json = new JSONArray(response);
            Log.d("Data dev", "Json orders: " + json);
        } catch (JSONException e) {
            callback.onServerError(oc_context.getString(R.string.error_loading_order));
            e.printStackTrace();
            return;
        }

        try {
            Gson gson = new Gson();
            OProductJson[] productsJson = gson.fromJson(json.toString(), OProductJson[].class);

            List<Product> products = new ArrayList<>();

            for(OProductJson productJson: productsJson) {
                Product product = ProductJson.toProduct(productJson.getProduct());
                product.setQuantity(productJson.getQuanty());

                products.add(product);
            }

            order.setProducts(products);

            callback.onOrderDetails(order);
        } catch (Exception ex) {
            callback.onLoadOrdersFail(oc_context.getString(R.string.error_loading_order));
            ex.printStackTrace();
        }
    }

    public void requestSupport(long orderId, Callback callback) {

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(oc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        attemptRequestSupport(orderId, callback);
    }

    private void attemptRequestSupport(long orderId, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(oc_context));

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.SUPPORT_TICKET__POST_PARAMETER_ID, Long.toString(orderId));
        params.put(NetworkInfo.SUPPORT_TICKET__POST_PARAMETER_MSG, oc_context.getString(R.string.call_in));

        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.SUPPORT_TICKET__POST_PARAMETER_ID, orderId);
            body.put(NetworkInfo.SUPPORT_TICKET__POST_PARAMETER_MSG, oc_context.getString(R.string.call_in));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        oc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SUPPORT_TICKET_URL), NetworkInfo.ORDER_DETAILS_TAG, params, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data support: " +response);
                onSuccessSupportResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessSupportResponse(String response, Callback callback) {
        JSONObject json;
        try {
            json = new JSONObject(response);
            Log.d("Data dev", "Support response Json: " +json);

            if(json.has("petition")){
                if(json.getString("petition").trim().equals("OK")) {
                    if(json.has("detail"))
                        callback.onRequestSupportComplete(json.getString("detail"));
                    else
                        callback.onRequestSupportComplete(oc_context.getString(R.string.support_send_to_service));
                    return;
                }
            }
        } catch (JSONException e) {
            callback.onServerError(oc_context.getString(R.string.server_error));
            e.printStackTrace();
            return;
        }

        callback.onRequestSupportFail(oc_context.getString(R.string.support_send_error));
    }
}
