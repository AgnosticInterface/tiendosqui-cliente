package com.tiendosqui.dapp.order_details;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.SmallOrder;

/**
 * Created by divai on 20/04/2017.
 */

public class OrderDetailsPresenter implements OrderDetailsContract.Presenter, OrderDetailsController.Callback {

    private OrderDetailsContract.View op_view;
    private OrderDetailsController op_controller;

    public OrderDetailsPresenter (@NonNull OrderDetailsContract.View ordersView,
                                  @NonNull OrderDetailsController ordersController) {

        op_view = ordersView;
        op_controller = ordersController;
    }

    @Override
    public void start() {
    }

    @Override
    public void getData(SmallOrder order) {
        op_controller.getOrderDetails(order, this);
    }

    @Override
    public void requestAssistance(long orderId) {
        op_controller.requestSupport(orderId, this);
    }

    @Override
    public void onNetworkConnectFailed() {
        op_view.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        op_view.showServerError(msg);
    }

    @Override
    public void onLoadOrdersFail(String msg) {
        op_view.showServerError(msg);
    }

    @Override
    public void onOrderDetails(SmallOrder order) {
        op_view.showOrderServer(order);
    }

    @Override
    public void onRequestSupportFail(String msg) {
        op_view.showErrorRequestingAssistance(msg);
    }

    @Override
    public void onRequestSupportComplete(String msg) {
        op_view.showRequestAssistance(msg);
    }
}
