package com.tiendosqui.dapp.order_details;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.ImageLoader;
import com.tiendosqui.dapp.utils.ActivityUtils;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by divait on 20/04/2017.
 */

public class OrderDetailsActivity extends BaseActivity implements OrderDetailsContract.View, OrderDetailsAdapter.OnItemClickListener {
    static final int REQUEST_ACCESS_CALL = 0x0001;

    private HttpCalls httpCalls;

    private TextView titleView;
    private TextView detailsView;
    private TextView timeView;
    private TextView timerView;
    private TextView stateView;
    private ImageView imageView;

    private RecyclerView productsList;
    private OrderDetailsAdapter productAdapter;
    LinearLayoutManager layoutManager;

    private Button btnRequestAssitance;

    private OrderDetailsContract.Presenter presenter;

    private SmallOrder order;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        setToolbar(true);

        productsList = findViewById(R.id.list_products);
        btnRequestAssitance = findViewById(R.id.assistance_button);

        titleView = findViewById(R.id.order_title);
        detailsView = findViewById(R.id.order_details);
        timeView = findViewById(R.id.order_time);
        timerView = findViewById(R.id.order_timer);
        stateView = findViewById(R.id.order_state);
        imageView = findViewById(R.id.user_img);

        btnRequestAssitance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilDialog.showDialogSupport(OrderDetailsActivity.this, order.getId(), supportCallback());
            }
        });

        productAdapter = OrderDetailsAdapter.newInstance(this, this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        productsList.setLayoutManager(layoutManager);
        productsList.setAdapter(productAdapter);

        // Create the http handler
        httpCalls = new HttpCalls(this);

        OrderDetailsController orderDetailsController = new OrderDetailsController(this, httpCalls);
        OrderDetailsPresenter orderDetailsPresenter = new OrderDetailsPresenter(this, orderDetailsController);
        setPresenter(orderDetailsPresenter);

        Gson gson = new Gson();
        order = gson.fromJson(getIntent().getStringExtra("order"), SmallOrder.class);
        showOrder(order);

        ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(getString(R.string.bar_title_order, order.getId()));

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    public void setPresenter(OrderDetailsContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showOrderServer(SmallOrder order) {
        List<Object> products = new ArrayList<>();
        products.addAll(order.getProducts());
        products.add(order);

        productAdapter.rechargeOrders(products);
    }

    @Override
    public void showRequestAssistance(String msg) {
        UtilDialog.showDialog(this,
                getString(R.string.support_dialog_title_2, order.getId()),
                getString(R.string.support_send_to_service),
                R.string.ok,
                null ,
                -1,
                null
        );
    }

    @Override
    public void showErrorRequestingAssistance(String msg) {
        UtilDialog.showDialog(this,
                getString(R.string.support_dialog_title_2, order.getId()),
                msg,
                R.string.ok,
                null ,
                -1,
                null
        );
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    public void showOrder(SmallOrder order) {
        String total = order.getTotal();

        try {
            total = NumberFormat.getNumberInstance(Locale.US).format(Float.valueOf(total));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        titleView.setText(order.getSeller().getName());
        detailsView.setText(getString(R.string.order_desc, order.getTotalProducts(), total));

        timeView.setText("");
        stateView.setText(changeState(order.getStatus()));

        if(order.getStatus() == 3) {
            findViewById(R.id.green_dot).setBackgroundResource(R.drawable.circle_red);
            ((ImageView)findViewById(R.id.image_dot)).setImageDrawable(getResources().getDrawable(R.drawable.ic_close_cross_white));
            timeView.setText(ActivityUtils.formatTimeToList(order.getDateReject(), this));
        } else {
            findViewById(R.id.green_dot).setBackgroundResource(R.drawable.circle_green);
            ((ImageView)findViewById(R.id.image_dot)).setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));

            if(order.getStatus() == 1) {
                timeView.setText(ActivityUtils.formatTimeToList(order.getDateSend(), this));
            } else if(order.getStatus() == 2) {
                new CountDownTimer(order.getTime()*60*1000, 1000) { // TODO

                    public void onTick(long millisUntilFinished) {
                        int seconds = (int) (millisUntilFinished / 1000);

                        timerView.setText(seconds/60 + ":" + seconds%60);
                    }

                    public void onFinish() {
                        timerView.setText("0:00");
                    }
                }.start();

                timeView.setText(ActivityUtils.formatTimeToList(order.getDateConfirm(), this));
            } else if(order.getStatus() == 4) {
                timeView.setText(ActivityUtils.formatTimeToList(order.getDateEnd(), this));
            }
        }

        ImageLoader.loadCircleImageFromURL(order.getSeller().getPicture(), imageView, R.drawable.will, this);

        presenter.getData(order);
    }

    @Override
    public void onItemClick(Product product) {
        // Not doing anything
    }

    private String changeState(int status) {
        switch (status) {
            default:
            case 1:
                return "Pedido recibido";
            case 2:
                return "Pedido confirmado";
            case 3:
                return "Pedido rechazado";
            case 4:
                return "Pedido entregado";
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 3);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }

    private UtilDialog.SupportCallback supportCallback() {
        return new UtilDialog.SupportCallback() {
            @Override
            public void callSupport() {
                makeCall();
            }

            @Override
            public void requestCallFromSupport() {
                UtilDialog.showDialog(OrderDetailsActivity.this, getString(R.string.support_dialog_title_2, order.getId()), getString(R.string.support_dialog_desc), R.string.support_dialog_action,requestSupport() , -1, null);
            }
        };
    }

    public void makeCall() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                Toast.makeText(this, "Permiso requerido", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_ACCESS_CALL);

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_ACCESS_CALL);
            }
            return;
        }

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + DataStorage.getSupportNumber(this)));
        startActivity(intent);
    }

    public DialogInterface.OnClickListener requestSupport() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.requestAssistance(order.getId());
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();
                } else {
                    Toast.makeText(this, "Permiso requerido", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
