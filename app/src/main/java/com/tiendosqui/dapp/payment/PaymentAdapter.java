package com.tiendosqui.dapp.payment;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.MethodPay;
import com.tiendosqui.dapp.entities.MethodPayElement;
import com.tiendosqui.dapp.entities.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divai on 26/05/2017.
 */

public class PaymentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements PaymentViewHolder.OnPaySelectedListener {
    private List<Order> orders;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onOpenPaySelected(long sellerId, String payment);
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        public TextView name;

        public View item;
        public RadioGroup payments;
        public List<RadioButton> buttons;

        public OrderViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.shop_name);

            payments = (RadioGroup) view.findViewById(R.id.payment_list);
            item = view;

            buttons = new ArrayList<>();
        }

        public void unselectedAll(int position) {

            for(int i=0; i<buttons.size();i++) {
                if(position != i) {
                    buttons.get(i).setChecked(false);
                }
            }
        }

        public void addButton(RadioButton btn) {
            buttons.add(btn);
        }
    }

    public static PaymentAdapter newInstance(MainOrder orders, Context context, OnItemClickListener listener) {
        return new PaymentAdapter(orders.getOrders(), context, listener);
    }

    public PaymentAdapter(List<Order> orders, Context context, OnItemClickListener listener) {
        this.orders = orders;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_pay_activity, parent, false);

        return new PaymentAdapter.OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final PaymentAdapter.OrderViewHolder sh = (PaymentAdapter.OrderViewHolder) holder;
        sh.payments.removeAllViews();

        Log.d("Data dev", "Report: " + orders.get(position).getSellerMethodPayments());

        int i = 0;
        for (MethodPayElement payElement : orders.get(position).getSellerMethodPayments()) {
            boolean isChecked = payElement.getMethod_pay().getName().equals(orders.get(0).getPayMethod());

            createPayment(payElement.getMethod_pay(), isChecked, orders.get(position).getSellerId(), getRadioId(i), i, sh.payments, sh);
            i++;
        }

        sh.name.setText(orders.get(position).getSellerName());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    @Override
    public void onItemSelected(String payment, long sellerId) {
        listener.onOpenPaySelected(sellerId, payment);
    }

    private View createPayment(MethodPay payment, boolean isChecked, long sellerId, int radioId, int position, RadioGroup group, OrderViewHolder holder) {
        View child = ((Activity)context).getLayoutInflater().inflate(R.layout.element_pay, group);
        PaymentViewHolder productVH = new PaymentViewHolder(child, holder, this);

        holder.addButton(productVH.configureData(payment.getName(), isChecked, sellerId, radioId, position));


        return child;
    }

    private int getRadioId(int pos) {
        switch (pos) {
            case 0:
                return R.id.radio0;
            case 1:
                return R.id.radio1;
            case 2:
                return R.id.radio2;
            case 3:
                return R.id.radio3;
            case 4:
                return R.id.radio4;
            case 5:
                return R.id.radio5;
            case 6:
                return R.id.radio6;
            case 7:
                return R.id.radio7;
            case 8:
                return R.id.radio8;
            case 9:
                return R.id.radio9;
            default:
                return R.id.cash_radio_btn;
        }
    }
}
