package com.tiendosqui.dapp.payment;

import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Created by divaiy on 12/04/2017.
 */

public class PayPresenter implements PayContract.Presenter, PayController.Callback{

    private final PayContract.View pp_payView;
    private PayController pp_payController;
    private Activity activity;

    public PayPresenter(@NonNull PayContract.View payView,
                          @NonNull PayController payController, Activity activity) {

        pp_payView = payView;
        payView.setPresenter(this);
        pp_payController = payController;

        this.activity = activity;
    }

    @Override
    public void start() {

    }

    @Override
    public void setPayment(String data, long sellerId) {
        pp_payController.setPayment(data, sellerId, this);
    }

    @Override
    public void onNoOrder() {
        pp_payView.showNoOrderError();
    }

    @Override
    public void onSetPayment(String msg) {
        pp_payView.selectPayment(msg);
    }
}
