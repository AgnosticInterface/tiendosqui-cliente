package com.tiendosqui.dapp.payment;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by Medycitas on 12/04/2017.
 */

public interface PayContract {

    interface View extends BaseView<Presenter> {

        void selectPayment (String msg);

        void showNoOrderError ();
    }

    interface Presenter extends BasePresenter {
        void setPayment(String data, long sellerId);
    }
}
