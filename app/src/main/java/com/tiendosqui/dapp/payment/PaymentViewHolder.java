package com.tiendosqui.dapp.payment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.tiendosqui.dapp.R;

/**
 * Created by divait on 26/05/2017.
 */

public class PaymentViewHolder extends RecyclerView.ViewHolder {

    private RadioButton radioButton;
    private TextView paymentText;

    private long sellerId;
    private int position;

    private OnPaySelectedListener callback;
    private PaymentAdapter.OrderViewHolder parent;

    // Interface to get the event of item click in the list
    public interface OnPaySelectedListener {
        void onItemSelected(String payment, long sellerId);
    }

    public PaymentViewHolder(View view, final PaymentAdapter.OrderViewHolder parent, final OnPaySelectedListener callback) {
        super(view);
        this.callback = callback;
        this.parent = parent;

        radioButton = (RadioButton) view.findViewById(R.id.cash_radio_btn);
        paymentText = (TextView) view.findViewById(R.id.payment_text);

    }
    public RadioButton configureData(String data, boolean isChecked, final long sellerId, int radioId, final int position){
        this.sellerId = sellerId;
        this.position = position;

        radioButton.setChecked(isChecked);
        radioButton.setId(radioId);
        paymentText.setText(data);

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    parent.unselectedAll(position);
                    callback.onItemSelected(paymentText.getText().toString(), sellerId);
                }
            }
        });

        return radioButton;
    }
}
