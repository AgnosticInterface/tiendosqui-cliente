package com.tiendosqui.dapp.payment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 12/04/2017.
 */

public class PayActivity extends BaseActivity implements PayContract.View, PaymentAdapter.OnItemClickListener{

    private PayContract.Presenter presenter;
    private PaymentAdapter adapter;

    private RecyclerView paymetnList;
    private Button btnSelect;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment);
        setToolbar(true);

        MainOrder orders = DataStorage.getOrders(this);
        adapter = PaymentAdapter.newInstance(orders, this, this);

        paymetnList = (RecyclerView) findViewById(R.id.list_payments);

        paymetnList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        paymetnList.setAdapter(adapter);

        btnSelect = (Button) findViewById(R.id.select_payment_button);

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        PayController payController = new PayController(getApplicationContext());
        presenter = new PayPresenter(this, payController, this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void setPresenter(PayContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void selectPayment(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNoOrderError() {
        Toast.makeText(this, getString(R.string.no_order), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOpenPaySelected(long sellerId, String payment) {
        presenter.setPayment(payment, sellerId);
    }
}
