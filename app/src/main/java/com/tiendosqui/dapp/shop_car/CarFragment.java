package com.tiendosqui.dapp.shop_car;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.checkout.CheckoutActivity;
import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.OrderComment;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.login.LoginActivity;
import com.tiendosqui.dapp.shop.ProductViewHolder;
import com.tiendosqui.dapp.shop.ShopAdapter;
import com.tiendosqui.dapp.tabs.TabChildFragment;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;

import java.util.ArrayList;
import java.util.List;
import java.text.NumberFormat;
import java.util.Locale;

import static com.tiendosqui.dapp.utils.DataStorage.getOrder;

/**
 * Created by Divait on 25/01/2017.
 *
 * The fragment to performa a search of products.
 */

public class CarFragment extends TabChildFragment implements CarContract.View, ShopAdapter.onItemClicked {
    static final int CHECKOUT_REQUEST = 1;

    private CarContract.Presenter presenter;

    private RecyclerView productsList;
    private View emptyView;
    private ShopAdapter productsAdapter;

    public TextView txtTitle;
    public TextView txtPrice;
    public Button btnNext;

    public CarFragment() {
        // Require empty constructor
    }

    public static CarFragment getInstance() {
        return new CarFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_car, container, false);

        txtTitle = (TextView) root.findViewById(R.id.products_total);
        txtPrice = (TextView) root.findViewById(R.id.products_price);
        btnNext = (Button) root.findViewById(R.id.btn_checkout);

        emptyView = root.findViewById(R.id.willy_view);
        ((TextView)emptyView.findViewById(R.id.willy_message)).setText(getString(R.string.empty_car));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goNext();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        productsAdapter = ShopAdapter.newInstance(getActivity(), this);

        productsList = (RecyclerView) root.findViewById(R.id.list_products);

        productsList.setLayoutManager(layoutManager);
        productsList.setAdapter(productsAdapter);

        return  root;
    }

    @Override
    public void onStart() {
        super.onStart();
        showProducts();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHECKOUT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                int result = data.getIntExtra("id", 0);
            }
        }
    }

    @Override
    public void onAdd(Product product, ProductViewHolder holder) {
        presenter.addItem(product);
        showOrderData();
        holder.addQuantity();
    }

    @Override
    public void onRemove(Product product) {
        presenter.removeItem(product);
        showOrderData();
    }

    @Override
    public void onComment(CheckBox checkBox, long sellerId) {
        UtilDialog.showDialogInput(
            getActivity(),
            getString(R.string.title_comment, getOrder(getActivity(), sellerId).getSellerName()),
            getString(R.string.description_comment),
            R.string.ok_comment,
            commentOnPositiveClick(checkBox, sellerId),
            R.string.cancel,
            null
        );
    }

    @Override
    public void showProducts(MainOrder orders) {
        List<Object> objects = new ArrayList<>();

        if(orders == null) {
            productsList.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);

            productsAdapter.rechargeObjects(objects);
            return;
        } else {
            productsList.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

        for (Order order : orders.getOrders()) {
            objects.add(order);
            objects.addAll(order.getProducts());
            objects.add(new OrderComment(order.getSellerId(), order.getComment()));
        }

        productsAdapter.rechargeObjects(objects);
    }

    @Override
    public void showLoadProductsError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError(boolean show) {
        Toast.makeText(getActivity(), getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPresenter(CarContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showProducts() {
        MainOrder orders = DataStorage.getOrders(getActivity());

        showOrderData();
        showProducts(orders);
    }

    private void showOrderData() {
        MainOrder mainOrder = DataStorage.getOrders(getContext());

        if(mainOrder == null || mainOrder.getOrders().size() == 0) {
            txtTitle.setText("");
            txtPrice.setText("");

            btnNext.setEnabled(false);
        } else {
            int count = 0;
            float totalPrice = 0;

            for (Order order : mainOrder.getOrders()) {
                count += order.getProductsCount();
                totalPrice += order.getTotalPrice();
            }

            String price = NumberFormat.getNumberInstance(Locale.US).format(totalPrice);

            txtTitle.setText(getString(R.string.subtotal_order, count));
            txtPrice.setText(getString(R.string.price, price));

            btnNext.setEnabled(true);
        }
    }

    private void goNext() {
        MainOrder mainOrder = DataStorage.getOrders(getActivity());

        for(Order order : mainOrder.getOrders()){
            if(order.getSubTotalPrice() < order.getSellerMinDelivery()){
                UtilDialog.showDialog(
                        getContext(),
                        order.getSellerName(),
                        getString(R.string.dialog_min_msg, NumberFormat.getNumberInstance(Locale.US).format(order.getSellerMinDelivery())),
                        R.string.ok,
                        null,
                        -1,
                        null
                        );
                return;
            }
        }

        Intent intent;
        if(!DataStorage.isLogged(getActivity())) {
            intent = new Intent(getActivity(), LoginActivity.class);
            intent.putExtra("back", false);
        } else {
            intent = new Intent(getActivity(), CheckoutActivity.class);
        }

        startActivityForResult(intent, CHECKOUT_REQUEST);
    }

    public DialogInterface.OnClickListener commentOnPositiveClick(final CheckBox checkBox, final long sellerId) {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText input = (EditText) ((AlertDialog) dialog).findViewById(R.id.text_input);
                String comment = "";

                try {
                    if (input == null) return;
                    comment = input.getText().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Order order = DataStorage.getOrder(getActivity(), sellerId);
                    order.setComment(comment);

                    DataStorage.setOrder(getActivity(), order);

                    checkBox.setText("\"" + comment + "\"");
                    checkBox.setChecked(true);
                }
            }
        };
    }
}
