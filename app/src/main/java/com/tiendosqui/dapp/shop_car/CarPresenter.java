package com.tiendosqui.dapp.shop_car;

import android.support.annotation.NonNull;

import com.tiendosqui.dapp.entities.Product;

/**
 * Created by divai on 24/03/2017.
 */

public class CarPresenter implements CarContract.Presenter, CarController.Callback {

    private CarContract.View sp_view;
    private CarController sp_controller;

    public CarPresenter(@NonNull CarContract.View mainView,
                        @NonNull CarController mainController) {

        sp_view = mainView;
        sp_controller = mainController;
    }

    @Override
    public void addItem(Product product) {
        sp_controller.addItem(product);
    }

    @Override
    public void removeItem(Product product) {
        sp_controller.removeItem(product);
    }

    @Override
    public void start() {
        // TODO
    }
/*
    @Override
    public void onNetworkConnectFailed() {
        sp_view.showNetworkError(true);
    }

    @Override
    public void onServerError(String msg) {
        sp_view.showServerError(msg);
    }

    @Override
    public void onLoadProductsFail(String msg) {
        sp_view.showLoadProductsError(msg);
    }
*/
    @Override
    public void refreshProducts() {
        sp_view.showProducts();
    }
}
