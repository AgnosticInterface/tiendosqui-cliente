package com.tiendosqui.dapp.shop_car;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.Order;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by divai on 16/05/2017.
 */

public class ShopInfoViewHolder extends RecyclerView.ViewHolder {
    private Context context;

    public TextView txtShopName;
    public TextView txtTimeDelivery;
    public TextView txtDeliveryCost;

    public ShopInfoViewHolder(View itemView, Context context) {
        super(itemView);

        this.context = context;

        txtShopName = (TextView) itemView.findViewById(R.id.shop_name_car);
        txtDeliveryCost = (TextView) itemView.findViewById(R.id.delivery_cost_car);
        txtTimeDelivery = (TextView) itemView.findViewById(R.id.time_delivery_car);
    }

    public void configureView(Order order) {
        if(order == null) {
            txtShopName.setVisibility(View.GONE);
            txtTimeDelivery.setVisibility(View.GONE);
            txtDeliveryCost.setVisibility(View.GONE);
        } else {
            txtShopName.setVisibility(View.VISIBLE);
            txtTimeDelivery.setVisibility(View.VISIBLE);
            txtDeliveryCost.setVisibility(View.VISIBLE);

            String dPrice = order.getSellerDeliveryCost();

            try {
                dPrice = NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(dPrice));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            txtShopName.setText(order.getSellerName());
            txtTimeDelivery.setText(context.getString(R.string.time_delivery_car, order.getSellerDeliveryTime()));
            txtDeliveryCost.setText(context.getString(R.string.delivery_cost_car, dPrice));
        }
    }
}
