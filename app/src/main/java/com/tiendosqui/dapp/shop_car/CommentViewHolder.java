package com.tiendosqui.dapp.shop_car;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.entities.OrderComment;

/**
 * Created by Dviait on 11/04/2017.
 *
 *
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {
    private CheckBox checkBox;
    private onCommentClicked listener;

    public View view;

    public interface onCommentClicked {
        void onClick(CheckBox checkBox, long sellerId);
    }

    public CommentViewHolder(View view, onCommentClicked listener) {
        super(view);

        this.view = view;
        this.listener = listener;
        checkBox = (CheckBox) view.findViewById(R.id.title_comment);
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void setData(final OrderComment comment) {
        if(comment.getComment().length() > 0) {
            getCheckBox().setText(comment.getComment());
            getCheckBox().setChecked(true);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(checkBox, comment.getSellerId());
            }
        });
    }
}
