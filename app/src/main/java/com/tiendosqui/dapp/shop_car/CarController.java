package com.tiendosqui.dapp.shop_car;

import android.content.Context;

import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

/**
 * Created by divait on 24/03/2017.
 */

public class CarController {

    private Context sc_context;
    private Callback sp_callback;

    private BaseTabActivity activity;

    public interface Callback {

        void refreshProducts();
    }

    public CarController(Context context, BaseTabActivity activity) {
        sc_context = context;
        this.activity = activity;
    }

    public void setListener(Callback callback){
        sp_callback = callback;
    }

    public void addItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null) {
            sp_callback.refreshProducts();
            return;
        }

        order.addProduct(product, activity);
    }

    public void removeItem(Product product) {
        Order order = DataStorage.getOrder(sc_context, product.getShopId());

        if(order == null) {
            sp_callback.refreshProducts();
            return;
        }

        int q =order.removeProduct(product.getId(), activity);
        if(q <= 0)
            sp_callback.refreshProducts();
    }
}
