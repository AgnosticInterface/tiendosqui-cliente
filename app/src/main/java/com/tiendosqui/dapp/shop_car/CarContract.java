package com.tiendosqui.dapp.shop_car;

import com.tiendosqui.dapp.entities.MainOrder;
import com.tiendosqui.dapp.entities.Product;
import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by divai on 24/03/2017.
 */

public interface CarContract {
    interface View extends BaseView<CarContract.Presenter> {

        void showProducts(MainOrder mainOrder);

        void showProducts();

        void showLoadProductsError(String msg);

        void showServerError(String msg);

        void showNetworkError(boolean show);

    }

    interface Presenter extends BasePresenter {

        void addItem(Product product);

        void removeItem(Product product);
    }
}
