package com.tiendosqui.dapp.recover_pass;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 28/04/2017.
 */

public class RecoverDoneActivity extends BaseActivity {

    private Button btnRecover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_pass_done);

        btnRecover = (Button) findViewById(R.id.recover_button);

        // Events
        btnRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setToolbar(true);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
