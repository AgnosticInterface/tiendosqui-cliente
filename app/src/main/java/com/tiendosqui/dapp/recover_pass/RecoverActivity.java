package com.tiendosqui.dapp.recover_pass;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 28/04/2017.
 */

public class RecoverActivity extends BaseActivity implements RecoverContract.View {

    private EditText editEmail;
    private TextInputLayout errorEmail;

    private Button btnRecover;

    private View progressView;
    private View registerFormView;

    private HttpCalls ra_httpCalls;
    private RecoverContract.Presenter ra_presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_pass);

        // Create the http handler
        ra_httpCalls = new HttpCalls(this);

        RecoverController recoverController = new RecoverController(getApplicationContext(), ra_httpCalls);
        ra_presenter = new RecoverPresenter(this, recoverController);

        editEmail= (EditText) findViewById(R.id.edit_register_email);

        errorEmail = (TextInputLayout) findViewById(R.id.register_error_email);

        btnRecover = (Button) findViewById(R.id.recover_button);

        registerFormView = findViewById(R.id.user_register_form);
        progressView = findViewById(R.id.register_progress);

        // Events
        btnRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRecoverPass();
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorEmail.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setToolbar(true);
    }

    @Override
    public void setPresenter(RecoverContract.Presenter presenter) {
        if (presenter != null) {
            ra_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showProgress(boolean show) {
        registerFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEmailError(String error) {
        errorEmail.setError(error);
        editEmail.requestFocus();
    }

    @Override
    public void showRecoverError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoginActivity() {
        finish();
        startActivity(new Intent(this, RecoverDoneActivity.class));
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void attemptRecoverPass() {
        ra_presenter.attemptRecover(
                editEmail.getText().toString()
        );
    }
}
