package com.tiendosqui.dapp.recover_pass;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by divait on 28/04/2017.
 */

public interface RecoverContract {
    interface View extends BaseView<Presenter> {
        void showProgress(boolean show);

        void setEmailError(String error);

        void showRecoverError(String msg);

        void showLoginActivity();

        void showServerError(String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void attemptRecover(String email);
    }
}
