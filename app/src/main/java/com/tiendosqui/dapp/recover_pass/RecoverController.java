package com.tiendosqui.dapp.recover_pass;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.Commands;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by divat on 28/04/2017.
 */

public class RecoverController {

    private final Context ri_context;
    private HttpCalls ri_httpCalls;

    public interface Callback {

        void onEmailError(String msg);

        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onAuthFailed(String msg);

        void onAuthSuccess();
    }

    public RecoverController(Context context, HttpCalls httpCalls) {
        ri_context = context;
        if (httpCalls != null) {
            ri_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void recoverPass(String email, final Callback callback) {
        if(Commands.isCommand(email)) {
            Commands.executeCommand(email, ri_context);
            callback.onEmailError("Command");
            return;
        }

        // Check logic
        boolean validEmail = isValidEmail(email, callback);
        if (!validEmail ) {
            return;
        }

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(ri_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        // Make register call
        attendRecoverPass(email, callback);

    }
    /**
     * Check if the username is useful.
     *
     * @param email the string to check.
     * @param callback receive error text response.
     * @return True if is valid False otherwise.
     */
    private boolean isValidEmail(String email, Callback callback) {
        boolean isValid = true;
        if (TextUtils.isEmpty(email)) {
            callback.onEmailError(ri_context.getString(R.string.error_empty_email));
            isValid = false;
        }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback.onEmailError(ri_context.getString(R.string.error_reg_invalid_email));
            isValid = false;
        }
        return isValid;
    }

    private void attendRecoverPass(String email, final Callback callback) {
        JSONObject body = new JSONObject();
        try {
            body.put(NetworkInfo.RECOVER_PASS_POST_PARAMETER_EMAIL, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ri_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.RECOVER_PASS_URL), NetworkInfo.RECOVER_PASS_TAG, new HashMap<String, String>(), body.toString(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data1: " + response);
                JSONObject json;
                try {
                    json = new JSONObject(response);

                    if(json.has("petition")) {
                        if(json.getString("petition").trim().equals("OK"))
                            callback.onAuthSuccess();
                        else
                            callback.onAuthFailed(json.getString("detail"));
                    } else
                        callback.onAuthFailed(ri_context.getString(R.string.error_recover_pass));
                } catch (Exception e) {
                    e.printStackTrace();

                    callback.onServerError(ri_context.getString(R.string.error_recover_pass));
                }
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

}
