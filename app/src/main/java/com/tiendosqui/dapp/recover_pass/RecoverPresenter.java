package com.tiendosqui.dapp.recover_pass;

import android.support.annotation.NonNull;

/**
 * Created by divait on 28/04/2017.
 */

public class RecoverPresenter implements RecoverContract.Presenter, RecoverController.Callback {

    private final RecoverContract.View recoverView;
    private RecoverController recoverController;

    public RecoverPresenter(@NonNull RecoverContract.View recoverView,
                             @NonNull RecoverController recoverController) {

        this.recoverView = recoverView;
        recoverView.setPresenter(this);
        this.recoverController = recoverController;
    }

    @Override
    public void start() {

    }

    @Override
    public void attemptRecover(String email) {
        recoverView.showProgress(true);
        recoverController.recoverPass(email, this);
    }

    @Override
    public void onEmailError(String msg) {
        recoverView.showProgress(false);
        recoverView.setEmailError(msg);
    }

    @Override
    public void onNetworkConnectFailed() {
        recoverView.showProgress(false);
        recoverView.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        recoverView.showProgress(false);
        recoverView.showServerError(msg);
    }

    @Override
    public void onAuthFailed(String msg) {
        recoverView.showProgress(false);
        recoverView.showRecoverError(msg);
    }

    @Override
    public void onAuthSuccess() {
        recoverView.showProgress(false);
        recoverView.showLoginActivity();
    }
}
