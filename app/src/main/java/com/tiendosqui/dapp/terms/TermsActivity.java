package com.tiendosqui.dapp.terms;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 25/04/2017.
 */

public class TermsActivity extends BaseActivity implements TermsContract.View {

    private TermsContract.Presenter la_presenter;
    private WebView webView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        webView = (WebView) findViewById(R.id.webview);

        setToolbar(true);

        // Create the http handler
        HttpCalls httpCalls = new HttpCalls(this);

        // Connect the view with the interactor
        TermsController controller = new TermsController(this, httpCalls);
        la_presenter = new TermsPresenter(this, controller, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        la_presenter.start();
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 4);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }

    @Override
    public void setPresenter(TermsContract.Presenter presenter) {
        if (presenter != null) {
            la_presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showProgress(boolean show) {

    }

    @Override
    public void showLoadError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoad(String url) {
        webView.loadUrl(NetworkInfo.getImageURL() + url);
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }
}
