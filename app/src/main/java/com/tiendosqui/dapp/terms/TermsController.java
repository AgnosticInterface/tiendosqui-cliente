package com.tiendosqui.dapp.terms;

import android.content.Context;
import android.util.Log;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Medycitas on 28/04/2017.
 */

public class TermsController {

    private final Context li_context;
    private HttpCalls li_httpCalls;

    public interface Callback {
        void onNetworkConnectFailed();

        void onServerError(String msg);

        void onLoadError(String msg);

        void onLoadSuccess(String url);
    }

    public TermsController(Context context, HttpCalls httpCalls) {
        li_context = context;
        if (httpCalls != null) {
            li_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void loadPdf(final Callback callback) {
        // Check Network
        if (!NetworkInfo.isNetworkAvailable(li_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        // Make login call
        signInUser(callback);

    }

    private void signInUser(final Callback callback) {
        li_httpCalls.standardGetCall(NetworkInfo.getURL(NetworkInfo.TERMS_URL), NetworkInfo.TERMS_TAG, null, new HashMap<String, String>(), new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "data1: " + response);
                JSONArray jsonArray;
                JSONObject json;
                try {
                    jsonArray = new JSONArray(response);
                    if(jsonArray.length() > 0) {
                        json = jsonArray.getJSONObject(0);
                        onSuccessLoad(json, callback);
                    } else {
                        callback.onLoadError(li_context.getString(R.string.error_terms_cond));
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    callback.onLoadError(li_context.getString(R.string.error_terms_cond));
                }
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(li_context.getString(R.string.server_error));
            }
        });
    }


    private void onSuccessLoad(JSONObject data, final Callback callback) throws JSONException {
        Log.d("Data dev", "File> " + data.getString("file"));

        callback.onLoadSuccess(data.getString("file"));
    }
}
