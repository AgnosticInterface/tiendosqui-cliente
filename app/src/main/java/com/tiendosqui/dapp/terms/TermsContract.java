package com.tiendosqui.dapp.terms;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by Medycitas on 28/04/2017.
 */

public class TermsContract {

    interface View extends BaseView<Presenter> {
        void showProgress(boolean show);

        void showLoadError(String msg);

        void showLoad(String url);

        void showServerError(String msg);

        void showNetworkError();
    }

    interface Presenter extends BasePresenter {
        void attemptLoadTerms();
    }
}
