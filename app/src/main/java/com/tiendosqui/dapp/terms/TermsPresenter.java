package com.tiendosqui.dapp.terms;

import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Created by Medycitas on 28/04/2017.
 */

public class TermsPresenter implements TermsContract.Presenter, TermsController.Callback {

    private final TermsContract.View lp_termsView;
    private TermsController lp_termsController;
    private Activity activity;

    public TermsPresenter(@NonNull TermsContract.View termsView,
                          @NonNull TermsController termsController, Activity activity) {

        lp_termsView = termsView;
        termsView.setPresenter(this);
        lp_termsController = termsController;

        this.activity = activity;
    }

    @Override
    public void start() {
        attemptLoadTerms();
    }

    @Override
    public void attemptLoadTerms() {
        lp_termsController.loadPdf(this);
    }

    // Callback
    @Override
    public void onNetworkConnectFailed() {
        lp_termsView.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        lp_termsView.showServerError(msg);
    }

    @Override
    public void onLoadError(String msg) {
        lp_termsView.showLoadError(msg);
    }

    @Override
    public void onLoadSuccess(String url) {
        lp_termsView.showLoad(url);
    }
}
