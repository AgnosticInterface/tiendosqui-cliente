package com.tiendosqui.dapp.support;

import com.tiendosqui.dapp.utils.base.BasePresenter;
import com.tiendosqui.dapp.utils.base.BaseView;

/**
 * Created by divai on 23/05/2017.
 */

public interface SupportContract {
    interface View extends BaseView<Presenter> {

        void showRequestSupportComplete ();

        void showRequestSupportError (String msg);

        void showServerError (String msg);

        void showNetworkError();

    }

    interface Presenter extends BasePresenter {
        void requestSupport();
    }
}
