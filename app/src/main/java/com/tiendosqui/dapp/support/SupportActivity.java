package com.tiendosqui.dapp.support;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.main.MainActivity;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.utils.DataStorage;
import com.tiendosqui.dapp.utils.UtilDialog;
import com.tiendosqui.dapp.utils.base.BaseActivity;

/**
 * Created by divait on 25/04/2017.
 */

public class SupportActivity extends BaseActivity implements SupportContract.View {
    static final int REQUEST_ACCESS_CALL = 0x0001;

    private SupportContract.Presenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        findViewById(R.id.card_view_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall();
            }
        });

        findViewById(R.id.card_view_receive_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilDialog.showDialog(SupportActivity.this, getString(R.string.support_dialog_title), getString(R.string.support_dialog_desc), R.string.support_dialog_action,requestSupport() , -1, null);
            }
        });

        setToolbar(true);

        // Create the http handler
        HttpCalls httpCalls = new HttpCalls(this);

        SupportController supportController = new SupportController(this, httpCalls);
        SupportPresenter supportPresenter = new SupportPresenter(this, supportController);
        setPresenter(supportPresenter);
    }

    public void makeCall() {
        if (ActivityCompat.checkSelfPermission(SupportActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(SupportActivity.this,
                    Manifest.permission.CALL_PHONE)) {

                Toast.makeText(SupportActivity.this, "Permiso requerido", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(SupportActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_ACCESS_CALL);

            } else {

                ActivityCompat.requestPermissions(SupportActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_ACCESS_CALL);
            }
            return;
        }

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + DataStorage.getSupportNumber(this)));
        startActivity(intent);
    }

    public DialogInterface.OnClickListener requestSupport() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.requestSupport();
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();
                } else {
                    Toast.makeText(SupportActivity.this, "Permiso requerido", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("tab", 4);

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
    }

    @Override
    public void setPresenter(SupportContract.Presenter presenter) {
        if (presenter != null) {
            this.presenter = presenter;
        } else {
            throw new RuntimeException("Presenter can't be null.");
        }
    }

    @Override
    public void showRequestSupportComplete() {
        UtilDialog.showDialog(SupportActivity.this,
                getString(R.string.support_dialog_title),
                getString(R.string.support_send_to_service),
                R.string.ok,
                null ,
                -1,
                null
        );
    }

    @Override
    public void showRequestSupportError(String msg) {
        UtilDialog.showDialog(SupportActivity.this,
                getString(R.string.support_dialog_title),
                msg,
                R.string.ok,
                null ,
                -1,
                null
        );
    }

    @Override
    public void showServerError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showNetworkError() {
        Toast.makeText(this, getString(R.string.disconnected_internet), Toast.LENGTH_LONG).show();
    }
}
