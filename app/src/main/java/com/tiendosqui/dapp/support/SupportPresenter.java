package com.tiendosqui.dapp.support;

import android.support.annotation.NonNull;

/**
 * Created by divai on 23/05/2017.
 */

public class SupportPresenter implements SupportContract.Presenter, SupportController.Callback {

    private SupportContract.View mp_view;
    private SupportController mp_controller;

    public SupportPresenter (@NonNull SupportContract.View supportView,
                          @NonNull SupportController supportController) {

        mp_view = supportView;
        mp_controller = supportController;
    }

    @Override
    public void start() {

    }

    @Override
    public void requestSupport() {
        mp_controller.requestSupport(this);
    }

    // Callback
    @Override
    public void onNetworkConnectFailed() {
        mp_view.showNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        mp_view.showServerError(msg);
    }

    @Override
    public void onRequestSupportFail(String msg) {
        mp_view.showRequestSupportError(msg);
    }

    @Override
    public void onRequestSupportComplete(String msg) {
        mp_view.showRequestSupportComplete();
    }

    @Override
    public void onNoUser(String msg) {
        onServerError(msg);
    }
}
