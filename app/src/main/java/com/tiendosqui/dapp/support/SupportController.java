package com.tiendosqui.dapp.support;

import android.content.Context;
import android.util.Log;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.network.HttpCalls;
import com.tiendosqui.dapp.network.NetworkInfo;
import com.tiendosqui.dapp.utils.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by divai on 23/05/2017.
 */

public class SupportController {

    private Context sc_context;
    private HttpCalls sc_httpCalls;

    public interface Callback {

        void onNetworkConnectFailed ();

        void onServerError (String msg);

        void onRequestSupportFail (String msg);

        void onRequestSupportComplete (String msg);

        void onNoUser(String msg);
    }

    public SupportController(Context context, HttpCalls httpCalls) {
        sc_context = context;

        if (httpCalls != null) {
            sc_httpCalls = httpCalls;
        } else {
            throw new RuntimeException("HttpCalls reference can't be null");
        }
    }

    public void requestSupport(Callback callback) {
        long userId = DataStorage.getUserId(sc_context);

        // Check Network
        if (!NetworkInfo.isNetworkAvailable(sc_context)) {
            callback.onNetworkConnectFailed();
            return;
        }

        if(userId < 0) {
            callback.onNoUser(sc_context.getString(R.string.create_user));
            return;
        }

        attemptRequestSupport(userId, callback);
    }

    private void attemptRequestSupport(long userId, final Callback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put(NetworkInfo.LOGIN_HEADER_AUTH, "JWT "+ DataStorage.getToken(sc_context));

        Map<String, String> params = new HashMap<>();
        params.put(NetworkInfo.SUPPORT_CALL_ME_POST_PARAMETER_ID, Long.toString(userId) );

        sc_httpCalls.standardPostCall(NetworkInfo.getURL(NetworkInfo.SUPPORT_CALL_ME_URL), NetworkInfo.SUPPORT_TAG, params, headers, new HttpCalls.Callback() {
            @Override
            public void successResponse(String response) {
                Log.d("Data dev", "Data support: " +response);
                onSuccessResponse(response, callback);
            }

            @Override
            public void errorResponse(String error) {
                callback.onServerError(error);
            }
        });
    }

    private void onSuccessResponse(String response, Callback callback) {
        JSONObject json;
        try {
            json = new JSONObject(response);
            Log.d("Data dev", "Support response Json: " +json);

            if(json.has("petition")){
                if(json.getString("petition").trim().equals("OK")) {
                    if(json.has("detail"))
                        callback.onRequestSupportComplete(json.getString("detail"));
                    else
                        callback.onRequestSupportComplete(sc_context.getString(R.string.support_send_to_service));
                    return;
                }
            }
        } catch (JSONException e) {
            callback.onServerError(sc_context.getString(R.string.support_send_error));
            e.printStackTrace();
            return;
        }

        callback.onRequestSupportFail(sc_context.getString(R.string.support_send_error));
    }
}
