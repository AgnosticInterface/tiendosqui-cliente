package com.tiendosqui.dapp.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tiendosqui.dapp.R;
import com.tiendosqui.dapp.utils.base.BaseTabActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 24/03/2017.
 *
 * The main content fragment in each tab.
 */

public class TabFragment extends Fragment {
    public static final int HOME = 0;
    public static final int SEARCH = 1;
    public static final int CAR = 2;
    public static final int FAVS = 3;
    public static final int SETTINGS = 4;

    private ArrayList<Fragment> mFragment;

    public static TabFragment getNewInstance(TabChildFragment fragment, int pos) {

        TabFragment tabFragment = new TabFragment();

        tabFragment.mFragment.add(fragment);// add first Fragment

        fragment.setParent(tabFragment);

        Log.d("Data dev", "Create new tab " +  tabFragment + ", " + fragment);

        return  tabFragment;
    }

    public TabFragment() {
        setRetainInstance(false);
        if(mFragment == null)
            mFragment = new ArrayList<>();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Fragment> oldFragments = getChildFragmentManager().getFragments();
        if (oldFragments != null && oldFragments.size() > 0) {
            mFragment = new ArrayList<>(oldFragments);

        } else {
            if (mFragment.size() > 1) {
                //Add first Fragment
                getChildFragmentManager().beginTransaction()
                        .add(R.id.container, mFragment.get(0))
                        .commit();
            }
        }

        if (mFragment.size() > 0){
            if (mFragment.get(mFragment.size() - 1) != null) {
                //Replace the Fragment for the last in the list
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, mFragment.get(mFragment.size() - 1))
                        .commit();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_base_tab, container, false);
    }

    public void removeBackItem() {
        //Remove the last item of the BackLog
        if (mFragment.size() > 1) {
            mFragment.remove(mFragment.size() - 1);

            if(mFragment.size() <= 1) {
                ((BaseTabActivity) getActivity()).showBackButton(false);
            }
        }
    }

    public void changeFragment(Fragment f1) {
        mFragment.add(f1);
        //Recive a new Fragment to add and show
        FragmentManager fm = getChildFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, f1);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        fm.executePendingTransactions();

        ((BaseTabActivity) getActivity()).showBackButton(true);

    }

    public Fragment getFragment(int position){

        if(position < mFragment.size())
            return mFragment.get(position);
        else
            return null;
    }

    public int size() {
        return mFragment.size();
    }
}
