package com.tiendosqui.dapp.tabs;

import android.support.v4.app.Fragment;

/**
 * Created by divait on 25/03/2017.
 *
 * The class for all the nested fragments.
 */

public abstract class TabChildFragment extends Fragment {
    protected TabFragment parent;

    public void setParent(TabFragment parent) {
        this.parent = parent;
    }
}
