package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 7/04/2017.
 */

public class OrderPUserJson {
    private String first_name;
    private String last_name;

    public OrderPUserJson() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
