package com.tiendosqui.dapp.entities.json;

import com.google.gson.Gson;
import com.tiendosqui.dapp.entities.Order;
import com.tiendosqui.dapp.entities.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medycitas on 31/03/2017.
 */

public class OrderJson {
    private Long shop;
    private String comment;
    private String method_pay;
    private String subtotal;
    private String delivery_cost;
    private String total;
    private String cant_total_products;
    private List<OrderProductJson> products;

    public OrderJson() {
    }

    public Long getShop() {
        return shop;
    }

    public void setShop(Long shop) {
        this.shop = shop;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getDelivery_cost() {
        return delivery_cost;
    }

    public void setDelivery_cost(String delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCant_total_products() {
        return cant_total_products;
    }

    public void setCant_total_products(String cant_total_products) {
        this.cant_total_products = cant_total_products;
    }

    public List<OrderProductJson> getProducts() {
        return products;
    }

    public void setProducts(List<OrderProductJson> products) {
        this.products = products;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setMethod_pay(String method_pay) {
        this.method_pay = method_pay;
    }

    public static OrderJson fromOrder(Order order) {
        OrderJson orderJson = new OrderJson();

        orderJson.setShop(order.getSellerId());
        orderJson.setSubtotal(Float.toString(order.getSubTotalPrice()));
        orderJson.setDelivery_cost(order.getSellerDeliveryCost());
        orderJson.setTotal(Float.toString(order.getTotalPrice()));
        orderJson.setCant_total_products(Integer.toString(order.getProductsCount()));
        orderJson.setComment(order.getComment());
        orderJson.setMethod_pay(order.getPayMethod());

        List<OrderProductJson> productsJson = new ArrayList<>();
        List<Product> products = order.getProducts();
        for(Product  product : products) {
            productsJson.add(
                new OrderProductJson(
                    product.getId(),
                    Integer.toString(product.getQuantity())
                )
            );
        }
        orderJson.setProducts(productsJson);

        return orderJson;
    }

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
