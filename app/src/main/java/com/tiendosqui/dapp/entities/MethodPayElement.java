package com.tiendosqui.dapp.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * Created by divait on 26/05/2017.
 */

public class MethodPayElement {

    private MethodPay method_pay;

    public MethodPayElement(){

    }

    public MethodPay getMethod_pay() {
        return method_pay;
    }
}
