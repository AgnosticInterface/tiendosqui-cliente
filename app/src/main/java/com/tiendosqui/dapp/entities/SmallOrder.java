package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;

import java.util.Date;
import java.util.List;

/**
 * Created by divait on 7/04/2017.
 */

public class SmallOrder implements Comparable<SmallOrder> {

    private long id;
    private Seller seller;
    private int address;
    private String payMethod;
    private int totalProducts;
    private String subtotal;
    private String total;
    private int status;
    private int time;

    private Date dateSend;
    private Date dateConfirm;
    private Date dateReject;
    private Date dateEnd;

    private List<Product> products;

    public SmallOrder(long id, Seller seller, int address, String payMethod, int totalProducts, String subtotal, String total, int status, Date dateSend, Date dateConfirm, Date dateReject, Date dateEnd, int time) {
        this.id = id;
        this.seller = seller;
        this.address = address;
        this.payMethod = payMethod;
        this.totalProducts = totalProducts;
        this.subtotal = subtotal;
        this.total = total;
        this.status = status;
        this.dateSend = dateSend;
        this.dateConfirm = dateConfirm;
        this.dateReject = dateReject;
        this.dateEnd = dateEnd;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public Seller getSeller() {
        return seller;
    }

    public int getAddress() {
        return address;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public int getTotalProducts() {
        return totalProducts;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public String getTotal() {
        return total;
    }

    public int getStatus() {
        return status;
    }

    public void setTotalProducts(int totalProducts) {
        this.totalProducts = totalProducts;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public Date getDateConfirm() {
        return dateConfirm;
    }

    public Date getDateReject() {
        return dateReject;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public int getTime() {
        return time;
    }

    @Override
    public int compareTo(SmallOrder order) {

        if (getId() > order.getId()) {
            return -1;
        }
        else if (getId() <  order.getId()) {
            return 1;
        }
        else {
            return 0;
        }

    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }


}
