package com.tiendosqui.dapp.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Divait on 24/01/2017.
 */

public class Category {
    private long id;
    private long subId;
    private String title;
    private String subcategory;
    private String image;

    private int quantity;
    private boolean loaded;

    public Category(String title, String image) {
        this.title = title;
        this.image = image;

        loaded = false;
    }

    public Category(String title, String image, int quantity) {
        this.title = title;
        this.image = image;
        this.quantity = quantity;

        loaded = false;
    }

    public Category(long id, String title, String image, int quantity) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.quantity = quantity;

        loaded = false;
    }

    public Category(long id, long subId, String title, String subcategory, String image) {
        this.id = id;
        this.subId = subId;
        this.title = title;
        this.subcategory = subcategory;
        this.image = image;

        loaded = false;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void plusOneQuantity() {
        this.quantity++;
    }
/*
    public static List<Category> getCategories(List<Product> products) {
        List<Category> categories = new ArrayList<>();

        for(Product product : products) {
            boolean exist = false;
            for(Category category : categories)
                if(product.getCategory().getId() == category.getId()) {
                    exist = true;
                    break;
                }

            if(!exist)
                categories.add(product.getCategory());
        }
        return categories;
    }
    */
}
