package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 21/12/2017.
 */

public class NewCategoryJson {

    private long categoryid;
    private int totalProductsinCategory;
    private String picture;
    private String name;
    private List<NewSubCategoryJson> subcategories;

    public NewCategoryJson() {
    }

    public long getCategoryId() {
        return categoryid;
    }

    public void setCategoryId(long categoryId) {
        this.categoryid = categoryId;
    }

    public int getTotalProductsinCategory() {
        return totalProductsinCategory;
    }

    public void setTotalProductsinCategory(int totalProductsinCategory) {
        this.totalProductsinCategory = totalProductsinCategory;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<NewSubCategoryJson> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<NewSubCategoryJson> subcategories) {
        this.subcategories = subcategories;
    }

    public static Category toCategory(NewCategoryJson categoryJson){
        return new Category(
                categoryJson.getCategoryId(),
                categoryJson.getName(),
                categoryJson.getPicture(),
                categoryJson.getTotalProductsinCategory()
        );
    }

    public static List<Category> toCategories(NewCategoryJson[] array){
        List<Category> categories = new ArrayList<>();
        for (NewCategoryJson json: array) {
            categories.add(toCategory(json));
        }

        return categories;
    }
}
