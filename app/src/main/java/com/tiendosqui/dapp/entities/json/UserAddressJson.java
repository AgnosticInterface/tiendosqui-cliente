package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 18/05/2017.
 */

public class UserAddressJson {
    private int id;
    private String address;
    private String address_alias;
    private String address_detail;

    public UserAddressJson() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress_alias() {
        return address_alias;
    }

    public void setAddress_alias(String address_alias) {
        this.address_alias = address_alias;
    }

    public String getAddress_detail() {
        return address_detail;
    }

    public void setAddress_detail(String address_detail) {
        this.address_detail = address_detail;
    }
}
