package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;

/**
 * Created by divai on 31/03/2017.
 */

public class User {
    private long id;

    private String name;
    private String phone;
    private String email;
    private String username;
    private String picture;
    private String type;
    private String status;

    public User(long id, String name, String phone, String email, String username, String picture, String type, String status) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.username = username;
        this.picture = picture;
        this.type = type;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPicture() {
        return picture;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
