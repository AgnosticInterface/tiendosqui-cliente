package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Address;

/**
 * Created by divai on 27/04/2017.
 */

public class AddressJson {
    private long id;
    private long client;
    private String address_alias;
    private String address;
    private String address_details;
    private String lat;
    private String lon;
    private String date_register;


    public AddressJson() {
    }

    public long getId() {
        return id;
    }

    public long getClient() {
        return client;
    }

    public String getAddress_alias() {
        return address_alias;
    }

    public String getAddress() {
        return address;
    }

    public String getAddress_details() {
        return address_details;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getDate_register() {
        return date_register;
    }

    public static Address toAddress(AddressJson addressJson) {
        double lat = 0;
        double lon = 0;
        try {
            lat = Double.valueOf(addressJson.getLat());
            lon = Double.valueOf(addressJson.getLon());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        return new Address(
                addressJson.getAddress_alias(),
                addressJson.getAddress_details(),
                addressJson.getAddress(),
                lat,
                lon
        );
    }
}
