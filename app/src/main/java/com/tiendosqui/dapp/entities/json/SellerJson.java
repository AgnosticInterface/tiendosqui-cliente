package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 7/04/2017.
 */

public class SellerJson {
    private long id;
    private String name;
    private String address;
    private String picture;
    private String phone;
    private String cat_shop;
    private String average_deliveries;

    public SellerJson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCat_shop() {
        return cat_shop;
    }

    public void setCat_shop(String cat_shop) {
        this.cat_shop = cat_shop;
    }

    public String getAverage_deliveries() {
        return average_deliveries;
    }

    public void setAverage_deliveries(String average_deliveries) {
        this.average_deliveries = average_deliveries;
    }
}
