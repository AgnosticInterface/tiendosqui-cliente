package com.tiendosqui.dapp.entities;

import java.util.List;

/**
 * Created by divai on 25/03/2017.
 */

public class Product {
    private long id;
    private long shopId;
    private String name;
    private String description;
    private String picture;
    private Category category;
    private String price;
    private String discountPrice;

    private int quantity;

    public Product(){}

    public Product(long id, String name, String description, String picture, Category category, String price, String discountPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.category = category;
        this.price = price;
        this.discountPrice = discountPrice;
    }

    public Product(long id, long shopId, String name, String description, String picture, Category category, String price, String discountPrice) {
        this.id = id;
        this.shopId = shopId;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.category = category;
        this.price = price;
        this.discountPrice = discountPrice;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Category getCategory() {
        return category;
    }

    public String getPicture() {
        return picture;
    }

    public String getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addQuantity() {
        this.quantity++;
    }

    public void removeQuantity() {
        this.quantity--;
    }

    public long getShopId() {
        return shopId;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public static int listContainsId(List<Product> list, long id) {
        for (int i =0; i<list.size();i++) {
            if (list.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }
}
