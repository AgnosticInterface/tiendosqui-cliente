package com.tiendosqui.dapp.entities.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by divai on 3/09/2017.
 */

public class BasePriceJson {

    @SerializedName("public")
    private String publicPrice;
    @SerializedName("list_price")
    private String listPrice;
    @SerializedName("desc")
    private String porcentaje;

    public BasePriceJson() {
    }

    public String getPublicPrice() {
        return publicPrice;
    }

    public String getListPrice() {
        return listPrice;
    }

    public String getPorcentaje() {
        return porcentaje;
    }
}
