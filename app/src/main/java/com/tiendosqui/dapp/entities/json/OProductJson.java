package com.tiendosqui.dapp.entities.json;

/**
 * Created by divait on 25/03/2017.
 */

public class OProductJson {

    private long order;
    private ProductJson product;
    private int quanty;
    private String price_unit;
    private String subtotal;

    public long getOrder() {
        return order;
    }

    public int getQuanty() {
        return quanty;
    }

    public String getPrice_unit() {
        return price_unit;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public ProductJson getProduct() {
        return product;
    }


    public OProductJson() {
    }

}
