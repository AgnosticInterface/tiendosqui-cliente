package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 11/05/2017.
 */

public class StatusOrderJson {
    private int id;
    private String name;

    public StatusOrderJson(){
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
