package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 25/03/2017.
 */

public class SubCategoryJson {
    private long id;
    private CategoryJson category;
    private String name;

    public SubCategoryJson() {
    }

    public long getId() {
        return id;
    }

    public CategoryJson getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }
}
