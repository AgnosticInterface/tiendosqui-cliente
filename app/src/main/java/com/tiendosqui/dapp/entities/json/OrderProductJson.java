package com.tiendosqui.dapp.entities.json;

/**
 * Created by Medycitas on 31/03/2017.
 */

public class OrderProductJson {

    private long product_id;
    private String cant;

    public OrderProductJson(long product_id, String cant) {
        this.product_id = product_id;
        this.cant = cant;
    }

    public long getProduct_id() {
        return product_id;
    }

    public String getCant() {
        return cant;
    }
}
