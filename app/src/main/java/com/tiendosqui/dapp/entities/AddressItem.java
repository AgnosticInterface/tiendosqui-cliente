package com.tiendosqui.dapp.entities;

/**
 * Created by divai on 17/04/2017.
 */

public class AddressItem {
    public long id;
    public String name;
    public String address;
    public int image;

    private Address addressItem;

    public AddressItem(long id, String name, String address, int image, Address a) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.image = image;
        addressItem = a;
    }
}
