package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Seller;
import com.tiendosqui.dapp.entities.SmallOrder;
import com.tiendosqui.dapp.utils.ActivityUtils;

/**
 * Created by divait on 7/04/2017.
 */

public class OrderProcessJson {

    private long id;
    private int user; // OrderPUserJson
    private UserAddressJson user_address;
    private String method_pay;
    private SellerJson shop;
    private int total_quanty_products;
    private String delivery_cost;
    private String subtotal;
    private String total;
    private int time;
    private String date_register;
    private String date_send;
    private String date_confirm;
    private String date_reject;
    private String date_end;
    private StatusOrderJson status_order;

    public OrderProcessJson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getUser_address() {
        return user_address.getId();
    }

    public String getMethod_pay() {
        return method_pay;
    }

    public void setMethod_pay(String method_pay) {
        this.method_pay = method_pay;
    }

    public SellerJson getShop() {
        return shop;
    }

    public void setShop(SellerJson shop) {
        this.shop = shop;
    }

    public int getTotal_quanty_products() {
        return total_quanty_products;
    }

    public void setTotal_quanty_products(int total_quanty_products) {
        this.total_quanty_products = total_quanty_products;
    }

    public String getDelivery_cost() {
        return delivery_cost;
    }

    public void setDelivery_cost(String delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDate_register() {
        return date_register;
    }

    public String getDate_send() {
        return date_send;
    }

    public String getDate_confirm() {
        return date_confirm;
    }

    public String getDate_reject() {
        return date_reject;
    }

    public String getDate_end() {
        return date_end;
    }

    public StatusOrderJson getStatus_order() {
        return status_order;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public static SmallOrder toOrder(OrderProcessJson order) {
        if(order == null || order.getShop() == null)
            return null;

        Seller seller = new Seller(
            order.getShop().getId(),
            order.getShop().getName(),
            order.getShop().getAddress(),
            order.getShop().getPicture(),
            order.getShop().getPhone(),
            order.getShop().getCat_shop(),
            order.getShop().getAverage_deliveries()
        );

        return new SmallOrder(
            order.getId(),
            seller,
            order.getUser_address(),
            order.getMethod_pay(),
            order.getTotal_quanty_products(),
            order.getSubtotal(),
            order.getTotal(),
            order.getStatus_order().getId(),
            ActivityUtils.getUtilDate(order.getDate_send()),
            ActivityUtils.getUtilDate(order.getDate_confirm()),
            ActivityUtils.getUtilDate(order.getDate_reject()),
            ActivityUtils.getUtilDate(order.getDate_end()),
            order.getTime()
        );
    }
}
