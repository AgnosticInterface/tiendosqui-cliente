package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;

/**
 * Created by Divait on 22/03/2017.
 *
 * An object for store the addresses.
 */

public class Address {

    private long id;
    private String name;
    private String comment;
    private String street;

    private String country;
    private String city;

    private double latitude;
    private double longitude;

    private int imageId;

    public static Address JsonToAddress(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Address.class);
    }

    public Address(String name, String comment, String street, double latitude, double longitude) {
        this.name = name;
        this. comment = comment;
        this.street = checkStreet(street);

        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address(String street, double latitude, double longitude) {
        this.street = checkStreet(street);

        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address(String street) {
        this.street = checkStreet(street);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getComment() {

        if(comment == null)
            return "...";
        else
            return comment;
    }

    public String getStreet() {
        return street;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String toAddressString() {
        return street;
    }

    private String checkStreet(String street) {
        String[] split = street.split(",");

        return split[0];
    }
}
