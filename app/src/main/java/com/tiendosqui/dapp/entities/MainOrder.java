package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 26/04/2017.
 */

public class MainOrder {

    private List<Order> orders;
    private int type; // For checkout <3w2a-ñ|5 x

    public MainOrder() {
        orders = new ArrayList<>();
    }

    public MainOrder(MainOrder order) {
        orders = order.getOrders();
    }

    public void addOrder(Order mOrder) {
        if(hasOrder(mOrder.getSellerId()))
            orders.set(indexOf(mOrder.getSellerId()), mOrder);
        else
            orders.add(mOrder);
    }

    public void removeOrder(long sellerId) {
        for (Order order : orders)
            if (order.getSellerId() == sellerId) {
                orders.remove(order);
                return;
            }
    }

    public Order getOrder(long sellerId) {
        for (Order order : orders) {
            if (order.getSellerId() == sellerId) {
                return order;
            }
        }

        return null;
    }

    public boolean hasOrder(long sellerId) {
        for (Order order : orders) {
            if (order.getSellerId() == sellerId) {
                return true;
            }
        }
        return  false;
    }

    public int indexOf(long sellerId) {
        int i = 0;
        for (Order order : orders) {
            if (order.getSellerId() == sellerId) {
                return i;
            }
            i++;
        }
        return  -1;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
