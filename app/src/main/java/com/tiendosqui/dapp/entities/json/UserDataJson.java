package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 31/03/2017.
 */

public class UserDataJson {

    private String first_name;
    private String email;
    private String username;

    public UserDataJson() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }
}
