package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 24/04/2017.
 */

public class StateJson {
    private int id;
    private String name;

    public StateJson() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
