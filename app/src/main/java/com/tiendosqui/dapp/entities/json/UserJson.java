package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.User;

/**
 * Created by divai on 31/03/2017.
 */

public class UserJson {

    private UserDataJson user;
    private String phone;
    private String picture;
    private UserTypeJson type_user;
    private UserStatusJson status;

    public UserJson() {
    }

    public UserDataJson getUser() {
        return user;
    }

    public String getPhone() {
        return phone;
    }

    public String getPicture() {
        return picture;
    }

    public UserTypeJson getType_user() {
        return type_user;
    }

    public UserStatusJson getStatus() {
        return status;
    }

    public static User fromJsonToUser(UserJson userJson, long id) {
        return new User(id,
                userJson.getUser().getFirst_name(),
                userJson.getPhone(),
                userJson.getUser().getEmail(),
                userJson.getUser().getUsername(),
                userJson.getPicture(),
                userJson.getType_user().getName(),
                userJson.getStatus().getName()
        );
    }
}
