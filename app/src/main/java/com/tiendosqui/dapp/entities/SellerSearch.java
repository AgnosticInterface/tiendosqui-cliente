package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by divait on 27/04/2017.
 */

public class SellerSearch extends Seller {

    private List<Product> products;

    public SellerSearch(Seller seller) {
        super(seller.getId(),
                seller.getName(),
                seller.getAddress(),
                seller.getPicture(),
                seller.getPhone(),
                seller.getCat_shop(),
                seller.getAverage_deliveries(),
                Float.toString(seller.getMin_price()),
                seller.getMin_shipping_price(),
                seller.getMethods_payment()
        );
    }

    public List<Product> getProducts() {
        if(products == null)
            products = new ArrayList<>();

        return products;
    }

    public void addProduct(Product product) {
        if(products == null)
            products = new ArrayList<>();

        products.add(product);
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static SellerSearch JsonToSeller(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, SellerSearch.class);
    }
}
