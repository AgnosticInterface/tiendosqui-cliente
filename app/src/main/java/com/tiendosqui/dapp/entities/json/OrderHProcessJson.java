package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 7/04/2017.
 */

public class OrderHProcessJson {

    private long id;
    private OrderPUserJson user; // TODO
    private int user_address;
    private String method_pay;
    private int shop;
    private int total_quanty_products;
    private String delivery_cost;
    private String subtotal;
    private String total;
    private String date_register;
    private StateJson status_order;

    public OrderHProcessJson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public OrderPUserJson getUser() {
        return user;
    }

    public void setUser(OrderPUserJson user) {
        this.user = user;
    }

    public int getUser_address() {
        return user_address;
    }

    public void setUser_address(int user_address) {
        this.user_address = user_address;
    }

    public String getMethod_pay() {
        return method_pay;
    }

    public void setMethod_pay(String method_pay) {
        this.method_pay = method_pay;
    }

    public int getShop() {
        return shop;
    }

    public void setShop(int shop) {
        this.shop = shop;
    }

    public int getTotal_quanty_products() {
        return total_quanty_products;
    }

    public void setTotal_quanty_products(int total_quanty_products) {
        this.total_quanty_products = total_quanty_products;
    }

    public String getDelivery_cost() {
        return delivery_cost;
    }

    public void setDelivery_cost(String delivery_cost) {
        this.delivery_cost = delivery_cost;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDate_register() {
        return date_register;
    }

    public void setDate_register(String date_register) {
        this.date_register = date_register;
    }

    public StateJson getStatus_order() {
        return status_order;
    }
/*
    public static SmallOrder toOrder(OrderHProcessJson order) {
        Seller seller = new Seller(
                /
            order.getShop().getId(),
            order.getShop().getName(),
            order.getShop().getAddress(),
            order.getShop().getPicture(),
            order.getShop().getPhone(),
            order.getShop().getCat_shop(),
            order.getShop().getAverage_deliveries()
/
        );

        return new SmallOrder(
            order.getId(),
            seller,
            order.getUser_address(),
            order.getMethod_pay(),
            order.getTotal_quanty_products(),
            order.getSubtotal(),
            order.getTotal(),
            order.getStatus_order().getId()
        );
    }
    */
}
