package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 25/03/2017.
 */

public class InnerProductJson {
    private SubCategoryJson subcategory;
    private String name;
    private String description;
    private String picture;
    private String sugested_price;

    public InnerProductJson() {
    }

    public SubCategoryJson getSubcategory() {
        return subcategory;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public String getSugested_price() {
        return sugested_price;
    }
}
