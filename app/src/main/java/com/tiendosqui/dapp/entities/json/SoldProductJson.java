package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Product;

/**
 * Created by Medycitas on 18/04/2017.
 */

public class SoldProductJson {

    private long product_id;
    private String price;
    private String description;
    private int total;
    private String suggested_price;
    private String image;
    private String name;

    public SoldProductJson() {
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getSuggested_price() {
        return suggested_price;
    }

    public void setSuggested_price(String suggested_price) {
        this.suggested_price = suggested_price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Product toProduct(SoldProductJson productJson) {
        return new Product(
                productJson.getProduct_id(),
                productJson.getName(),
                productJson.getDescription(),
                productJson.getImage(),
                null,
                productJson.getPrice(),
                null
        );
    }
}
