package com.tiendosqui.dapp.entities.json;

import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * Created by divai on 8/06/2017.
 */

@Table
public class JsonCity {
    @Unique
    long id;
    String name;
    String description;
    String picture;

    public JsonCity() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }
}
