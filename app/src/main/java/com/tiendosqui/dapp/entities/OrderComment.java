package com.tiendosqui.dapp.entities;

/**
 * Created by Medycitas on 15/05/2017.
 */

public class OrderComment {
    private long sellerId;
    private String comment;

    public OrderComment(long sellerId, String comment) {
        this.sellerId = sellerId;
        this.comment = comment;
    }

    public long getSellerId() {
        return sellerId;
    }

    public String getComment() {
        return comment;
    }
}
