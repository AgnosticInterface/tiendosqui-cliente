package com.tiendosqui.dapp.entities.json;

import com.tiendosqui.dapp.entities.Category;

/**
 * Created by divai on 25/03/2017.
 */

public class CategoryJson {
    private long id;
    private String name;
    private String picture;

    public CategoryJson(long id, String name, String picture) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPicture() {
        return picture;
    }
/*
    public static Category toCategory(CategoryJson categoryJson){
        return new Category(
                categoryJson.getId(),
                categoryJson.getName(),
                categoryJson.getPicture()
        );
    }
    */
}
