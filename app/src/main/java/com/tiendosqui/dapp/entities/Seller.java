package com.tiendosqui.dapp.entities;

import com.google.gson.Gson;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * Created by divait on 25/01/2017.
 */

@Table
public class Seller {
    @Unique
    private long id;
    private String name;
    private String cat_shop;
    private String average_deliveries;

    private String address;
    private String phone;
    private String picture;
    private String min_price;
    private String min_shipping_price;
    private float rate;
    @Ignore
    private MethodPayElement[] methods_payment;
    private String payments;

    public static Seller JsonToSeller(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Seller.class);
    }

    public Seller() {
    }

    public Seller(long id, String name, String address, String picture, String phone, String cat_shop, String average_deliveries) {
        this.id = id;
        this.name = name;
        this.address =address;
        this.picture = picture;
        this.phone = phone;
        this.cat_shop = cat_shop;
        this.average_deliveries = average_deliveries;
    }

    public Seller(long id, String name, String address, String picture, String phone, String cat_shop, String average_deliveries, String min_price, String min_shipping_price, MethodPayElement[] methods) {
        this.id = id;
        this.name = name;
        this.address =address;
        this.picture = picture;
        this.phone = phone;
        this.cat_shop = cat_shop;
        this.average_deliveries = average_deliveries;
        this.min_price = min_price;
        this.min_shipping_price = min_shipping_price;
        this.methods_payment = methods;

        Gson gson = new Gson();
        payments = gson.toJson(methods);
    }

    public String getPicture() {
        return picture;
    }

    public void setImage(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCat_shop() {
        return cat_shop;
    }

    public void setCat_shop(String cat_shop) {
        this.cat_shop = cat_shop;
    }

    public String getAverage_deliveries() {
        return average_deliveries;
    }

    public long getId() {
        return id;
    }

    public String getMin_shipping_price() {
        return min_shipping_price;
    }

    public float getMin_price() {
        float min = 0;

        try {
            min = Float.valueOf(min_price);
        } catch (NumberFormatException ex){
            ex.printStackTrace();
        }

        return min;
    }

    public float getRate() {
        return rate;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public void setAverage_deliveries(String average_deliveries) {
        this.average_deliveries = average_deliveries;
    }

    public MethodPayElement[] getMethods_payment() {
        if(methods_payment == null) {
            Gson gson = new Gson();
            methods_payment = gson.fromJson(payments, MethodPayElement[].class);
        }

        return methods_payment;
    }

    public void setPayments() {
        Gson gson = new Gson();
        payments = gson.toJson(methods_payment);
    }

    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
