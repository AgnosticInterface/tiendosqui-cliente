package com.tiendosqui.dapp.entities.json;

/**
 * Created by divai on 21/12/2017.
 */

public class NewSubCategoryJson {

    private long id;
    private int total;
    private String name;

    public NewSubCategoryJson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
